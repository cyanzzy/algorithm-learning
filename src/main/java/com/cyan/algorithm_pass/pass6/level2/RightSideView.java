package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.*;

/**
 * 右视图
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class RightSideView {

//    // BFS
//    public List<Integer> rightSideView(TreeNode root) {
//
//        List<Integer> res = new ArrayList<>();
//        // base case
//        if (root == null) {
//            return res;
//        }
//        Queue<TreeNode> queue = new LinkedList<>();
//        queue.offer(root);
//
//        while (!queue.isEmpty()) {
//            int cnt = queue.size();
//            for (int i = 0; i < cnt; i++) {
//                TreeNode node = queue.poll();
//                if (node.left != null) {
//                    queue.offer(node.left);
//                }
//                if (node.right != null) {
//                    queue.offer(node.right);
//                }
//                if (i == cnt - 1) {
//                    // 将当前层最后一个节点放入列表
//                    res.add(node.val);
//                }
//            }
//        }
//        return res;
//    }

    // DFS
    private List<Integer> ans;

    public List<Integer> rightSideView(TreeNode root) {
        ans = new ArrayList<>();
        dfs(root, 0);
        return ans;
    }

    private void dfs(TreeNode node, int depth) {
        if (node == null) return;
        if (ans.size() <= depth)
            ans.add(node.val);
        else
            ans.set(depth, node.val);
        dfs(node.left, depth + 1);
        dfs(node.right, depth + 1);
    }

}
