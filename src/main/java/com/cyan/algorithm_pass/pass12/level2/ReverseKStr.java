package com.cyan.algorithm_pass.pass12.level2;

/**
 * K 组反转字符串
 *
 * @author Cyan Chau
 * @create 2024-03-19
 */
public class ReverseKStr {

    public String reverseStr(String s, int k) {

        char[] chars = s.toCharArray();
        // OFFSET 2 * K
        for (int i = 0; i < chars.length; i += 2 * k) {
            reverse(chars, i, Math.min(i + k, chars.length) - 1);
        }
        return new String(chars);

    }

    public void reverse(char[] arr, int left, int right) {
        while (left < right) {
            char temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left++;
            right--;
        }
    }
}
