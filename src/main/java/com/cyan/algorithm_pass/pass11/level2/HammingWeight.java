package com.cyan.algorithm_pass.pass11.level2;

/**
 * 位 1 的个数
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class HammingWeight {

//    // 循环检查二进制位
//
//    /**
//     * @see com.cyan.algorithm_pass.pass11.level1
//     * @param n
//     * @return
//     */
//    public int hammingWeight(int n) {
//        int ret = 0;
//
//        for (int i = 0; i < 32; i++) {
//            if ((n & 1 << i) != 0) {
//                ret++;
//            }
//        }
//        return ret;
//    }


    // 位运算优化
    public int hammingWeight(int n) {
        int ret = 0;
        while (n != 0) {
            n &= n - 1;
            ret++;
        }
        return ret;
    }
}
