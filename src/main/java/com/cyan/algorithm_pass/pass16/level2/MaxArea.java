package com.cyan.algorithm_pass.pass16.level2;

/**
 * 盛水最多的容器
 *
 * @author Cyan Chau
 * @create 2024-04-29
 */
public class MaxArea {

    public int maxArea(int[] height) {
        int l = 0, r = height.length - 1;
        int ans = 0;
        while (l < r) {
            int area = Math.min(height[l], height[r]) * (r - l);
            ans = Math.max(ans, area);
            if (height[l] <= height[r]) {
                ++l;
            }
            else {
                --r;
            }
        }
        return ans;
    }
}
