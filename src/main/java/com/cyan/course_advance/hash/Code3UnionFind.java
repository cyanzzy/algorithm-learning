package com.cyan.course_advance.hash;

import java.util.HashMap;
import java.util.List;
import java.util.Stack;

/**
 * 并查集
 *
 * @author Cyan Chau
 * @create 2023-08-15
 */
public class Code3UnionFind {


    // 将集合中的单个元素封装成并查集结构
    public static class Element<V> {
        public V value;

        public Element(V value) {
            this.value = value;
        }

    }

    //并查集操作
    public static class UnionFindSet<V> {

        // 元素 V 所对应的并查集结构
        public HashMap<V, Element<V>> elementMap;

        // 存储代表节点  key 的代表元素是 value
        public HashMap<Element<V>, Element<V>> fatherMap;

        // 存储代表节点所在集合的秩，代表节点所在集合的元素个数为value
        public HashMap<Element<V>, Integer> rankMap;

        // 初始化并查集
        public UnionFindSet(List<V> list) {

            // 初始化哈希结构
            elementMap = new HashMap<>();
            fatherMap = new HashMap<>();
            rankMap = new HashMap<>();

            // 对于集合中的单个元素封装成并查集结构
            for (V value : list) {

                // 将value封装成并查集节点
                Element<V> element = new Element<V>(value);

                // value-->对应并查集节点
                elementMap.put(value, element);

                // key的代表节点为value
                fatherMap.put(element, element);

                // key，代表节点所在集合元素个数初始化成1
                rankMap.put(element, 1);
            }
        }

        // 递归找代表节点
        public Element<V> findFather(Element<V> element) {

            Stack<Element<V>> path = new Stack<>();

            while (element != fatherMap.get(element)) {
                // 将不是代表节点的元素压栈
                path.push(element);
                // 继续向上查找
                element = fatherMap.get(element);
            }

            // 路径压缩：将element到代表节点所在路径的元素的直接父亲设置成代表节点，降低时间复杂度
            while (!path.isEmpty()) {
                fatherMap.put(path.pop(), element);
            }
            return element;
        }

        // 判断两集合是不是属于一个集合
        public boolean isSameSet(V a, V b) {

            // 如果两个集合的代表节点一样，表示在一个集合中，返回true，否则不一样返回false
            if (elementMap.containsKey(a) && elementMap.containsKey(b)) {
                return findFather(elementMap.get(a)) == findFather(elementMap.get(b));
            }

            return false;
        }

        // 将a、b所在的两个集合合并
        public void union(V a, V b) {

            if (elementMap.containsKey(a) && elementMap.containsKey(b)) {

                Element<V> aF = findFather(elementMap.get(a));
                Element<V> bF = findFather(elementMap.get(b));

                // 当处于不同集合才能合并
                if (aF != bF) {

                    // 存储较大的集合
                    Element<V> big = rankMap.get(aF) >= rankMap.get(bF) ? aF : bF;
                    // 存储看较小的集合
                    Element<V> small = big == aF ? bF : aF;
                    // 将较小集合的直接代表节点设置成较大集合的代表节点
                    fatherMap.put(small, big);
                    // 更新秩
                    rankMap.put(big, rankMap.get(aF) + rankMap.get(bF));
                    // 删除小集合
                    rankMap.remove(small);
                }
            }
        }
    }
}