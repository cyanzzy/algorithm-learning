package com.cyan.course_base.graph;

import java.util.Collection;
import java.util.HashMap;

/**
 * @author Cyan Chau
 * @create 2023-04-06
 */
public class UnionFind {

    // 保存并查集中所有集合的所有元素的代表节点
    // <Node, Node's Father>
    private HashMap<Node, Node> fatherMap;

    // 保存节点所在集合的秩（集合元素数）
    private HashMap<Node, Integer> rankMap;

    public UnionFind() {
        this.fatherMap = new HashMap<>();
        this.rankMap = new HashMap<>();
    }

    // 找到father节点
    private Node findFather(Node node) {
        Node father =  fatherMap.get(node);
        if (father != node) {
            father = findFather(father);
        }
        fatherMap.put(node, father);
        return father;
    }

    // 初始化并查集所有单个元素，father指向自己，rank置1
    public void makeSets(Collection<Node> nodes) {
        fatherMap.clear();
        rankMap.clear();
        for (Node node : nodes) {
            fatherMap.put(node, node);
            rankMap.put(node, 1);
        }
    }

    // 判断a、b是否属于一个集合
    public boolean isSameSet(Node a, Node b) {
        return findFather(a) == findFather(b);
    }

    // 将a所在的集合和b所在的集合合并
    public void union(Node a, Node b) {
        // 若a、b为空
        if (a == null || b == null) {
            return;
        }
        // 找到a的代表节点
        Node aFather = findFather(a);
        // 找到b的代表节点
        Node bFather = findFather(b);
        // 合并操作
        if (aFather != bFather) {
            // a所在集合元素个数
            int aFrank = rankMap.get(aFather);
            // b所在集合元素个数
            int bFrank = rankMap.get(bFather);
            // 将少的归并到多的集合中
            if (aFrank <= bFrank) {
                // a所在的集合并到b所在的集合中
                fatherMap.put(aFather, bFather);
                rankMap.put(bFather, aFrank + bFrank);
            } else {
                // b所在的集合并到a所在的集合中
                fatherMap.put(bFather, aFather);
                rankMap.put(aFather, aFrank + bFrank);
            }
        }

    }
}
