package com.cyan.algorithm_pass.pass8.level1;

import com.cyan.algorithm_pass.TreeNode;

/**
 * 轴对称树
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class IsSymmetric {

    public boolean isSymmetric(TreeNode root) {
        return dfs(root, root);
    }

    public boolean dfs(TreeNode p, TreeNode q) {
        if (p == null && q == null) {
            return true;
        }
        if (p == null || q == null) {
            return false;
        }
        return p.val == q.val && dfs(p.left, q.right) && dfs(p.right, q.left);
    }
}
