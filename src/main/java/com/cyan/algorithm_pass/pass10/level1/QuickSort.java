package com.cyan.algorithm_pass.pass10.level1;

import com.cyan.course_base.sort.Code6QuickSort;

import java.util.List;

/**
 * 快速排序
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class QuickSort {

    /**
     * 实现方式 1
     *
     * @<code>
     *     package com.cyan.course_base.sort;
     *     Code6QuickSort
     * </code>
     * @see com.cyan.course_base.sort.Code6QuickSort
     */

    /**
     * 实现方式 2
     * @param array
     * @param start
     * @param end
     */
    public void quickSort(int[] array, int start, int end) {
        if (start >= end) {
            return;
        }
        int pivotIndex = doublePointerSwap(array, start, end);
        // 用分界值下标区分出左右区间，进行递归调用
        quickSort(array, start, pivotIndex - 1);
        quickSort(array, pivotIndex + 1, end);
    }


    // 参考 ：https://baike.baidu.com/item/%E5%BF%AB%E9%80%9F%E6%8E%92%E5%BA%8F%E7%AE%97%E6%B3%95/369842
    /**
     * 双边指针（交换法）
     * 思路：
     * 记录分界值 pivot，创建左右指针（记录下标）。
     * （分界值选择方式有：首元素，随机选取，三数取中法）
     *
     * 首先从右向左找出比pivot小的数据，
     * 然后从左向右找出比pivot大的数据，
     * 左右指针数据交换，进入下次循环。
     *
     * 结束循环后将当前指针数据与分界值互换，
     * 返回当前指针下标（即分界值下标）
     */
    public int doublePointerSwap(int[] arr, int startIndex, int endIndex) {
        int pivot = arr[startIndex];
        int leftPoint = startIndex;
        int rightPoint = endIndex;

        while (leftPoint < rightPoint) {
            // 从右向左找出比pivot小的数据
            while (leftPoint < rightPoint
                    && arr[rightPoint] > pivot) {
                rightPoint--;
            }
            // 从左向右找出比pivot大的数据
            while (leftPoint < rightPoint
                    && arr[leftPoint] <= pivot) {
                leftPoint++;
            }
            // 没有过界则交换
            if (leftPoint < rightPoint) {
                int temp = arr[leftPoint];
                arr[leftPoint] = arr[rightPoint];
                arr[rightPoint] = temp;
            }
        }
        // 最终将分界值与当前指针数据交换
        arr[startIndex] = arr[rightPoint];
        arr[rightPoint] = pivot;
        // 返回分界值所在下标
        return rightPoint;
    }


    public static void main(String[] args) {
        int[] arr = {1,4,7,58,69,32};
        new Code6QuickSort().printArray(arr);
        new QuickSort().quickSort(arr, 0, arr.length-1);
        new Code6QuickSort().printArray(arr);
    }
}
