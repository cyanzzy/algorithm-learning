package com.cyan.algorithm_pass.pass12.level1;

/**
 * 字符串转换整数
 *
 * @author Cyan Chau
 * @create 2024-03-19
 */
public class MyAtoi {

    // 臃肿代码
    public int myAtoi(String s) {

        char[] chars = s.toCharArray();

        // 1. 读入字符串并丢弃无用的前导空格
        int index = 0;
        while (index < s.length() && chars[index] == ' ') {
            index++;
        }
        // 【极端情况】{"        "}
        if (index == s.length()) {
            // 如果没有读入数字，则整数为 0
            return 0;
        }

        // 2. 检查下一个字符（假设还未到字符末尾）为正还是负号，读取该字符（如果有）。
        // 确定最终结果是负数还是正数。 如果两者都不存在，则假定结果为正。
        int sign = 1;
        char firstChar = chars[index];
        if (firstChar == '+') {
            index++;
        } else if (firstChar == '-') {
            index++;
            sign = -1;
        }

        // 读入下一个字符，直到到达下一个非数字字符或到达输入的结尾
        // 3. 将后续出现的数字字符进行转换
        int ans = 0;
        while (index < s.length()) {
            char curChar = chars[index];
            // 3.1 合法性校验
            if (curChar > '9' || curChar < '0') {
                break;
            }
            // 3.2 溢出判断
            if (ans > Integer.MAX_VALUE / 10 ||
                    (ans == Integer.MAX_VALUE / 10 && (curChar - '0') > Integer.MAX_VALUE % 10)) {
                return Integer.MAX_VALUE;
            }
            if (ans < Integer.MIN_VALUE / 10 ||
                    (ans == Integer.MIN_VALUE / 10 && (curChar - '0') > -(Integer.MIN_VALUE % 10))) {
                return Integer.MIN_VALUE;
            }
            ans = ans * 10 + sign * (curChar - '0');
            index++;
        }
        return ans;

    }

    // 自动机
}
