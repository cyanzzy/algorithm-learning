package com.cyan.course_base.graph;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * @author Cyan Chau
 * @create 2023-04-06
 */
public class Kruskal {

    // 比较器
    public class EdgeComparator implements Comparator<Edge> {
        @Override
        public int compare(Edge o1, Edge o2) {
            return o1.weight - o2.weight;
        }
    }

    public Set<Edge> kruskal(Graph graph) {
        // 定义并查集结构
        UnionFind unionFind = new UnionFind();
        // 初始化并查集
        unionFind.makeSets(graph.nodes.values());
        // 定义优先队列存储边集合
        PriorityQueue<Edge> priorityQueue = new PriorityQueue<>(new EdgeComparator());

        for (Edge edge : graph.edges) {
            priorityQueue.add(edge);
        }

        Set<Edge> result = new HashSet<>();

        while (!priorityQueue.isEmpty()) {
            // 弹出边集合中最小的边
            Edge edge = priorityQueue.poll();
            if (!unionFind.isSameSet(edge.from, edge.to)) {
                result.add(edge);
            }
        }
        return result;
    }

}
