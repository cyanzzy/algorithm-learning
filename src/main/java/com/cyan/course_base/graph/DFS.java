package com.cyan.course_base.graph;

import java.util.HashSet;
import java.util.Stack;

/**
 * @author Cyan Chau
 * @create 2023-04-01
 */
public class DFS {

    // 非递归实现
    public void dfs(Node node) {
        // 如果节点不存在，则返回
        if (node == null) {
            return;
        }

        // 定义递归栈
        Stack<Node> stack = new Stack<>();
        // 定义标记数组
        HashSet<Node> set = new HashSet<>();

        // 当前节点入栈
        stack.add(node);
        // 同时标记为已访问
        set.add(node);

        // Visit操作
        System.out.println(node.value);

        while (!stack.isEmpty()) {
            // 栈顶元素出栈
            Node cur = stack.pop();
            // 遍历栈顶元素的邻接点
            for (Node next : cur.nexts) {
                // 若next没有被访问过
                if (!set.contains(next)) {
                    // 当前节点cur继续压栈，其邻接点没有被访问完
                    stack.push(cur);
                    // 当前节点cur的邻接点next压栈
                    stack.add(next);
                    // Visit操作
                    System.out.println(next.value);
                    // break
                    break;
                }
            }
        }
    }
}
