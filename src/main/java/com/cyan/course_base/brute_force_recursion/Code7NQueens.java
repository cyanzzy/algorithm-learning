package com.cyan.course_base.brute_force_recursion;

/**
 * N 皇后
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code7NQueens {


    /**
     * 判断摆放位置是否合法
     *
     * 目前在 i 行放皇后，record[0:i-1] 需要注意，record[i...] 不需要注意
     *
     * @return 返回 i 行皇后，放在j列，是否合法
     */
    public boolean isValid(int[] record, int i, int j) {
        for(int k = 0; k < i; k++) { // 之前的某个k行的皇后
            // 共列                       共斜线
            if(j == record[k] || Math.abs(record[k] - j) == Math.abs(i - k)) {
                return false;
            }
        }
        return true;
    }

    // record[i]表示第 i 行皇后所在的列数
    public int process(int i, int[] record, int n) {

        if (i == n) { // 终止条件
            // 之前做的选择，来到终止位置，有一种结果
            return 1;
        }

        int res = 0;

        // 在当前 i 行  0...n-1 列尝试
        for (int j = 0; j < n; j++) {
            // 当前 i 行的皇后放在 j 列，会不会和之前的 (0..i-1) 行的皇后，共行 共列 或 共斜线
            // 若是，认为无效；若不是，认为有效
            if(isValid(record, i, j)) {
                record[i] = j;
                // 继续尝试下一行
                res += process(i + 1, record, n);
            }
        }
        return res;
    }

    // invoke
    public int NQueens(int n) {

        if (n < 1) {
            return 0;
        }

        // record[i]表示第i行的皇后放在第几列
        int[] record = new int[n];

        /**
         * 0 从第0行放皇后
         *
         * record 存放列的信息
         *
         * n 不能超过 n 行
         */
        return process(0, record, n);
    }
}

class BitSpeed {

    /**
     * process
     *
     * @param limit 限制数，由 n 决定
     * @param colLim 列的限制，1的位置不能放皇后，0的位置可以
     * @param leftDiaLim 左斜线的限制，1的位置不能放皇后，0的位置可以
     * @param rightDiaLim 右斜线的限制，1的位置不能放皇后，0的位置可以
     * @return 摆放个数
     */
    public int process(int limit, int colLim, int leftDiaLim,
                       int rightDiaLim) {
        // n行的n皇后放完了
        if (colLim == limit) { // 列限制放完了，没地方放了
            return 1;
        }

        // pos代表目前能放皇后的位置
        int pos = 0;
        // mostRightOne代表在pos中，最右边的1在什么位置
        int mostRightOne = 0;

        // colLim | leftDiaLim | rightDiaLim 总限制
        // pos代表目前能放皇后的位置
        pos = limit & (~(colLim | leftDiaLim | rightDiaLim));
        int res = 0;
        // pos上几个1循环几次
        while (pos != 0) {
            // mostRightOne代表在pos中，最右边的1在什么位置，
            // 然后从右到左依次筛选出pos中可选择的位置进行尝试
            mostRightOne = pos & (~pos + 1);
            pos = pos - mostRightOne;
            // 下一行怎么尝试
            res += process(limit, colLim | mostRightOne,  // 列限制或当前的决定
                    (leftDiaLim | mostRightOne) << 1, // 左限制或当前决定整体左移
                    (rightDiaLim | mostRightOne) >>> 1);// 右限制或当前决定整体右移
        }
        return res;
    }

    public int NQueens(int n) {
        // 违规条件
        if (n < 1 || n > 32) {
            return 0;
        }
        // 生成二进制数，譬如，9皇后 该二进制数表示低9位是1，高位全是0
        int limit = n == 32 ? -1 : (1 << n) - 1;
        // 调用得到答案
        return process(limit, 0, 0, 0);
    }

}
