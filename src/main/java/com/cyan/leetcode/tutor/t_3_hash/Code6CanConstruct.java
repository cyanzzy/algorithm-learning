package com.cyan.leetcode.tutor.t_3_hash;

/**
 * 赎金信
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/ransom-note/description/
 * @author Cyan Chau
 * @create 2023-04-02
 * update 2024-04-11
 */
public class Code6CanConstruct {

    public boolean canConstruct(String ransomNote, String magazine) {

        // 违规条件
        if (ransomNote.length() > magazine.length()) {
            return false;
        }

        // 统计magazine中的词频
        int[] frequency = new int[26];
        for (char ch : magazine.toCharArray()) {
            frequency[ch - 'a']++;
        }

        // 遍历ransomNote
        for (char ch : ransomNote.toCharArray()) {
            frequency[ch - 'a']--;
            // 若magazine中某词频不足0，说明缺少字符构成ransom
            if (frequency[ch - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }
}
