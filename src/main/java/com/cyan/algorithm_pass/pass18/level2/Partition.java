package com.cyan.algorithm_pass.pass18.level2;

import java.util.ArrayList;
import java.util.List;

/**
 * 分割回文串
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class Partition {

    List<String> path;
    List<List<String>> ans;
    public List<List<String>> partition(String s) {
        path = new ArrayList<>();
        ans = new ArrayList<>();
        dfs(s, 0);
        return ans;
    }

    public void dfs(String s, int startIndex) {
        if (startIndex >= s.length()) {
            ans.add(new ArrayList<>(path));
            return;
        }

        for (int i = startIndex; i < s.length(); i++) {
            if (isPalindrome(s, startIndex, i)) { // 是回文子串
                // 获取[startIndex,i]在s中的子串
                String str = s.substring(startIndex, i + 1);
                path.add(str);
            } else {                // 如果不是则直接跳过
                continue;
            }
            dfs(s, i + 1); // 寻找i+1为起始位置的子串
            path.remove(path.size() - 1);        // 回溯过程，弹出本次已经填在的子串
        }

    }

    public boolean isPalindrome(String s, int startIndex, int end) {
        for (int i = startIndex, j = end; i < j; i++, j--) {
            if (s.charAt(i) != s.charAt(j)) {
                return false;
            }
        }
        return true;
    }

}
