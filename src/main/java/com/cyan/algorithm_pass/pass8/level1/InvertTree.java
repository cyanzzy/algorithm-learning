package com.cyan.algorithm_pass.pass8.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 翻转二叉树
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class InvertTree {

//    // 前序遍历
//    public TreeNode invertTree(TreeNode root) {
//        if (root == null) {
//            return null;
//        }
//        // Visit
//        TreeNode temp = root.left;
//        root.left = root.right;
//        root.right = temp;
//
//        invertTree(root.left);
//        invertTree(root.right);
//        return root;
//    }

//    // 后序遍历
//    public TreeNode invertTree(TreeNode root) {
//        if (root == null) {
//            return null;
//        }
//
//        invertTree(root.left);
//        invertTree(root.right);
//        // Visit
//        TreeNode temp = root.left;
//        root.left = root.right;
//        root.right = temp;
//        return root;
//    }

    // 层次遍历
    public TreeNode invertTree(TreeNode root) {
        if (root == null) {
            return null;
        }
        Deque<TreeNode> deque = new LinkedList<>();
        deque.offer(root);
        while (!deque.isEmpty()) {
            TreeNode node = deque.poll();
            // invert
            TreeNode temp = node.left;
            node.left = node.right;
            node.right = temp;

            if (node.left != null) {
                deque.offer(node.left);
            }
            if (node.right != null) {
                deque.offer(node.right);
            }
        }
        return root;
    }

}
