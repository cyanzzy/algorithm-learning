package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayDeque;
import java.util.Queue;

/**
 * 找树左下角的值
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class FindBottomLeftValue {

    public int findBottomLeftValue(TreeNode root) {
        int ret = 0;
        Queue<TreeNode> queue = new ArrayDeque<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            TreeNode p = queue.poll();
            if (p.right != null) {
                queue.offer(p.right);
            }
            if (p.left != null) {
                queue.offer(p.left);
            }
            ret = p.val;
        }
        return ret;
    }

}
