package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 合并两个有序链表
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code3MergeTwoLists {

    // 迭代
    public ListNode mergeTwoLists1(ListNode list1, ListNode list2) {

        // 新链表头节点（虚拟节点）
        ListNode L = new ListNode(0);
        // 保存尾指针
        ListNode tail = L;

        // 尾插法
        while (list1 != null && list2 != null) { // 两个链表都不为空时

            if (list1.val < list2.val) {
                tail.next = list1;
                list1 = list1.next;
            } else if (list1.val > list2.val) {
                tail.next = list2;
                list2 = list2.next;
            } else { // 相等时，两个节点都接上去
                tail.next = list1;
                list1 = list1.next;
                tail = tail.next;
                tail.next = list2;
                list2 = list2.next;
            }
            tail = tail.next;
        }

        while (list1 != null) { // L1 链表未遍历完时
            tail.next = list1;
            list1 = list1.next;
            tail = tail.next;
        }

        while (list2 != null) { // L2 链表未遍历完时
            tail.next = list2;
            list2 = list2.next;
            tail = tail.next;
        }

        return L.next;
    }

    // 迭代优化
    public ListNode mergeTwoLists2(ListNode list1, ListNode list2) {

        // 新链表头节点（虚拟节点）
        ListNode L = new ListNode(0);
        // 保存尾指针
        ListNode tail = L;

        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                tail.next = list1;
                list1 = list1.next;
            } else {
                tail.next = list2;
                list2 = list2.next;
            }
            tail = tail.next;
        }

        // 合并后 list1 和 list2 最多只有一个还未被合并完
        // 我们直接将链表末尾指向未合并完的链表即可
        tail.next = list1 == null ? list2 : list1;

        return L.next;
    }

    // 递归
    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {

        if (list1 == null) {
            return list2;
        } else if (list2 == null) {
            return list1;
        } else if (list1.val < list2.val) {
            list1.next = mergeTwoLists(list1.next, list2);
            return list1;
        } else {
            list2.next = mergeTwoLists(list1, list2.next);
            return list2;
        }
    }

    public static void printList(ListNode head) {
        if (head == null) {
            System.out.println("NULL");
            return;
        }

        while (head != null) {
            System.out.print(head.val);
            System.out.print("->");
            head = head.next;
        }
        System.out.println("null");
    }

    public static void main(String[] args) {
        // L1 = [1,2,4]
        ListNode list1 = new ListNode(1);
        list1.next = new ListNode(2);
        list1.next.next = new ListNode(4);

        // L2 = [1,2,3,4]
        ListNode list2 = new ListNode(1);
        list2.next = new ListNode(2);
        list2.next.next = new ListNode(3);
        list2.next.next.next = new ListNode(4);

//        printList(list1);
//        System.out.println();
//        printList(list2);

        ListNode listNode = new Code3MergeTwoLists().mergeTwoLists(list1, list2);

        printList(listNode);
    }
}
