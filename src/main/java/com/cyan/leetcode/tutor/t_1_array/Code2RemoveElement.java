package com.cyan.leetcode.tutor.t_1_array;

/**
 * 移除元素
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/remove-element/description/
 * @author Cyan Chau
 * @create 2023-02-14, 2023-10-22
 * @update 2024-04-03
 */
public class Code2RemoveElement {

    public int removeElement(int[] nums, int val) {

        // 读写指针
        // 写指针 write 负责写入数据
        // 读指针 read 负责读取数据
        int write = 0, read = 0;

        while(read < nums.length) {
            if (nums[read] != val) {
                nums[write] = nums[read];
                write++;
            }
            read++;
        }
        return write;
    }


    public int remove(int[] nums, int val) {

        /*
         * 题目要求删除数组中等于 val 的元素，数组长度一定小于等于输入数组的长度
         * 使用双指针，当前要处理的元素 cur 指针，指向下一个将要赋值的位置 nextAssignment 指针
         *
         * * 如果 cur 指向的元素不为 val，他一定是输出数组的一个元素，此时就将 cur 指针指向的
         *   元素复制到 nextAssignment 位置，然后将两指针右移
         * * 如果 cur 指向的元素等于 val，它不能在输出数组里，此时 nextAssignment 不动，cur 右移一位
         */


        // nextAssignment 表示下一个要赋值的位置
        int nextAssignment = 0;

        // cur 从头开始遍历，表示当前处理的元素
        for (int cur = 0; cur < nums.length; cur++) {
            // 当 cur != value 说明该元素要留在数组中，那么就将该元素赋值给 nextAssignment
            if (nums[cur] != val) {
                nums[nextAssignment] = nums[cur];
                nextAssignment++;
            }
            // 当 cur == value 说明该元素不能留在数组中，那么 cur 直接右移，而 nextAssignment 不动（因为它即将被赋值）
        }
        return nextAssignment;
    }
}
