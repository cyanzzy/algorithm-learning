package com.cyan.algorithm_pass.pass9.level1;

/**
 * 带有重复元素的二分查找
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class DuplicatedBinarySearch {

    public int binarySearch(int[] nums, int target) {

        if (nums.length == 0 || nums == null) {
            return -1;
        }
        int low = 0, high = nums.length -1;
        while (low <= high) {
            int mid = low + ((high - low) >> 1);
            if (nums[mid] < target) {
                low = mid + 1;
            } else if (nums[mid] > target) {
                high = mid + 1;
            } else {
                // 找到该元素后，继续向左边查找
                while (mid != 0 && nums[mid] == target) {
                    mid--;
                }
                if (mid == 0 && nums[mid] == target) {
                    return mid;
                }
                return mid + 1;
            }
        }
        return -1;
    }
}
