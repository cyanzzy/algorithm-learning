package com.cyan.algorithm_pass.pass14.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * 合并 K 个链表
 *
 * @author Cyan Chau
 * @create 2024-03-25
 */
public class MergeKList {

    // 构造小顶堆
    PriorityQueue<ListNode> queue;

    public ListNode mergeKLists(ListNode[] lists) {

        queue = new PriorityQueue<>(Comparator.comparing(listNode -> listNode.val));

        // lists = [[1,4,5],[1,3,4],[2,6]]
        // 此处的每个 node，其实是 lists 中链表数组的头指针
        for (ListNode node : lists) {
            // for loop：把 1, 1, 2 装入小顶堆，
            // 即将 lists 中每个链表数组中最小元素装入小顶堆
            if (node != null) {
                queue.offer(node);
            }
        }

        // 虚拟节点和尾指针
        ListNode L = new ListNode(0);
        ListNode tail = L;

        while (!queue.isEmpty()) {
            // 弹出（小顶）堆的堆顶元素
            tail.next = queue.poll();
            tail = tail.next;

            // 如果弹出的 top 节点所属的链表还有剩余节点的情况下
            if (tail.next != null) {
                // 将 top 下一个指向节点送入优先队列
                queue.offer(tail.next);
            }
        }
        return L.next;
    }

}
