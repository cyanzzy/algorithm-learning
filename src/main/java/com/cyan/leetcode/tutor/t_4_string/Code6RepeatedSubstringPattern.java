package com.cyan.leetcode.tutor.t_4_string;

/**
 *  KMP 应用
 *  459. 重复的子字符串
 *
 * @accepted Failed
 * @see com.cyan.course_advance.kmp
 * @reference https://cyanzzy.github.io/2023/08/17/%E7%AE%97%E6%B3%95%E6%8F%90%E5%8D%87-2-KMP/
 * @reference https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/solutions/575568/shua-chuan-lc-shuang-bai-po-su-jie-fa-km-tb86/
 * @link https://leetcode.cn/problems/repeated-substring-pattern/
 * @author Cyan Chau
 * @create 2024-04-13
 */
public class Code6RepeatedSubstringPattern {


//    // 枚举法
//    public boolean repeatedSubstringPattern(String s) {
//
//        int n = s.length();
//
//        for (int i = 1; i * 2 <= n; i++) {
//            if (n % i == 0) {
//                boolean match = true;
//                for (int j = i; j < n; j++) {
//                    if (s.charAt(j) != s.charAt(j - i)) {
//                        match = false;
//                        break;
//                    }
//                }
//                if (match) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }

//    // 移动匹配
//    public boolean repeatedSubstringPattern(String s) {
//        // 首先拼接，s + s
//        // 然后丢弃首尾字符
//        return (s + s).indexOf(s, 1) != s.length();
//    }

//    https://leetcode.cn/problems/repeated-substring-pattern/solutions/386481/zhong-fu-de-zi-zi-fu-chuan-by-leetcode-solution/
//    public boolean repeatedSubstringPattern(String s) {
//        // 首先拼接，s + s
//        // 然后 KMP(s + s,s)
//        return kmp(s + s, s) != -1;
//    }
    // KMP
    public int[] getNext(char[] pattern) {
        if (pattern.length == 1) {
            return new int[]{-1};
        }
        int[] next = new int[pattern.length];
        next[0] = -1;
        next[1] = 0;
        int i = 2, j = 0;

        while (i < next.length) {
            if (pattern[i - 1] == pattern[j]) { // 相等，匹配下一个
                next[i++] = ++j;
            } else if (j > 0) {  // 不相等往前跳，继续匹配
                j = next[j];
            } else {
                // 来到最左边情况，说明i之前字符不存在前后缀匹配情况，匹配下一个
                next[i++] = 0;
            }
        }
        return next;
    }

//    public int kmp(String str, String pattern) {
//        if (str == null || pattern == null ||
//                pattern.length() < 1 || str.length() < pattern.length()) {
//            return -1;
//        }
//        char[] mainStr = str.toCharArray();
//        char[] subStr = pattern.toCharArray();
//        int i = 0, j = 0;
//        int[] next = getNext(subStr);
//        while (i < mainStr.length && j < subStr.length) {
//            if (mainStr[i] == subStr[j]) {
//                i++;
//                j++;
//            } else if (next[j] == -1) {
//                // 往前跳到开头都匹配不出来，找主串下一个位置开始匹配
//                i++;
//            } else {
//                // 当前匹配不出来且没有跳到子串开头，往前跳
//                j = next[j];
//            }
//        }
//        return j == subStr.length ? i - j : -1;
//    }

}
