package com.cyan.leetcode.tutor.t_5_stack_queue;

import java.util.Stack;

/**
 * 用栈实现队列
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/implement-queue-using-stacks/
 * @author Cyan Chau
 * @create 2024-04-21
 */
public class Code1MyQueue {

    /**
     * 失败原因：
     * 算法思想没问题，但是 out 栈为空时，需要将 in 栈元素全部出栈并入栈 out 逻辑出错
     * 正确逻辑
     * <code>
     *     if(outStack.isEmpty()) {
     *         while (!inStack.isEmpty()) {
     *            outStack.push(inStack.pop());
     *         }
     *     }
     *
     * </code>
     */
    class MyQueue {

        // 输入栈，负责入队
        private Stack<Integer> inStack;
        // 输出栈，负责出队
        private Stack<Integer> outStack;

        public MyQueue() {
            this.inStack = new Stack<>();
            this.outStack = new Stack<>();
        }

        // 入队：向 in 栈压入元素，如果 in 栈满了，则将 in 栈元素依次出栈并压入 out 栈
        // Java API 栈属于无界的
        public void push(int x) {
            inStack.push(x);
        }

        // 出队：将 out 栈元素依次弹出，如果 out 栈为空，则将 in 栈元素依次出栈并压入 out
        public int pop() {
            if (outStack.isEmpty()) {
                while (!inStack.isEmpty()) {
                    outStack.push(inStack.pop());
                }
            }
            return outStack.pop();
        }

        public int peek() {
            if (outStack.isEmpty()) {
                while (!inStack.isEmpty()) {
                    outStack.push(inStack.pop());
                }
            }
            return outStack.peek();
        }

        public boolean empty() {
            return inStack.isEmpty() && outStack.isEmpty();
        }
    }

/**
 * Your MyQueue object will be instantiated and called as such:
 * MyQueue obj = new MyQueue();
 * obj.push(x);
 * int param_2 = obj.pop();
 * int param_3 = obj.peek();
 * boolean param_4 = obj.empty();
 */
}
