package com.cyan.algorithm_pass.pass14.level3;

import java.util.PriorityQueue;

/**
 * 数据流的中位数
 *
 * @author Cyan Chau
 * @create 2024-03-25
 */
public class MedianFinder {

    // 小顶堆存储较大一半元素
    PriorityQueue<Integer> minHeap;
    // 大顶堆存储较小一半元素
    PriorityQueue<Integer> maxHeap;

    public MedianFinder() {
        // 小顶堆
        this.minHeap = new PriorityQueue<>();
        // 大顶堆
        this.maxHeap = new PriorityQueue<>((a, b) -> (b - a));
    }

    public void addNum(int num) {

        if (minHeap.isEmpty() || num > minHeap.peek()) {
            minHeap.offer(num);
            // 小顶堆元素多，平衡一下
            if (minHeap.size() - maxHeap.size() > 1) {
                maxHeap.offer(minHeap.poll());
            }
        } else {
            maxHeap.offer(num);
            if (maxHeap.size() - minHeap.size() > 0) {
                minHeap.offer(maxHeap.poll());
            }
        }
    }

    public double findMedian() {
        if (minHeap.size() > maxHeap.size()) {
            return minHeap.peek();
        } else if (minHeap.size() < maxHeap.size()) {
            return maxHeap.peek();
        } else {
            return (maxHeap.peek() + minHeap.peek()) / 2.0;
        }
    }
}
