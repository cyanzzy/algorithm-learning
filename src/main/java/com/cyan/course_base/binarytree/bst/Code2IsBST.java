package com.cyan.course_base.binarytree.bst;

import com.cyan.course_base.binarytree.Node;

/**
 * BST: 递归判断
 *
 * @author Cyan Chau
 * @create 2023-03-06
 */
public class Code2IsBST {

    int prev = Integer.MIN_VALUE;

    public boolean isBST(Node head) {
        // 空树是一个BST
        if (head == null) {
            return true;
        }
        // 首先检查左子树，需要返回一个结果
        boolean isLeftBST = isBST(head.left);

        // 如果左子树不符合要求，则返回false (左子树是否满足)
        if (!isLeftBST) {
            return false;
        }
        // 检查当前节点与它左孩子大小 (左子树与当前根节点关系是否满足)
        if (head.val <= prev) {
            // 不满足条件
            return false;
        } else {
            // 更新prev，准备检查右子树
            prev = head.val;
        }
        // 检查其右子树
        return isBST(head.right); // (右子树是否满足)
    }
}
