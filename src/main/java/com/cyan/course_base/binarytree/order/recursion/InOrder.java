package com.cyan.course_base.binarytree.order.recursion;

import com.cyan.course_base.binarytree.Node;

/**
 * 中序遍历 递归版本
 *
 * @author Cyan Chau
 * @create 2023-03-01
 */
public class InOrder {

    public void inorder(Node head) {

        // 递归终止条件
        if (head == null) {
            return;
        }

        // 先访问左子树
        inorder(head.left);

        // 再访问根节点
        // Visit(head);

        // 最后访问右子树
        inorder(head.right);
    }
}
