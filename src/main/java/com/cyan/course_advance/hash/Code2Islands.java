package com.cyan.course_advance.hash;

/**
 * 岛问题
 *
 * @author Cyan Chau
 * @create 2023-08-15
 */
public class Code2Islands {
    /**
     * 岛问题

     * @param m
     * @return
     */
    public static int countIslands(int[][] m) {
        if (m == null || m[0] == null) {
            return 0;
        }
        int N = m.length;
        int M = m[0].length;
        int res = 0;
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < M; j++) {
                if (m[i][j] == 1) {
                    res++;
                    //从当前(i,j)感染并返回结果
                    infect(m, i, j, N, M);
                }
            }
        }
        return res;
    }

    /**
     * 感染函数

     * @param m 矩阵m
     * @param i 当前横坐标
     * @param j 当前纵坐标
     * @param N 矩阵长
     * @param M 矩阵宽
     */
    private static void infect(int[][] m, int i, int j, int N, int M) {
        //违规条件
        if (i < 0 || i >= N || j < 0 || j >= M || m[i][j] != 1) {
            return;
        }
        //当前位置感染
        m[i][j] = 2;
        //向右感染
        infect(m, i + 1, j, N, M);
        //向左感染
        infect(m, i - 1, j, N, M);
        //向下感染
        infect(m, i, j + 1, N, M);
        //向上感染
        infect(m, i, j - 1, N, M);
    }
}