package com.cyan.algorithm_pass.pass3.level1;

/**
 * 合并两个有序数组
 *
 * @author Cyan Chau
 * @create 2024-01-18
 */
public class Code3Merge {

    public void merge(int[] nums1, int m, int[] nums2, int n) {

        int p1 = m - 1, p2 = n - 1;
        int tail = m + n - 1;
        int curr;

        while (p1 >= 0 || p2 >= 0) {

            if (p1 == -1) { // p1 到头
                curr = nums2[p2--];
            } else if (p2 == -1) { // p2 到头
                curr = nums1[p1--];
            } else if (nums1[p1] >= nums2[p2]) {
                curr = nums1[p1--];
            } else {
                curr = nums2[p2--];
            }
            nums1[tail--] = curr;
        }
    }
}
