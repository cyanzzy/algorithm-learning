package com.cyan.course_base.sort;

/**
 * BubbleSort
 *
 *  复盘：
 *  第一个元素和第二个元素比较大小，如果第一个元素较大，则进行交换，否则继续迭代（第一轮）
 *  第二个元素和第三个元素比较大小，如果第二个元素较大，则进行交换，否则继续迭代（第二轮）
 *  .......
 *
 * @author Cyan Chau
 * @create 2023-02-13
 */
public class Code2BubbleSort {

    /**
     * 算法思想：
     *
     * 将第一个关键字与第二个关键字比较， 如果第一个关键字比第二个关键字大，则二者交换位置，否则不变；
     * 第二个关键字与第三个关键字比较，如果第二个关键字比第三个关键字大，则二者交换位置，否则不变…
     * 如此反复，一趟排序完成后，最大的关键字会被交换到最后，经过多趟排序即可使序列有序。
     *
     * @param arr
     */
    public void bubbleSort(int[] arr) {
        // flag表示是否发生交换操作，true表示发生，false表示未发生
        boolean flag = false;
        // 非法情况
        if (arr == null || arr.length < 2) {
            return;
        }
        // 外循环控制无序序列范围，最开始整个序列都是无序的，所以在末尾
        for (int i = arr.length; i > 0; --i) {
            // 未发生交换操作
            flag = false;
            for (int j = 0; j < i; ++j) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    // 发生交换操作
                    flag = true;
                }
            }
            // 一趟排序完成后。未发生交换操作，表示有序，则直接退出排序
            if (flag == false) {
                return;
            }
        }
    }
}
