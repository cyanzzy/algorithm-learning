package com.cyan.algorithm_pass.pass8.level3;

import com.cyan.algorithm_pass.TreeNode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * 最近公共祖先
 *
 * @author Cyan Chau
 * @create 2024-03-08
 */
public class LowestCommonAncestor {

//    Map<Integer, TreeNode> parent = new HashMap<>();
//    Set<Integer> visited = new HashSet<>();
//
//    public void dfs(TreeNode root) {
//        if (root.left != null) {
//            // <node.left, node>
//            parent.put(root.left.val, root);
//            dfs(root.left);
//        }
//        if (root.right != null) {
//            // <node.right, node>
//            parent.put(root.right.val, root);
//            dfs(root.right);
//        }
//    }
//
//    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode p, TreeNode q) {
//
//        // 指定 parent
//        dfs(root);
//        while (p != null) {
//            visited.add(p.val);
//            // find p.parent
//            p = parent.get(p.val);
//        }
//        while (q != null) {
//            if (visited.contains(q.val)) {
//                return q;
//            }
//            q = parent.get(q.val);
//        }
//        return null;
//    }

    public TreeNode lowestCommonAncestor(TreeNode root, TreeNode o1, TreeNode o2) {

        // 如果发现 cur 等于 null，或者 o1、o2，则返回 cur
        if (root == null || root == o1 || root == o2) {
            return root;
        }
        // 检查左子树
        TreeNode left = lowestCommonAncestor(root.left, o1, o2);
        // 检查右子树
        TreeNode right = lowestCommonAncestor(root.right, o1, o2);
        // 如果 left 和 right 都不为空，说明左子树发现过 o1 或 o2，右子树也发现过 o1 或 o2，
        // 说明 o1 向上与 o2 向上的过程中，首次在 cur 相遇，返回 cur
        if (left != null && right != null) {
            return root;
        }

        // 左右两棵树并不都有返回值
        // 如果 left 和 right 有一个为空，另一个不为空，假设不为空的那个记为 node，此时 node 要么是 o1 或 o2 的一个，要么是 o1 和 o2 的最近公共祖先，直接返回 node
        // 如果 left 和 right 都为空，说明 cur 整棵子树没有发现过 o1 或 o2，返回 null
        return left != null ? left : right;
    }

}