package com.cyan.course_base.linkedlist;

/**
 * 链表相交问题
 *
 * @author Cyan Chau
 * @create 2023-02-28
 */
public class Code4LoopList {

    // 单链表是否有环
    public Node getLoopNode(Node head) {
        // 违规条件
        if (head == null || head.next == null) {
            return null;
        }

        // 慢指针
        Node slow = head.next;
        // 快指针
        Node fast  = head.next.next;

        // 慢指针和快指针同步移动
        while (fast != slow) {
            if (fast.next == null || fast.next.next == null) {
                return null;
            }
            fast = fast.next.next;
            slow = slow.next;
        }

        // 当快慢指针相遇时
        fast = head;

        // fast与slow同步移动
        while (fast != slow) {
            fast = fast.next;
            slow = slow.next;
        }

        return slow;
    }

    // 两个无环链表相交
    public Node noLoop(Node head1, Node head2) {
        // 违规条件
        if (head1 == null || head2 == null) {
            return null;
        }
        // cur1
        Node cur1 = head1;
        // cur2
        Node cur2 = head2;
        // len
        int len = 0;

        // len1
        while (cur1.next != null) {
            len++;
            cur1 = cur1.next;
        }

        // len2
        while (cur2.next != null) {
            len--;
            cur2 = cur2.next;
        }

        // 判断
        if (cur1 != cur2) {
            return null;
        }

        // cur1选择长的链表
        cur1 = len > 0 ? head1 : head2;

        // cur2选择短的链表
        cur2 = cur1 == head1 ? head2 : head1;

        // len
        len = Math.abs(len);

        // 移动cur1便于下一步同步移动
        while (len != 0) {
            len--;
            cur1 = cur1.next;
        }

        // 两个指针同步移动
        while (cur1 != cur2) {
            cur1 = cur1.next;
            cur2 = cur2.next;
        }
        return cur1;
    }

    // 两条有环链表相交
    public Node bothLoop(Node head1, Node head2, Node loop1, Node loop2){
        // 定义两个哨兵
        Node cur1 = null;
        Node cur2 = null;

        // 图示1情况
        if (loop1 == loop2) {
            cur1 = head1;
            cur2 = head2;
            int len = 0;
            while (cur1 != loop1) {
                len++;
                cur1 = cur1.next;
            }
            while (cur2 != loop2) {
                len--;
                cur2 = cur1.next;
            }
            cur1 = len > 0 ? head1 : head2;
            cur2 = cur1 == head1 ? head2 : head1;
            len = Math.abs(len);
            while (len != 0) {
                len--;
                cur1 = cur1.next;
            }
            while (cur1 != cur2) {
                cur1 = cur1.next;
                cur2 = cur2.next;
            }
            return cur1;
        } else { // 图示2和3
            cur1 = loop1.next;
            while (cur1 != loop1) {
                if (cur1 == loop2) { // 图3
                    return loop1;
                }
                cur1 = cur1.next;
            }
            return null;
        }
    }

    // main
    public Node getIntersectNode(Node head1, Node head2) {

        // 违规条件
        if (head1 == null || head2 == null) {
            return null;
        }

        // 判断第一个链表有没有环
        Node loop1 = getLoopNode(head1);

        // 判断第一个链表有没有环
        Node loop2 = getLoopNode(head2);

        // 情况1
        if (loop1 == null && loop2 == null) {
            return noLoop(head1, head2);
        }

        // 情况2/3
        if (loop1 != null && loop2 != null) {
            return bothLoop(head1, loop1, head2, loop2);
        }

        return null;
    }
}
