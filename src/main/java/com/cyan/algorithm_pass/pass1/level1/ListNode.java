package com.cyan.algorithm_pass.pass1.level1;


/**
 * Linked List
 *
 * @author Cyan Chau
 * @create 2023-12-04
 */
public class ListNode {

    private int data;
    private ListNode next;

    public ListNode(int data) {
        this.data = data;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }

    public ListNode getNext() {
        return next;
    }

    public void setNext(ListNode next) {
        this.next = next;
    }

    public int getListLength(ListNode head) {
        int length = 0;
        ListNode node = head;

        while (node != null) {
            length++;
            node = node.next;
        }
        return length;
    }

    /**
     * 链表插入
     *
     * @param head 链表头节点
     * @param nodeInserted 待插入节点
     * @param position 待插入位置，从 1 开始
     * @return 插入后得到的链表头节点
     */
    public ListNode insertListNode(ListNode head, ListNode nodeInserted, int position) {
        // 边界判断
        if (head == null) {
            return nodeInserted;
        }
        // 已经存放的元素个数
        int size = getListLength(head);
        // 边界判断
        if (position > size + 1 || position < 1) {
            System.out.println("位置参数越界");

            return head;
        }
        // 头部插入
        if (position == 1) {
            nodeInserted.next = head;
            head = nodeInserted;

            return head;
        }
        ListNode pNode = head;
        int count = 1;
        // 由于 position 被 size 限制住，无需考虑 pNode = null
        while (count < position - 1) { // 寻找插入位置
            pNode = pNode.next;
            count++;
        }
        // 插入节点
        nodeInserted.next = pNode.next;
        pNode.next = nodeInserted;

        return head;
    }

    /**
     * 删除节点
     *
     * @param head 链表头节点
     * @param position 待删除节点位置，从 1 开始
     * @return 删除后的链表头节点
     */
    public ListNode deleteListNode(ListNode head, int position) {
        // 边界判断
        if (head == null) {
            return null;
        }
        int size = getListLength(head);
        // 边界判断
        if (position > size || position < 1) {
            System.out.println("位置越界");
            return head;
        }

        // 头部删除
        if (position == 1) {
            return head.next;
        } else { // 其他情况
            ListNode cur = head;
            int count = 1;
            while (count < position - 1) { // 寻找删除位置
                cur = cur.next;
                count++;
            }
            // 删除节点
            ListNode curNode = cur.next;
            cur.next = curNode.next;
        }

        return head;
    }
}
