package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 自底向上的层次遍历
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class LevelOrderBottom {

    public List<List<Integer>> levelOrderBottom(TreeNode root) {

        // 存储结果
        List<List<Integer>> levelOrder = new LinkedList<>();
        // base case
        if (root == null) {
            return levelOrder;
        }
        // 队列
        Queue<TreeNode> queue = new LinkedList<>();
        // 入队根元素
        queue.offer(root);
        // 当队列不为空时
        while (!queue.isEmpty()) {
            // 当前层次
            List<Integer> level = new ArrayList<>();
            // 当前队列长度（当前层元素个数）
            int currentLevelSize = queue.size();
            // 依次取 currentLevelSize 元素拓展
            for (int i = 0; i < currentLevelSize; i++) {
                TreeNode node = queue.poll();
                level.add(node.val);
                TreeNode left = node.left, right = node.right;
                if (left != null) {
                    queue.offer(left);
                }
                if (right != null) {
                    queue.offer(right);
                }
            }
            // 在遍历完一层节点之后，将存储该层节点值的列表添加到结果列表的头部
            levelOrder.add(0, level);
        }
        return levelOrder;
    }

}
