package com.cyan.course_base.linkedlist;

/**
 * @author Cyan Chau
 * @create 2023-02-23
 */
public class Node {

    public Integer val;

    public Node next;

    public Node() {
    }

    public Node(Integer val) {
        this.val = val;
    }

    public Node(Integer val, Node next) {
        this.val = val;
        this.next = next;
    }
}
