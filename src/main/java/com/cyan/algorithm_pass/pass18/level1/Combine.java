package com.cyan.algorithm_pass.pass18.level1;

import java.util.ArrayList;
import java.util.List;

/**
 * 组合
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class Combine {

    // ******************************** N 叉树版本
//    List<List<Integer>> result; // 根据测试用例定义存储最终答案的集合
//    List<Integer> path; // 定义存储每条遍历分支的答案集合
//
//    public List<List<Integer>> combine(int n, int k) {
//        result = new ArrayList<>();
//        path = new ArrayList<>();
//        dfs(n, k, 1);
//        return result;
//    }
//
//    public void dfs(int n, int k, int startIndex) {
//        if (path.size() == k) {
//            result.add(new ArrayList<>(path));
//        }
//        for (int i = startIndex; i <= n; i++) {
//            path.add(i);
//            dfs(n, k, i + 1);
//            path.remove(path.size()-1);
//        }
//    }


    // 剪枝优化
    List<List<Integer>> result; // 根据测试用例定义存储最终答案的集合
    List<Integer> path; // 定义存储每条遍历分支的答案集合

    public List<List<Integer>> combine(int n, int k) {
        result = new ArrayList<>();
        path = new ArrayList<>();
        dfs(n, k, 1);
        return result;
    }

    public void dfs(int n, int k, int startIndex) {
        // 递归终止条件
        if (path.size() == k) {
            result.add(new ArrayList<>(path));
            return;
        }
        // 单层遍历过程
        for (int i = startIndex; i <= n - (k - path.size()) + 1; i++) {
            path.add(i);
            dfs(n, k, i + 1);
            path.remove(path.size() - 1);
        }
    }
    // ****************************************** 二叉树版本
//    List<List<Integer>> ans;
//    List<Integer> temp;
//
//    public void dfs(int index, int n, int k) {
//        // 答案数组满足长度k返回答案
//        if (temp.size() == k) {
//            ans.add(new ArrayList<>(temp));
//            return;
//        }
//        // 递归终止条件
//        if (index == n + 1) {
//            return;
//        }
//        // 考虑当前位置的选择
//        temp.add(index);
//        dfs(index + 1, n, k);
//        temp.remove(temp.size() - 1);
//
//        // 不考虑当前位置的选择
//        dfs(index + 1, n, k);
//    }
//
//    public List<List<Integer>> combine(int n, int k) {
//        ans = new ArrayList<>();
//        temp = new ArrayList<>();
//        dfs(1, n, k);
//        return ans;
//    }
    // 优化版本
//    public void dfs(int index, int n, int k) {
//        // 剪枝：temp 长度加上区间 [index, n] 的长度小于 k，不可能构造出长度为 k 的 temp
//        if (temp.size() + (n - index + 1) < k) {
//            return;
//        }
//        // 记录合法的答案
//        if (temp.size() == k) {
//            ans.add(new ArrayList<Integer>(temp));
//            return;
//        }
//        // 考虑选择当前位置
//        temp.add(index);
//        dfs(index + 1, n, k);
//        temp.remove(temp.size() - 1);
//
//        // 考虑不选择当前位置
//        dfs(index + 1, n, k);
//    }

}
