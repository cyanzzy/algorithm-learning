package com.cyan.algorithm_pass.pass3.level3;

import java.util.HashSet;
import java.util.Set;

/**
 * 只出现一次的数字
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code2SingleNumber {

    public int singleNumber(int[] nums) {

        Set<Integer> set = new HashSet<>();
        for (int num : nums) {
            // 当元素第一次出现，添加进集合，set.add(num) 返回 true
            // 当元素第二次出现时，此时集合中已经有了该元素，set.add(num) 返回 false
            if (!set.add(num)) { // if 当且仅当 true 才执行，因此取反
                // 将第二次出现的元素干掉
                set.remove(num);

            }
        }
        // 处理边界
        if (set.size() == 0) {
            return 0;
        }

        return set.toArray(new Integer[0])[0];

    }

    public int singleNumber1(int[] nums) {
        int single = 0;
        for (int num : nums) {
            single ^= num;
        }
        return single;
    }

}
