package com.cyan.course_base.binarytree.order.recursion;

import com.cyan.course_base.binarytree.Node;

/**
 * 先序遍历 递归版本
 *
 * @author Cyan Chau
 * @create 2023-03-01
 */
public class PreOrder {

    public void preorder(Node head) {

        // 递归终止条件
        if (head == null) {
            return;
        }
        // 访问树节点
       // Visit(head);

        // 遍历左子树
        preorder(head.left);

        // 遍历右子树
        preorder(head.right);
    }
}
