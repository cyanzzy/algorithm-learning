package com.cyan.leetcode.tutor.t_4_string;

/**
 * 反转字符串Ⅱ
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/reverse-string-ii/
 * @author Cyan Chau
 * @create 2024-04-13
 */
public class Code2ReverseStr {



    // 官方答案
    public String reverseStr(String s, int k) {
        int n = s.length();
        char[] arr = s.toCharArray();
        for (int i = 0; i < n; i += 2 * k) {
            reverse(arr, i, Math.min(i + k, n) - 1);
        }
        return new String(arr);
    }

    public void reverse(char[] arr, int left, int right) {
        while (left < right) {
            char temp = arr[left];
            arr[left] = arr[right];
            arr[right] = temp;
            left++;
            right--;
        }
    }


    // 本次作答
//    public String reverseStr(String s, int k) {
//
//        char[] ch = s.toCharArray();
//        for (int i = 0; i < ch.length; i +=2 * k) {
//            if (ch.length- i < k) {
//                revere(ch, i, ch.length - 1);
//            }
//            else revere(ch, i, i+k-1);
//        }
//        StringBuilder sb = new StringBuilder();
//        for (char c : ch) {
//            sb.append(c);
//        }
//        return sb.toString();
//    }
//
//    public void revere(char[] str, int begin, int end) {
//        for (int i = begin, j= end; i < j; i++, j--) {
//            char tempChar = str[i];
//            str[i] = str[j];
//            str[j] = tempChar;
//        }
//    }
}
