package com.cyan.algorithm_pass.pass3.level2;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

/**
 * 汇总区间
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code5SummaryRanges {

    public List<String> summaryRanges1(int[] nums) {

        List<String> res = new ArrayList<>();
        int curr = 0;
        while (curr < nums.length) {
            int low = curr;
            curr++;
            while (curr < nums.length && nums[curr] == nums[curr - 1] + 1) {
                curr++;
            } // 寻找连续序列
            int high = curr - 1;
            StringBuilder temp = new StringBuilder(Integer.toString(nums[low]));
            if (low < high) {
                temp.append("->");
                temp.append(Integer.toString(nums[high]));
            }
            res.add(temp.toString());
        }
        return res;
    }

    public List<String> summaryRanges(int[] nums) {

        List<String> res = new ArrayList<>();
        int slow = 0, fast = 0;

        while (fast < nums.length) {
            if (fast + 1 == nums.length || nums[fast] + 1 != nums[fast + 1]) {
                StringBuilder temp = new StringBuilder(Integer.toString(nums[slow]));
                if (slow != fast) {
                    temp.append("->").append(Integer.toString(nums[fast]));
                }
                res.add(temp.toString());
                // 更新为下一个区间起始位置
                slow = fast + 1;
            }
            fast++;
        }
        return res;
    }
}