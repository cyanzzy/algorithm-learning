package com.cyan.algorithm_pass.pass8.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.LinkedList;
import java.util.List;

/**
 * 二叉树的所有路径
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class BinaryTreePaths {

    /*  输入：root = [1,2,3,null,5]
        输出：["1->2->5","1->3"]
     */
    public List<String> binaryTreePaths(TreeNode root) {
        // 所有路径
        List<String> paths = new LinkedList<>();
        dfs(root, "", paths);
        return paths;
    }

    /**
     * dfs
     *
     * @param node node
     * @param path 单条路径
     * @param paths 所有路径
     */
    public void dfs(TreeNode node, String path, List<String> paths) {
        if (node == null) { // base case
            return;
        }
        // 叶子节点
        if (node.left == null && node.right == null) {
            paths.add(path + node.val);
            return;
        }

        dfs(node.left, path + node.val + "->", paths);
        dfs(node.right, path + node.val + "->", paths);
    }
}
