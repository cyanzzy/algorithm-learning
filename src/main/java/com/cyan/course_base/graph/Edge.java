package com.cyan.course_base.graph;

/**
 * @author Cyan Chau
 * @create 2023-03-27
 */
public class Edge {

    // 边的权值
    public int weight;

    // 边的起始节点
    public Node from;

    // 边的终止节点
    public Node to;

    public Edge(int weight, Node from, Node to) {
        this.weight = weight;
        this.from = from;
        this.to = to;
    }
}
