package com.cyan.algorithm_pass.pass9.level2;

/**
 * 山脉数组的峰顶索引
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class PeakIndexInMountainArray {

//    // 枚举
//    public int peakIndexInMountainArray(int[] arr) {
//        int ans = -1;
//        for (int i = 0; i < arr.length; i++) {
//            if (arr[i] > arr[i + 1]) {
//                ans = i;
//                break;
//            }
//        }
//        return ans;
//    }

    //二分
    public int peakIndexInMountainArray(int[] arr) {
        int left = 0, right = arr.length -1, ans = -1;
        while (left <= right) {
            int mid = left + ((right - left) >> 1);
            if (arr[mid] > arr[mid + 1]) {
                // 找到第一个  arr[mid] 使 arr[mid] > arr[mid + 1]
                ans = mid;
                // 继续向左找
                right = mid - 1;
            } else { // 向右找
                left = mid + 1;
            }
        }
        return ans;
    }
}
