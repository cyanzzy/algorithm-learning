package com.cyan.course_base.brute_force_recursion;


/**
 * 数字字符串转字母组合的方法数
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code4NumberOfCombinations {

    /**
     * process表示来到 i 位置时返回的组合总数
     *
     * @param str
     * @param i str[0..i-1]表示已经转换完成，str[i..]表示还没转换的情况下
     * @return 最终返回答案
     */
    public int process(char[] str, int i) {

        if (i == str.length) { // 递归终止条件
            // 表示str[0..n-1]转换完毕，没有后续字符了，答案只有一种
            return 1;
        }

        if (str[i] == '0') {
            // 遇到了0，之前转换成的全部作废，进入无效状态
            return 0;
        }

        // 以'1'开头后续的答案
        if (str[i] == '1') {
            // 当前i是以1开头，其一是以str[i]构成个位数的答案
            int res = process(str, i + 1);
            // 当前i是以1开头，其二是以str[i]和str[i+1]构成两位数的答案
            if (i + 1 < str.length) {
                res += process(str, i + 2);
            }
            return res;
        }

        // 以'2'开头后续的答案
        if (str[i] == '2') {
            // 当前i是以2开头，其一是以str[i]构成个位数的答案
            int res = process(str, i + 1);
            // 当前i是以2开头，其二是在以str[i]和str[i+1]构成两位数不超过26情况下构成另一种答案
            if (i + 1 < str.length && (str[i + 1] >= '0' && str[i + 1] <= '6')) {
                res += process(str, i + 2);
            }
            return res;
        }

        // 剩余的情况是以3-9开头只有一种答案
        return process(str, i + 1);
    }
}
