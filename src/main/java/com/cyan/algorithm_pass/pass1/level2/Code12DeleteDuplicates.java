package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 删除排序链表中的重复元素（重复元素都不要）
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code12DeleteDuplicates {

    public ListNode deleteDuplicates(ListNode head) {

        // base case
        if (head == null) {
            return head;
        }

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;

        ListNode cur = dummyNode;

        while (cur.next != null && cur.next.next != null) {
            if (cur.next.next.val == cur.next.val) {
                int x = cur.next.val;
                // delete all duplicates
                while (cur.next != null && cur.next.val == x) {
                    cur.next = cur.next.next;
                }
            } else {
                cur = cur.next;
            }
        }

        return dummyNode.next;

    }
}
