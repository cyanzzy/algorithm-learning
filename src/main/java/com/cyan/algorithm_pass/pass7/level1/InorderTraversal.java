package com.cyan.algorithm_pass.pass7.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 中序遍历
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class InorderTraversal {

    public List<Integer> inorderTraversal(TreeNode root) {

        List<Integer> ans = new ArrayList<>();
        inorder(root, ans);
        return ans;
    }

    public void inorder(TreeNode root, List<Integer> ans) {

        if (root == null) {
            return;
        }
        inorder(root.left, ans);
        ans.add(root.val);
        inorder(root.right, ans);
    }
}
