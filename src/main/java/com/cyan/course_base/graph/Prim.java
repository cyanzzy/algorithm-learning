package com.cyan.course_base.graph;

import java.util.Comparator;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

/**
 * @author Cyan Chau
 * @create 2023-04-04
 */
public class Prim {

    // 定义比较器
    public class EdgeComparator  implements Comparator<Edge> {
        @Override
        public int compare(Edge o1, Edge o2) {
            return o1.weight - o2.weight;
        }
    }

    // 邻接表实现
    public Set<Edge> prim(Graph graph) {
        // 存储边集合的小根堆
        PriorityQueue<Edge> priorityQueue = new PriorityQueue<>(new EdgeComparator());
        HashSet<Node> set = new HashSet<>();
        HashSet<Edge> result = new HashSet<>();

        for (Node node : graph.nodes.values()) {
            // 如果node没有访问过
            if (!set.contains(node)) {
                set.add(node);
                // 将node相连的边压入优先队列
                for (Edge edge : node.edges) {
                    priorityQueue.add(edge);
                }
                while (!priorityQueue.isEmpty()) {
                    // 弹出与node相连权值最小边
                    Edge edge = priorityQueue.poll();
                    // 选择该边的邻接点
                    Node toNode = edge.to;
                    if (!set.contains(toNode)) {
                        // 将toNode标记为已访问
                        set.add(toNode);
                        // 将选中的权值最小边纳入result
                        result.add(edge);
                        // 从加入到最小生成树的节点出发，将其相连的边放入优先队列
                        for (Edge nextEdge : toNode.edges) {
                            priorityQueue.add(nextEdge);
                        }
                    }
                }
            }
        }
        return result;
    }

    // 邻接矩阵实现
    public int Prim(int[][] graph) {
        int size = graph.length;
        // distances存储边权值的最小值
        int[] distances = new int[size];
        // 标记是否加入到最小生成树中
        boolean[] visit = new boolean[size];
        // 初始化
        visit[0] = true;
        // distances初始化成0到各顶点的权值
        for (int i = 0; i < size; i++) {
            distances[i] = graph[0][i];
        }
        // 定义最小代价
        int sum = 0;
        for (int i = 0; i < size; i++) {
            // 存储路径权值最小值
            int minPath = Integer.MAX_VALUE;
            // 存储顶点编号
            int minIndex = -1;
            // 从distances中选权值最小的边
            for (int j = 0; j < size; j++) {
                if (!visit[j] && distances[j] < minPath) {
                    minPath = distances[j];
                    minIndex = j;
                }
            }
            if (minIndex == -1) {
                return sum;
            }
            // 顶点为minIndex节点加入最小生成树
            visit[minIndex] = true;
            sum += minPath;
            // 更新distances数组
            for (int j = 0; j < size; j++) {
                // 如果当前顶点没有加入生成树且minIndex到j的距离小于原先distances[j]的距离
                if (!visit[j] && distances[j] > graph[minIndex][j]) {
                    distances[j] = graph[minIndex][j];
                }
            }
        }
        return sum;
    }
}
