package com.cyan.algorithm_pass.pass12.level3;

/**
 * 压缩字符串
 *
 * @author Cyan Chau
 * @create 2024-03-22
 */
public class Compress {

    public int compress(char[] chars) {
        int write = 0, left = 0;

        for (int read = 0; read < chars.length; read++) {
            // ['a', ..., 'a']
            if (read == chars.length - 1 // 末尾
                    || chars[read] != chars[read + 1]) {// 重复字符的最右侧
                // ['a']
                chars[write++] = chars[read];
                int num = read - left + 1;
                // num = 12
                if (num > 1) {
                    int anchor = write;
                    while (num > 0) {
                        // ['a', '2', '1']
                        chars[write++] = (char) (num % 10 + '0');
                        num /= 10;
                    }
                    // Reverse ['2', '1']
                    reverse(chars, anchor, write - 1);
                }
                left = read + 1;
            }
        }
        return write;
    }

    public void reverse(char[] chars, int left, int right) {
        while (left < right) {
            char temp = chars[left];
            chars[left] = chars[right];
            chars[right] = temp;
            left++;
            right--;
        }
    }

}
