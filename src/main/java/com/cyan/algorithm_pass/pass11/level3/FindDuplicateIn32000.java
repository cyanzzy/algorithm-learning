package com.cyan.algorithm_pass.pass11.level3;


/**
 * 海量数据中寻找重复元素
 *
 * @author Cyan Chau
 * @create 2024-03-12
 */
public class FindDuplicateIn32000 {



    class BitSet {

        int[] bits;

        public BitSet(int size) {
            this.bits = new int[size >> 5];
        }

        // 判断 pos 位置的数是否出现过
        boolean get(int pos) {
            // / 32
            int posBit = (pos >> 5);
            // % 32
            int bitNumber = (pos & 0x1F);
            return (bits[posBit] & (1 << bitNumber)) != 0;
        }


        // 将 pos 位置的值设置为 1
        void set(int pos) {
            // / 32
            int posBit = (pos >> 5);
            // % 32
            int bitNumber = (pos & 0x1F);
            bits[posBit] = bits[posBit] | (1 << bitNumber);
        }

    }

    public void checkDuplicatedNum(int[] number) {
        BitSet bitset = new BitSet(32000);

        for (int i = 0; i < number.length; i++) {
            int num = number[i]; // 数组范围 [1, N]
            int postion = num - 1; // num 在 bitmap 数组下标

            if (bitset.get(postion)) {
                // Visit()
                System.out.println(num);
            } else { // 第一次出现
                bitset.set(postion);

            }

        }
    }

}
