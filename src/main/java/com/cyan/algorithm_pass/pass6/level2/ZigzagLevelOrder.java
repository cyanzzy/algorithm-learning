package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.*;

/**
 * 锯齿形层次遍历
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class ZigzagLevelOrder {

    public List<List<Integer>> zigzagLevelOrder(TreeNode root) {

        // 结果
        List<List<Integer>> ans = new LinkedList<>();
        // base case
        if (root == null) {
            return ans;
        }
        // 队列
        Queue<TreeNode> queue = new ArrayDeque<>();
        // 入队根元素
        queue.offer(root);
        // 奇偶标记
        boolean isOrderLeft = true;

        while (!queue.isEmpty()) {
            // 当前层次
            Deque<Integer> levelList = new LinkedList<>();
            // 当前队列长度（当前层元素个数）
            int currentLevelSize = queue.size();
            for (int i = 0; i < currentLevelSize; i++) {
                TreeNode curNode = queue.poll();
                if (isOrderLeft) {
                    levelList.offerLast(curNode.val);
                } else {
                    levelList.offerFirst(curNode.val);
                }
                if (curNode.left != null) {
                    queue.offer(curNode.left);
                }
                if (curNode.right != null) {
                    queue.offer(curNode.right);
                }
            }
            // 将当前层次元素装入 ans
            ans.add(new LinkedList<>(levelList));
            // 奇偶反转
            isOrderLeft = !isOrderLeft;
        }
        return ans;
    }
}
