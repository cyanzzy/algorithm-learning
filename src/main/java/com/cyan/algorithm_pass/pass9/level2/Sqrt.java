package com.cyan.algorithm_pass.pass9.level2;

/**
 * 求平方根
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class Sqrt {

    public int sqrt(int num) {

        int left = 0, right = num, ans = - 1;

        while (left <= right) {
            int mid = left + ((right - left) >> 1);

            if ((long) mid * mid <= num) {
                ans = mid;
                left = mid + 1;
            } else {
                right = mid - 1;
            }
        }
        return ans;
    }
}
