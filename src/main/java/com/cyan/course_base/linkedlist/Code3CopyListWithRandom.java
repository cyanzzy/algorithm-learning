package com.cyan.course_base.linkedlist;

import java.util.HashMap;

/**
 * 复制带有随机节点的链表
 *
 * @author Cyan Chau
 * @create 2023-02-27
 */
public class Code3CopyListWithRandom {

    class Node {
        int value;
        Node next;
        Node rand;
        Node(int val) {
            value = val;
        }
    }

    public Node copyListWithRandom(Node head) {
        // 构建原始节点映射至克隆节点的哈希表
        HashMap<Node, Node> map = new HashMap<>();
        Node cur = head;

        // 拷贝原始节点
        while (cur != null) {
            // <原始节点，原始节点对应的拷贝节点>
            map.put(cur, new Node(cur.value));
            cur = cur.next;
        }

        cur = head;

        while (cur != null) {
            // 拷贝节点的next域指向原始节点的next域对应的拷贝节点
            map.get(cur).next = map.get(cur.next);
            // 拷贝节点的rand域指向原始节点rand域对应的拷贝节点
            map.get(cur).rand = map.get(cur.rand);
            cur = cur.next;
        }

        return map.get(head);
    }

}
