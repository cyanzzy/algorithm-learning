package com.cyan.leetcode.tutor.t_3_hash;

import java.util.HashMap;
import java.util.Map;

/**
 * 四数之和 Ⅱ
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/4sum-ii/description/
 * @author Cyan Chau
 * @create 2023-03-27
 * update 2024-04-11
 */
public class Code5FourSumCount {

    public int fourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {

        // 1. 将AB分为一组，CD分为一组
        int ans = 0;
        HashMap<Integer, Integer> mapAB = new HashMap<>();

        // 2. 对于A和B，得到所有A[i]+B[j]的值，映射进哈希表
        for (int i : nums1) {
            for (int j : nums2) {
                // <AB元素和,频次>
                // TODO answer 要求返回四个数索引组合个数，因此返回频次即可
                mapAB.put(i + j, mapAB.getOrDefault(i + j, 0) + 1);
            }
        }

        // 3. 对于C和D，计算C[k]+D[l]
        for (int k : nums3) {
            for (int l : nums4) {
                if (mapAB.containsKey(-(k + l))) {
                    ans += mapAB.get(-(k+l));
                }
            }
        }

        return ans;
    }

    /**
     * Reason for failed
     *
     * 已知目标 A+B == -(C+D)，求组合出现次数
     * 关键点：通过 Map 记录 A + B，出现频次；分组
     * 当计算 C+D 时，判断是否能从 Map 中获取 -(C+D)，如果满足条件，可以直接拿到出现次数
     */
    public int reFourSumCount(int[] nums1, int[] nums2, int[] nums3, int[] nums4) {

        int ans = 0;
        Map<Integer, Integer> map = new HashMap<>();

        // AB一组，CD另一组，将A+B存入map
        // 每个键表示一种 A[i]+B[j]，对应的值为 A[i]+B[j] 出现的次数
        for (int a : nums1) {
            for (int b : nums2) {
                map.put(a + b,map.getOrDefault(a + b, 0) + 1);
            }
        }
        // 对于 CD 组，每次二重循环遍历计算 C+D，并判断 C+D == -(A+B)
        for (int c : nums3) {
            for (int d : nums4) {
                if (map.containsKey(-(c + d))) {
                    ans += map.get(-(c + d));
                }
            }
        }

        return ans;

    }

}
