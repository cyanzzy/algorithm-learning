package com.cyan.course_base.binarytree.bst;

import com.cyan.course_base.binarytree.Node;

import java.util.LinkedList;
import java.util.List;

/**
 * BST: 遍历中序序列
 *
 * @author Cyan Chau
 * @create 2023-03-03
 */
public class Code1IsBST {

    public boolean isBST(Node head) {
        // 空树符合要求
        if (head == null) {
            return false;
        }

        List<Node> inList = new LinkedList<>();

        process(head, inList);

        int pre = Integer.MIN_VALUE;

        // 如果中序遍历不是递增的，不是二叉搜索树
        for (Node node: inList) {
            if (pre >= node.val) {
                return false;
            }
            pre = node.val;
        }
        return true;
    }

    public void process(Node node, List<Node> inList) {
        if (node == null) {
            return;
        }
        process(node.left, inList);
        inList.add(node);
        process(node.right, inList);
    }
}
