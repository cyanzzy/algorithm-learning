package com.cyan.algorithm_pass.pass9.level1;

/**
 * 二分查找
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class BinarySearch {

//    // 迭代方式
//    public int binarySearch(int[] t_1_array, int low, int high, int target) {
//
//        while (low <= high) {
//            int mid = low + ((high - low) >> 1);
//            if (t_1_array[mid] == target) {
//                return mid;
//            } else if (t_1_array[mid] > target) {
//                high = mid -1;
//            } else {
//                low = mid + 1;
//            }
//        }
//        return -1;
//    }

    // 递归方式
    public int binarySearch(int[] array, int low, int high, int target) {

        if (low <= high) {
            int mid = low + ((high - low) >> 1);
            if (array[mid] == target) {
                return mid;
            } else if (array[mid] > target) {
                return binarySearch(array, low, mid - 1, target);
            } else {
                return binarySearch(array, mid + 1, high, target);
            }
        }
        return -1;
    }
}
