package com.cyan.course_base.linkedlist;

import java.util.Stack;

/**
 * 链表回文
 *
 * @author Cyan Chau
 * @create 2023-02-23
 */
public class Code1IsPalindrome {

    // 方式1
    public boolean isPalindrome1(Node head) {
        if (head == null || head.next == null) {
            return true;
        }
        // 定义栈
        Stack<Node> stack = new Stack<Node>();

        // 定义右指针
        Node right = head.next;
        // 定义当前指针
        Node cur = head;

        while (cur.next != null || cur.next.next != null) {
            // 慢指针步长为1
            right = right.next;
            // 快指针步长为2
            cur = cur.next.next;
        }

        // 将链表的后半部分放入栈
        while (right != null) {
            stack.push(right);
            right = right.next;
        }

        // 判断
        while (!stack.isEmpty()) {
            if (head.val != stack.pop().val) {
                return false;
            }
            head = head.next;
        }

        return true;
    }

    // 方式2
    public boolean isPalindrome2(Node head) {
        if (head == null || head.next == null) {
            return true;
        }

        // 定义慢指针n1
        Node slowNode = head;
        // 定义快指针n2
        Node fastNode = head;

        /*
          当整个循环结束后慢指针s位于中点，快指针f位于null
               S
            1->2->3->4->5
                          f

         */

        // 当快指针走到链表末尾，慢指针便到达链表中点
        while (fastNode.next != null && fastNode.next.next != null) {
            // 慢指针步长为1
            slowNode = slowNode.next;
            // 快指针步长为2
            fastNode = fastNode.next.next;
        }

        // 快指针此时指向慢指针右半部分的第一个节点
        fastNode = slowNode.next;
        // 中间节点的后继断链
        slowNode.next = null;
        // 定义辅助节点n3
        Node tempNode = null;

        /*
          快指针此时指向慢指针右半部分的第一个节点,中间节点的后继断链
                  S
            1->2->3  4->5
                     f

         */
        // 链表右半部分转置
        while (fastNode != null) {
            // n3保存后继节点
            tempNode = fastNode.next;
            // 开始转置
            fastNode.next = slowNode;
            slowNode = fastNode;
            fastNode = tempNode;
        }
        // n3指向转置后的链表的（从左至右）最后一个结点
        tempNode = slowNode;
        // n2指向转置后的链表的（从左至右）第一个结点
        fastNode = head;
        boolean res = true;

        // 回文判断
        while (slowNode != null && fastNode != null) {
            if (slowNode.val != fastNode.val) {
                res = false;
                break;
            }
            // 从右到左
            slowNode = slowNode.next;
            // 从左到右
            fastNode = fastNode.next;
        }

        slowNode = tempNode.next;
        tempNode.next = null;

        // 复原链表
        while (slowNode != null) {
            fastNode = slowNode.next;
            slowNode.next = tempNode;
            tempNode = slowNode;
            slowNode = fastNode;
        }
        return res;
    }
}
