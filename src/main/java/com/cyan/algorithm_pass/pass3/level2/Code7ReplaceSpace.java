package com.cyan.algorithm_pass.pass3.level2;

/**
 * 字符串替换
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code7ReplaceSpace {

    public String replaceSpace(StringBuffer str) {

        if (str == null) {
            return null;
        }
        // count 统计空格数量
        int count = 0;
        // 旧字符串长度
        int oldLen = str.length();
        // 统计空格数量
        for (int i = 0; i < oldLen; i++) {
            if (str.charAt(i) == ' ') {
                count++;
            }
        }
        // 新字符串长度
        int newLen = oldLen + count * 2;
        str.setLength(newLen);
        // 快指针指向旧字符串末尾
        int fast = oldLen - 1;
        // 慢指针指向新字符串末尾
        int slow = newLen - 1;

        while (fast >= 0 && slow > fast) {
            char ch = str.charAt(fast);
            if (ch == ' ') {
                fast--;
                str.setCharAt(slow--, '0');
                str.setCharAt(slow--, '2');
                str.setCharAt(slow--, '%');
            } else { // 若快指针指向的不是空格，则将其复制到慢指针位置，然后快慢指针同时向前移动 1 步长
                str.setCharAt(slow, ch);
                fast--;
                slow--;
            }
        }
        return str.toString();
    }

    public static void main(String[] args) {
        StringBuffer stringBuffer = new StringBuffer("We Are Happy");

        String str = new Code7ReplaceSpace().replaceSpace(stringBuffer);
        System.out.println(str);
    }
}
