package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 删除链表的倒数第 N 个节点
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code10RemoveNthFromEnd {

    // 计算链表长度
    public ListNode removeNthFromEnd1(ListNode head, int n) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;

        int len = getLength(head);
        ListNode cur = dummyNode;

        for (int i = 1; i < len - n + 1; i++) {
            cur = cur.next;
        }
        cur.next = cur.next.next;
        ListNode ans = dummyNode.next;
        return ans;
    }
    public int getLength(ListNode head) {
        int length = 0;
        while (head != null) {
            ++length;
            head = head.next;
        }
        return length;
    }

    // 栈
    public ListNode removeNthFromEnd2(ListNode head, int n) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;
        Deque<ListNode> stack = new LinkedList<>();
        ListNode cur = dummyNode;

        // 链表节点全部入栈
        while (cur != null) {
            stack.push(cur);
            cur  = cur.next;
        }

        for (int i = 0; i < n; i++) {
            stack.pop();
        }
        // 记录待删除节点的前驱
        ListNode prev = stack.peek();
        prev.next = prev.next.next;

        return dummyNode.next;
    }

    // 快慢指针
    public ListNode removeNthFromEnd(ListNode head, int n) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;

        ListNode fast = head;
        // 特殊情况，要落在倒数 k 个节点的前驱
        ListNode slow = dummyNode;

        while (n > 0) { // 快指针先走 n 步
            n--;
            fast = fast.next;
        }
        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }
        // 找到待删除节点的前驱后删除目标节点
        slow.next = slow.next.next;

        return dummyNode.next;
    }

}
