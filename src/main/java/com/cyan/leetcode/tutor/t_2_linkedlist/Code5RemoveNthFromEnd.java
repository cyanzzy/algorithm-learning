package com.cyan.leetcode.tutor.t_2_linkedlist;

import java.io.PrintWriter;

/**
 * 删除链表的倒数第 N 个结点
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/remove-nth-node-from-end-of-list/
 * @author Cyan Chau
 * @create 2023-03-01
 * @update 2024-04-09
 */
public class Code5RemoveNthFromEnd {

//    public ListNode removeNthFromEnd(ListNode head, int n) {
//
//        // 建立虚拟节点
//        ListNode dummy = new ListNode(0, head);
//        // 快指针指向第一个元素节点
//        ListNode fast = head;
//        // 慢指针指向虚拟节点
//        ListNode slow = dummy;
//
//        // 快指针先移动。直到两个指针步长差距为n
//        for (int i = 0; i < n; ++i) {
//            fast = fast.next;
//        }
//
//        // 完成上述操作，慢指针与快指针同步移动
//        while (fast != null) {
//            fast = fast.next;
//            slow = slow.next;
//        }
//
//        // 此时快指针超前慢指针n步
//        slow.next = slow.next.next;
//        ListNode ans = dummy.next;
//
//        return ans;
//    }

    // 算法思想
    // 基于快慢指针，快指针放在第一个元素，慢指针放在虚拟节点处
    // 先让快指针走 N 步
    // 然后快慢指针同时移动，当快指针走到尽头，慢指针到达目标节点前驱节点，即可执行删除操作
    public static ListNode removeNthFromEnd(ListNode head, int n) {

        // 虚拟节点
        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;
        ListNode fast = head, slow = dummyNode;

        // fast 先走 n 步，直到两个步长差 n
        while (n > 0) {
            fast = fast.next;
            n--;
        }
        // 同步移动
        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }
        // 当 fast 走到 null，slow 便落入目标节点的前驱
        slow.next = slow.next.next;
        return dummyNode.next;
    }

    public static int getLengthByHead(ListNode head) {
        int count = 0;
        while (head != null) {
            head = head.next;
            count++;
        }
        return count;
    }
    public static void main(String[] args) {
        ListNode listNode = new ListNode(0);

        ListNode head = new ListNode(1);
        head.next = new ListNode(2);
        head.next.next = new ListNode(3);
        head.next.next.next = new ListNode(4);
        head.next.next.next.next = new ListNode(5);
        ListNode.printList(head);

        listNode.next = head;
        int lentghByHead = getLengthByHead(listNode);
        System.out.println(lentghByHead);


//        removeNthFromEnd(head, 2);

//        ListNode.printList(head);

    }


}
