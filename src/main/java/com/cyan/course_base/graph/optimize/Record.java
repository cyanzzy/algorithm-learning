package com.cyan.course_base.graph.optimize;

import com.cyan.course_base.graph.Node;

import java.util.HashMap;

/**
 * 手写堆
 *
 * @author Cyan Chau
 * @create 2023-04-11
 */
public class Record {

    // 堆
    private Node[] nodes;
    // 堆节点的索引
    private HashMap<Node, Integer> heapIndexMap;
    // head到Node的最小距离
    private HashMap<Node, Integer> distanceMap;
    // 堆大小
    private int size;

    public Record(int size) {
        this.nodes = new Node[size];
        this.heapIndexMap = new HashMap<>();
        this.distanceMap = new HashMap<>();
        this.size = 0;
    }

    // 判空
    public boolean isEmpty() {
        return size == 0;
    }

//    // 弹出堆顶
//    public Record pop() {
//        // 记录堆顶元素
//        new Record()
//    }


}
