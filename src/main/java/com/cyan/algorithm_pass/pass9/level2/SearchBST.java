package com.cyan.algorithm_pass.pass9.level2;

import com.cyan.algorithm_pass.TreeNode;

/**
 * 二叉搜索树中的搜索
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class SearchBST {

//    // 递归
//    public TreeNode searchBST(TreeNode root, int val) {
//
//        if (root == null) {
//            return null;
//        }
//        if (val == root.val) {
//            return root;
//        }
//        return searchBST(val < root.val ? root.left : root.right, val);
//    }

    // 迭代
    public TreeNode searchBST(TreeNode root, int val) {
        while (root != null) {
            if (val == root.val) {
                return root;
            }
            root = val < root.val ? root.left : root.right;
        }
        return null;
    }
}
