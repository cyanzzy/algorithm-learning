package com.cyan.algorithm_pass.pass4.level1;

import org.omg.CORBA.PERSIST_STORE;

import java.util.Arrays;

/**
 * 数组栈
 *
 * @author Cyan Chau
 * @create 2024-02-22
 */
public class AStack<T> {

    // 栈结构
    private T[] stack;
    // 栈顶指针
    private int top;

    //构造函数
    public AStack() {

        // 以下行会导致编译时错误
        // private T[] t_1_array = new T[10]; 泛型擦除
        this.stack = (T[]) new Object[10];
    }

    // 判空
    public boolean isEmpty() {
        return top == 0;
    }

    // 取栈顶元素
    public T peek() {
        T t = null;
        if (top > 0) {
            t = stack[top - 1];
        }
        return t;
    }

    // 出栈
    public T pop() {
        T t = peek();
        if (top > 0) {
            stack[top - 1] = null;
            top--;
        }
        return t;
    }

    // 入栈
    public void push(T t) {
        expandCapacity(top + 1);
        stack[top++] = t;
    }

    // 扩容
    public void expandCapacity(int size) {
        int length = stack.length;
        if (size > length) {
            size = size * 3 / 2 + 1; // 每次扩大 50%
            stack = Arrays.copyOf(stack, size);
        }
    }

    public static void main(String[] args) {
        AStack<String> stack = new AStack<>();
        System.out.println(stack.peek());
        System.out.println(stack.isEmpty());

        stack.push("1");
        stack.push("2");
        stack.push("3");

        System.out.println(stack.pop());
        System.out.println(stack.isEmpty());
        System.out.println(stack.peek());
    }
}
