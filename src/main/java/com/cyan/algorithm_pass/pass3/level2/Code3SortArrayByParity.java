package com.cyan.algorithm_pass.pass3.level2;

import java.util.concurrent.locks.AbstractQueuedSynchronizer;

/**
 * 按奇偶排序数组
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code3SortArrayByParity {

    public int[] sortArrayByParity1(int[] nums) {

        int left = 0, right = nums.length -1;
        while (left < right) {
            // 如果遇到的是偶数，就表示这个元素已经排好序了，继续从左往右遍历，直到遇到一个奇数。
            while (left < right && nums[left] % 2 == 0) {
                left++;
            }
            // 如果遇到的是奇数，就表示这个元素已经排好序了，继续从右往左遍历，直到遇到一个偶数。
            while (left < right && nums[right] % 2 == 1) {
                right--;
            }
            // 交换这个奇数和偶数的位置，并且重复两边的遍历，直到在中间相遇
            if (left < right) {
                int temp = nums[left];
                nums[left] = nums[right];
                nums[right] = temp;
                left++;
                right--;
            }
        }
        return nums;
    }

    public int[] sortArrayByParity(int[] nums) {

        int [] res = new int[nums.length];
        int left = 0, right = nums.length -1;

        for (int num : nums) {
            if (num % 2 == 0) { // 偶数
                res[left++] = num;
            } else { // 奇数
                res[right--] = num;
            }
        }
        return res;
    }
}
