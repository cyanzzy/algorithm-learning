package com.cyan.course_base.graph;

import java.util.HashMap;
import java.util.HashSet;

/**
 * 图的邻接表
 *
 * @author Cyan Chau
 * @create 2023-03-27
 */
public class Graph {

    // 图的顶点集合
    public HashMap<Integer, Node> nodes;

    // 图的边集合
    public HashSet<Edge> edges;

    public Graph() {
        nodes = new HashMap<>();
        edges = new HashSet<>();
    }
}
