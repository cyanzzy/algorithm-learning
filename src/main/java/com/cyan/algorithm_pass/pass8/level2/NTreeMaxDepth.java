package com.cyan.algorithm_pass.pass8.level2;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * N 叉树最大深度
 *
 * @author Cyan Chau
 * @create 2024-03-08
 */
public class NTreeMaxDepth {

//    // DFS
//    public int maxDepth(Node root) {
//        if (root == null) {
//            return 0;
//        }
//        int maxChildDepth = 0;
//        List<Node> children = root.children;
//
//        for (Node child : children) {
//            int childDepth = maxDepth(child);
//            maxChildDepth = Math.max(maxChildDepth, childDepth);
//        }
//        return maxChildDepth + 1;
//    }

    // BFS
    public int maxDepth(Node root) {
        if (root == null) {
            return 0;
        }
        Queue<Node> queue = new LinkedList<Node>();
        queue.offer(root);
        int depth = 0;
        while (!queue.isEmpty()) {
            // 当前层元素个数
            int levelSize = queue.size();
            for (int i = 0; i < levelSize; i++) {
                Node node = queue.poll();
                List<Node> children = node.children;
                for (Node child : children) {
                    queue.offer(child);
                }
            }
            // 每层结束，深度++
            depth++;
        }
        return depth;
    }

    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }

}
