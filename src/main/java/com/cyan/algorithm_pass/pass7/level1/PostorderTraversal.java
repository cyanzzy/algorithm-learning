package com.cyan.algorithm_pass.pass7.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 后序遍历
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class PostorderTraversal {

    public List<Integer> postorderTraversal(TreeNode root) {

        List<Integer> ans = new ArrayList<>();
        postorder(root, ans);
        return ans;
    }

    public void postorder(TreeNode root, List<Integer> ans) {

        if (root == null) {
            return;
        }
        postorder(root.left, ans);
        postorder(root.right, ans);
        ans.add(root.val);
    }
}
