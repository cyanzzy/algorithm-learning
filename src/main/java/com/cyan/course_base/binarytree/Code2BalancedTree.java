package com.cyan.course_base.binarytree;

/**
 * BalancedTree: 树形DP
 *
 * @author Cyan Chau
 * @create 2023-03-08
 */
public class Code2BalancedTree {

    // 信息结构
    public class ReturnData {

        // 是否平衡
        public boolean isBalanced;

        // 高度
        public int height;

        public ReturnData(boolean isBalanced, int height) {

            this.isBalanced = isBalanced;
            this.height = height;
        }

    }

    // 递归函数
    public ReturnData process(Node x) {

        // 空树满足条件
        if (x == null) {
            return new ReturnData(true, 0);
        }

        // 递归检查左子树
        ReturnData leftData = process(x.left);

        // 递归检查右子树
        ReturnData rightData = process(x.right);

        // 树的高度
        int height = Math.max(leftData.height, rightData.height) + 1;

        // 平衡性
        boolean isBalanced = leftData.isBalanced && rightData.isBalanced && Math.abs(leftData.height - rightData.height) < 2;

        // 返回整合的信息
        return new ReturnData(isBalanced, height);
    }

    // 调用
    public boolean isBalanced(Node head) {
        return process(head).isBalanced;
    }

}
