package com.cyan.algorithm_pass.pass12.level2;

/**
 * 仅反转字母
 *
 * @author Cyan Chau
 * @create 2024-03-19
 */
public class ReverseOnlyLetters {

    public String reverseOnlyLetters(String s) {

        char[] arr = s.toCharArray();
        int left = 0, right = s.length() - 1;
        while (left < right) {
            // 判断左边是否扫描到字母
            while (left < right && !Character.isLetter(s.charAt(left))) {
                left++;
            }
            // 判断右边是否扫描到字母
            while (left < right && !Character.isLetter(s.charAt(right))) {
                right--;
            }
            swap(arr, left, right);
            left++;
            right--;
        }
        return new String(arr);
    }

    public void swap(char[] arr, int left, int right) {
        char temp = arr[left];
        arr[left] = arr[right];
        arr[right] = temp;
    }

}
