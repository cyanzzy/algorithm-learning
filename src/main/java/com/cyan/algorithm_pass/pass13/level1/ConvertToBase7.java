package com.cyan.algorithm_pass.pass13.level1;

/**
 * 七进制
 *
 * @author Cyan Chau
 * @create 2024-03-23
 */
public class ConvertToBase7 {

    public String convertToBase7(int num) {
        if (num == 0) {
            return "0";
        }
        // num < 0 true
        boolean negative = num < 0;
        num = Math.abs(num);
        StringBuffer digits = new StringBuffer();
        while (num > 0) {
            digits.append(num % 7);
            num /= 7;
        }
        if (negative) {
            digits.append('-');
        }
        return digits.reverse().toString();
    }
}
