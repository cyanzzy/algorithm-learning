package com.cyan.course_advance.kmp;

/**
 * KMP
 *
 * @author Cyan Chau
 * @create 2023-08-17
 */
public class KMP {

    public static int[] getNext(char[] match) {
        if (match.length == 1) {
            return new int[]{-1};
        }
        int[] next = new int[match.length];
        next[0] = -1;
        next[1] = 0;
        int i = 2;
        int j = 0;

        while (i < next.length) {
            if (match[i - 1] == match[j]) { //相等，匹配下一个
                next[i++] = ++j;
            } else if (j > 0) { //不相等往前跳，继续匹配
                j = next[j];
            } else {//来到最左边情况，说明i之前字符不存在前后缀匹配情况，匹配下一个
                next[i++] = 0;
            }
        }
        return next;
    }


    /**
     * KMP
     * @param str
     * @param match
     * @return
     */
    public static int KMP(String str, String match) {
        if (str == null || match == null ||
                match.length() < 1 || str.length() < match.length()) {
            return -1;
        }
        char[] mainStr = str.toCharArray();
        char[] subStr = match.toCharArray();
        int i = 0;
        int j = 0;
        int[] next = getNext(subStr);
        while (i < mainStr.length && j < subStr.length) {
            if (mainStr[i] == subStr[j]) {
                i++;
                j++;
            } else if (next[j] == -1) {//往前跳到开头都匹配不出来，找主串下一个位置开始匹配
                i++;
            } else {//当前匹配不出来且没有跳到子串开头，往前跳
                j = next[j];
            }
        }
        return j == subStr.length ? i - j : -1;
    }
}
