package com.cyan.algorithm_pass.pass18.level2;

/**
 * 单词搜索
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class Exist {

    public boolean exist(char[][] board, String word) {

        char[] words = word.toCharArray();
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (dfs(board, words, i, j, 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean dfs(char[][] board, char[] word, int i, int j, int k) {
        if (i >= board.length || i < 0 ||
            j >= board[0].length || j < 0||
            board[i][j] != word[k]) {
            return false;
        }
        if (k == word.length - 1) {
            return true;
        }
        board[i][j] = '\0';
        boolean res = dfs(board, word, i + 1, j, k + 1) || // 右
                      dfs(board, word, i - 1, j, k + 1) || // 左
                      dfs(board, word, i, j + 1, k + 1) || // 下
                      dfs(board, word, i, j - 1, k + 1); // 上
        board[i][j] = word[k];
        return res;
    }

}
