package com.cyan.algorithm_pass.pass8.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 二叉树最大深度
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class MaxDepth {
//
//    // DFS
//    public int maxDepth(TreeNode root) {
//
//        if (root == null) {
//            return 0;
//        } else {
//            int leftHeight = maxDepth(root.left);
//            int rightHeight = maxDepth(root.right);
//            return Math.max(leftHeight, rightHeight) + 1;
//        }
//    }

    // BFS
    public int maxDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int depth = 0;
        while (!queue.isEmpty()) {
            // 某层所有元素数
            int levelSize = queue.size();
            for (int i = 0; i < levelSize; i++) {
                TreeNode node = queue.poll();
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            depth++;
        }
        return depth;
    }

}
