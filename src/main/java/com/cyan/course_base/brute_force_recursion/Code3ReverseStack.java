package com.cyan.course_base.brute_force_recursion;

import java.util.Stack;

/**
 * 使用递归函数逆序栈（不能申请额外空间）
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code3ReverseStack {


    // 获取栈底元素并移除它
    public int getAndRemoveLastElement(Stack<Integer> stack) {
        int result = stack.pop();
        if (stack.isEmpty()) { // 递归结束条件
            return result;
        } else {
            int last = getAndRemoveLastElement(stack);
            stack.push(result);
            return last;
        }
    }

    // 逆序栈
    public void reverse(Stack<Integer> stack) {
        if (stack.isEmpty()) {
            return;
        } else {
            //获取栈底元素
            int i = getAndRemoveLastElement(stack);
            reverse(stack);
            stack.push(i);
        }
    }
}
