package com.cyan.leetcode.tutor.t_4_string;

/**
 * CR 182. 动态口令
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/zuo-xuan-zhuan-zi-fu-chuan-lcof/description/
 * @author Cyan Chau
 * @create 2024-04-13
 */
public class Code4DynamicPassword {


    // 本次作答
    public String dynamicPassword(String password, int target) {
        char[] ch = password.toCharArray();
        int len = ch.length - 1;
        reverse(ch, 0, target - 1);
        reverse(ch, target, len);
        reverse(ch, 0, len);

        return new String(ch);

    }
    public void reverse(char[] str, int begin, int end) {
        for(int i = begin, j = end; i < j; i++, j--) {
            char temp = str[i];
            str[i] = str[j];
            str[j] = temp;
        }
    }
}
