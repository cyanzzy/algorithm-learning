package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 删除排序链表中的重复元素（仅保留一个重复元素）
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code11DeleteDuplicates {

    public ListNode deleteDuplicates(ListNode head) {

        // base case
        if (head == null) {
            return head;
        }
        ListNode cur = head;

        while (cur.next != null) {
            if (cur.val == cur.next.val) {
                cur.next = cur.next.next;
            } else {
                cur = cur.next;
            }
        }
        return head;
    }
}
