package com.cyan.algorithm_pass.pass16.level2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *  至多包含两个不同字符的最长子串
 *
 * @author Cyan Chau
 * @create 2024-04-29
 */
public class LengthOfLongestSubStringTwoDistinct {

    public int lengthOfLongestSubStringTwoDistinct(String s) {
        if (s.length() < 3) {
            return s.length();
        }
        int left = 0, right = 0;
        Map<Character, Integer> hashMap = new HashMap<>();
        int maxLen = 2;

        while (right < s.length()) {
            if (hashMap.size() < 3) {
                hashMap.put(s.charAt(right), right++);
            }
            // 如果数量达到 3
            if (hashMap.size() == 3) {
                // 最左侧待删除的为止
                int del_idex = Collections.min(hashMap.values());
                hashMap.remove(s.charAt(del_idex));
                // 窗口 left 的新位置
                left = del_idex + 1;
            }
            maxLen = Math.max(maxLen, right - left);
        }
        return maxLen;
    }
}
