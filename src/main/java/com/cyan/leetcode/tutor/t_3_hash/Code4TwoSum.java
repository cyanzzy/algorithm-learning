package com.cyan.leetcode.tutor.t_3_hash;

import java.util.HashMap;
import java.util.Map;

/**
 * 两数之和
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/two-sum/
 * @author Cyan Chau
 * @create 2023-03-26
 * update 2024-04-11
 */
public class Code4TwoSum {

    // 暴力枚举
    public int[] twoSum1(int[] nums, int target) {
        // 哨兵i指向当前待处理的元素，哨兵j用于找出target-nums[i]的元素
        int i, j;
        for (i = 0; i < nums.length; i++) {
            for (j = i + 1; j < nums.length; j++) {
                if (nums[j] == target - nums[i]) {
                    return new int[]{i, j};
                }
            }
        }

        return new int[0];
    }

    // 哈希表
    public int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                return new int[]{map.get(target - nums[i]), i};
            }
            map.put(nums[i], i);
        }
        return new int[0];
    }
}
