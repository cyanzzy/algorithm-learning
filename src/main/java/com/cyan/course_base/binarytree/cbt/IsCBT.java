package com.cyan.course_base.binarytree.cbt;

import com.cyan.course_base.binarytree.Node;

import java.util.LinkedList;

/**
 * CBT:  层次遍历模型 完全二叉树
 *
 * @author Cyan Chau
 * @create 2023-03-07
 */
public class IsCBT {

    public boolean isCBT(Node head) {
        // 空树符合要求
        if (head == null) {
            return true;
        }

        LinkedList<Node> queue = new LinkedList<>();

        // 定义叶子判断标记
        boolean leaf = false;

        // 定义双指针
        Node lchild = null;
        Node rchild = null;

        // 头节点入队
        queue.add(head);

        while (!queue.isEmpty()) {
            // 弹出队首节点
            head = queue.poll();
            lchild = head.left;
            rchild = head.right;

            // 1. 开启叶子判断
            // 2. 如果有右孩子但无左孩子，则返回false
            if (leaf && (lchild != null || rchild!= null) || (lchild == null && rchild != null)) {
                return false;
            }

            // 左孩子入队
            if (lchild != null) {
                queue.add(lchild);
            }

            // 右孩子入队
            if (rchild != null) {
                queue.add(rchild);
            }

            // 第一次遇到不是拥有两个孩子的节点，则开启叶子判断
            if (lchild == null || rchild == null) {
                leaf = true;
            }
        }
        return true;
    }
}
