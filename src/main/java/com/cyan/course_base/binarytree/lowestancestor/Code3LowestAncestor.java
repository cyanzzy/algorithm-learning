package com.cyan.course_base.binarytree.lowestancestor;

import com.cyan.course_base.binarytree.Node;

import java.util.HashMap;

/**
 * 结构2：优化结构
 *
 * @author Cyan Chau
 * @create 2023-03-23
 */
public class Code3LowestAncestor {
    public class Record {
        // 建立任意两个结点之间最近公共祖先的哈希表
        private HashMap<Node, HashMap<Node, Node>> map;

        public Record(Node head) {
            map = new HashMap<>();
            initMap(head);
            setMap(head);
        }

        public void initMap(Node head) {
            // 如果结点为空，退出
            if (head == null) {
                return;
            }
            map.put(head, new HashMap<>());

            initMap(head.left);
            initMap(head.right);
        }

        public void setMap(Node head) {
            // 如果结点为空
            if (head == null) {
                return;
            }
            // 根结点下的所有节点构成的哈希表的值部分初始化<h,h>
            // <n, <h, h>>
            headRecord(head.left, head);
            headRecord(head.right, head);

            subRecord(head);

            setMap(head.left);
            setMap(head.right);
        }

        // 将以n为根结点的所有子树的结点和h结点的最近公共祖先标为h
        public void headRecord(Node n, Node h) {
            if (n == null) {
                return;
            }
            // <n, <h,h>>
            map.get(n).put(h, h);

            headRecord(n.left, h);
            headRecord(n.right, h);
        }

        // 将head为根结点的整棵树的最近公共祖先标head
        public void subRecord(Node head) {
            // 头结点为空，退出
            if (head == null) {
                return;
            }
            preLeft(head.left, head.right, head);

            subRecord(head.left);
            subRecord(head.right);
        }

        // 将h左子树的每个结点的最近公共祖先标为h
        public void preLeft(Node l, Node r, Node h) {
            // h的左孩子r为空
            if (l == null) {
                return;
            }
            preRight(l, r, h);

            preLeft(l.left, r, h);
            preLeft(l.right, r, h);
        }

        // 将h右子树的每个结点的最近公共祖先标为h
        public void preRight(Node l, Node r, Node h) {
            // h的右孩子r为空
            if (r == null) {
                return;
            }
            // <l,<r,h>>
            // 构建以l为键，<r,h>为值得哈希表，表示r的祖先为h
            map.get(l).put(r, h);

            preRight(l, r.left, h);
            preRight(l, r.right, h);
        }

        public Node query(Node o1, Node o2) {
            if (o1 == o2) {
                return o1;
            }
            if (map.containsKey(o1)) {
                return map.get(o1).get(o2);
            }
            if (map.containsKey(o2)) {
                return map.get(o2).get(o1);
            }
            return null;
        }
    }

}
