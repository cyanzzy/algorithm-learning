package com.cyan.algorithm_pass.pass12.level3;

/**
 * 最长公共前缀
 *
 * @author Cyan Chau
 * @create 2024-03-22
 */
public class LongestCommonPrefix {

//    // 横向扫描
//    public String longestCommonPrefix(String[] strs) {
//        if (strs == null || strs.length == 0) {
//            return "";
//        }
//        String prefix = strs[0];
//        int count = strs.length;
//        for (int i = 1; i < count; i++) {
//            // 求任意两个字符串的公共前缀
//            // LCP(0,1)->LCP(0,2)
//            prefix = longestCommonPrefix(prefix, strs[i]);
//            if (prefix.length() == 0) {
//                break;
//            }
//        }
//        return prefix;
//    }
//
//    public String longestCommonPrefix(String str1, String str2) {
//        int len = Math.min(str1.length(), str2.length());
//        int index = 0;
//        while (index < len) {
//            if (str1.charAt(index) != str2.charAt(index)) {
//                break;
//            }
//            index++;
//        }
//        return str1.substring(0, index);
//    }

//    // 纵向扫描
//    public String longestCommonPrefix(String[] strs) {
//        if (strs == null || strs.length == 0) {
//            return "";
//        }
//        // 第一个单词长度
//        int length = strs[0].length();
//        // strs 数组长度
//        int count = strs.length;
//
//        // 横向走
//        for (int i = 0; i < length; i++) {
//            char ch = strs[0].charAt(i);
//            // 纵向走
//            for (int j = 1; j < count; j++) {
//                if (i == strs[j].length() || strs[j].charAt(i) != ch) {
//                    return strs[0].substring(0,i);
//                }
//            }
//        }
//        return strs[0];
//    }

    // 分治
    public String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        } else {
            return longestCommonPrefix(strs, 0, strs.length - 1);
        }
    }

    public String longestCommonPrefix(String[] strs, int start, int end) {
        if (start == end) {
            return strs[start];
        }

        int mid = start + ((end - start) >> 1);
        String lcpLeft = longestCommonPrefix(strs, start, mid);
        String lcpRight = longestCommonPrefix(strs, mid + 1, end);
        return commonPrefix(lcpLeft, lcpRight);
    }

    public String commonPrefix(String lcpLeft, String lcpRight) {
        int minLength = Math.min(lcpLeft.length(), lcpRight.length());
        for (int i = 0; i < minLength; i++) {
            if (lcpLeft.charAt(i) != lcpRight.charAt(i)) {
                return lcpLeft.substring(0, i);
            }
        }
        return lcpLeft.substring(0, minLength);
    }

}
