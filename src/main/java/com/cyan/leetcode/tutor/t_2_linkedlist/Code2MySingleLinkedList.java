package com.cyan.leetcode.tutor.t_2_linkedlist;

/**
 *  设计【单】链表
 *
 * @accepted Success   TODO 细节问题
 * @link https://leetcode.cn/problems/design-linked-list/description/
 * @author Cyan Chau
 * @create 2023-02-24
 * update 2024-04-08
 */
public class Code2MySingleLinkedList {

    // TODO 细节
    class ListNode {
        int val;
        ListNode next;
        ListNode(){}
        ListNode(int val) {
            this.val=val;
        }
    }

    class MyLinkedList {
        int size;
        ListNode head;

        // TODO 细节
        public MyLinkedList() {
            size = 0;
            head = new ListNode(0);
        }

        // Add a node of value val before the index-th node in the linked list.
        // If index equals to the length of linked list, the node will be appended to the end of linked list.
        // If index is greater than the length, the node will not be inserted.
        public void addAtIndex(int index, int val) {

            //If index is greater than the length,
            // the node will not be inserted.
            if (index > size) {
                return;
            }

            // [so weird] If index is negative,
            // the node will be inserted at the head of the list.
            if (index < 0) {
                index = 0;
            }
            // TODO 细节
            ++size;

            //find predecessor of the node to be added
            ListNode pre = head;
            for (int i = 0; i < index; i++) {
                pre = pre.next;
            }

            // node to be added
            ListNode toAdd = new ListNode(val);

            // insertion itself
            toAdd.next = pre.next;
            pre.next = toAdd;
        }


        // Add a node of value val before the first element of the linked list.
        // After the insertion, the new node will be the first node of the linked list.
        public void addAtHead(int val) {
            addAtIndex(0, val);
        }

        // Append a node of value val to the last element of the linked list.
        public void addAtTail(int val) {
            addAtIndex(size, val);
        }

        //  Delete the index-th node in the linked list, if the index is valid.
        public void deleteAtIndex(int index) {

            // if the index is invalid, do nothing
            if (index < 0 || index >= size) return;

            // TODO 细节
            size--;

            // find predecessor of the node to be deleted
            ListNode pre = head;
            for(int i = 0; i < index; ++i) {
                pre = pre.next;
            }

            // delete pre.next
            pre.next = pre.next.next;
        }

        // Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        public int get(int index) {

            // if index is invalid
            if (index < 0 || index >= size) {
                return -1;
            }

            ListNode cur = head;

            // index steps needed
            // to move from sentinel node to wanted index
            for (int i = 0; i < index + 1; i++) {
                cur = cur.next;
            }

            return cur.val;
        }
    }
}
