package com.cyan.algorithm_pass.pass5.level2;

import java.util.Stack;

/**
 * 使用栈实现队列
 *
 * @author Cyan Chau
 * @create 2024-03-02
 */
public class MyQueue {

    // 输出栈
    private Stack<Integer> inStack;
    // 输出栈
    private Stack<Integer> outStack;

    public MyQueue() {
        inStack = new Stack<>();
        outStack = new Stack<>();
    }

    // 入队
    public void push(int x) {
        inStack.push(x);
    }

    // 出队
    public int pop() {
        // 如果输出栈为空，则将输入栈元素出栈并压入输出栈，再进行出队操作
        if (outStack.isEmpty()) {
            inToOut();
        }
        return outStack.pop();
    }

    // 取队首
    public int peek() {
        if (outStack.isEmpty()) {
            inToOut();
        }
        return outStack.peek();
    }

    // 判空
    public boolean empty() {
        return inStack.isEmpty() && outStack.isEmpty();
    }

    // 将输入栈元素弹出，并压入输出栈
    public void inToOut() {
        while (!inStack.isEmpty()) {
            outStack.push(inStack.pop());
        }
    }
}
