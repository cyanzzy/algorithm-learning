package com.cyan.algorithm_pass.pass12.level2;

/**
 * 反转字符串
 *
 * @author Cyan Chau
 * @create 2024-03-19
 */
public class ReverseString {

    // 双指针
    public void reverseString(char[] s) {

        for (int left = 0, right = s.length - 1; left < right; left++, right--) {
            char temp = s[left];
            s[left] = s[right];
            s[right] = temp;
        }
    }

}
