package com.cyan.algorithm_pass.pass13.level3;

import java.util.Arrays;

/**
 * 素数
 *
 * @author Cyan Chau
 * @create 2024-04-28
 */
public class Prime {

    public boolean isPrime(int num) {

        int max = (int) Math.sqrt(num);
        for (int i = 2; i <= max; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public int countPrimes(int n) {
        int cnt = 0;
        for (int i = 2; i < n; i++) {
            if (isPrime(i)) {
                cnt++;
            }
        }
        return cnt;
    }

//    // 埃氏筛
//    public int countPrimes(int n) {
//        int[] isPrime = new int[n];
//        Arrays.fill(isPrime, 1);
//        int ans = 0;
//        for (int i = 2; i < n; ++i) {
//            if (isPrime[i] == 1) {
//                ans += 1;
//                if ((long) i * i < n) {
//                    for (int j = i * i; j < n; j += i) {
//                        isPrime[j] = 0;
//                    }
//                }
//            }
//        }
//        return ans;
//    }

}
