package com.cyan.algorithm_pass.pass18.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * 路径总和
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class PathSum {


    List<List<Integer>> res = new ArrayList<>();

    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<Integer> path = new LinkedList<>();
        dfs(root, targetSum, path);
        return res;
    }

    public void dfs(TreeNode root, int targetSum, List<Integer> path) {
        // 递归终止条件
        if (root == null) {
            return;
        }

        targetSum -= root.val;
        path.add(root.val);
        if (targetSum == 0 && root.left == null && root.right == null) {
            res.add(new LinkedList<>(path));
        }
        dfs(root.left, targetSum, path);
        dfs(root.right, targetSum, path);
        path.remove(path.size() - 1);
    }

}
