package com.cyan.algorithm_pass.pass13.level2;

/**
 * 4 的幂
 *
 * @author Cyan Chau
 * @create 2024-04-28
 */
public class IsPowerOfFour {

    public boolean isPowerOfFour(int n) {

        if (n <= 0) {
            return false;
        }
        while (n % 4 == 0) {
            n /= 4;
        }
        return n == 1;
    }

//    // 转换成 2 的幂
//    public boolean isPowerOfFour(int n) {
//        if (n <= 0) return false;
//        int x = (int)Math.sqrt(n);
//        // 2的幂： n > 0 && (n & -n) == n;
//        return x * x == n && (x & -x) == x;
//    }
}
