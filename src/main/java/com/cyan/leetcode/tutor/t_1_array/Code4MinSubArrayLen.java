package com.cyan.leetcode.tutor.t_1_array;


import java.util.Arrays;

/**
 * 长度最小的子数组
 *
 * @accepted Failed 未理解滑动窗口
 * @link https://leetcode.cn/problems/minimum-size-subarray-sum/description/
 * @author Cyan Chau
 * @create 2023-02-16, 2023-10-22
 * @update 2024-04-03
 */
public class Code4MinSubArrayLen {

    // 暴力枚举
    public int minSubArrayLen1(int target, int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int ans = Integer.MAX_VALUE;

        // 枚举每个位置的可能性
        for (int i = 0; i < nums.length; i++) {
            int sum = 0;
            // 从i位置作为子数组的开始
            for (int j = i; j < nums.length; j++) {
                sum += nums[j];
                if (sum >= target) {
                    ans = Math.min(ans, j - i + 1);
                    break;
                }
            }
        }

        return ans == Integer.MAX_VALUE ? 0 : ans;
    }

    // 滑动窗口
    public int minSubArrayLen2(int target, int[] nums) {
        if (nums.length == 0) {
            return 0;
        }

        int ans = Integer.MAX_VALUE;
        int start = 0, end = 0, sum = 0;

        /*
            2 3 1 2 4 3
           [2]3 1 2 4 3
           [2 3]1 2 4 3
           [2 3 1]2 4 3
           [2 3 1 2]4 3

           2 [3 1 2 4]3
           2  3[1 2 4]3
           2  3 1[2 4 3]
           2  3 1 2[4 3]
        */
        // 保证窗口右边界不越界
        while (end < nums.length) {
            // 添加元素到窗口中
            sum += nums[end];

            // 窗口内某元素满足条件，开启判断
            while (sum >= target) {
                // 判断逻辑
                ans = Math.min(ans, end - start + 1);

                // 左边界++
                sum -= nums[start];
                start++;
            }
            // 右边界++
            end++;
        }
        return ans == Integer.MAX_VALUE ? 0 : ans;
    }

    // 前缀和+二分查找
    public int minSubArrayLen3(int s, int[] nums) {
        if (nums.length == 0) {
            return 0;
        }
        int ans = Integer.MAX_VALUE;
        // 记录前缀和
        int[] sums = new int[nums.length + 1];

        // 为了方便计算，令 size = n + 1
        // sums[0] = 0 意味着前 0 个元素的前缀和为 0
        // sums[1] = A[0] 前 1 个元素的前缀和为 A[0]
        // 以此类推
        for (int i = 1; i <= nums.length; i++) {
            sums[i] = sums[i - 1] + nums[i - 1];
        }

        /**
         * 通过二分查找得到大于或等于i的最小下标bound，
         * 使得 sums[bound]−sums[i−1]≥s
         *  sums[bound]≥s+sums[i−1]
         */
        for (int i = 1; i <= nums.length; i++) {
            int target = s + sums[i-1];
            int bound = Arrays.binarySearch(sums, target);
            if (bound < 0) {
                bound = -bound - 1;
            }
            if (bound <= nums.length) {
                ans = Math.min(ans, bound - (i - 1));
            }
        }
        return ans == Integer.MAX_VALUE ? 0 : ans;
    }

    // 一刷 滑动窗口，Failed
    /*
      错误1:  用例 [2,3,1,2,4,3] 7 中，使用 if (windows >= target) 判断窗口，没有顾及到窗口左边界过期后，剩余元素是否满足要求
      错误2： 用例 [1,1,1,1,1,1,1,1] 11 中，综合不满足 target，该情况未考虑到，而是直接  return min;
      错误3：未考虑边界情况 if (nums.length == 0) {

     */
    public int minSubArrayLen(int target, int[] nums) {

        // base case
        if (nums.length == 0) {
            return 0;
        }

        // 定义窗口的左右边界
        int left =0, right = 0;
        // 定义窗口，该窗口表示窗口内的元素之和
        int windows = 0;
        // 定义比较器
        int min = Integer.MAX_VALUE;

        while (right < nums.length) {

            // 模拟窗口右移
            windows += nums[right];
            // 判断逻辑
            while (windows >= target) { // 一旦发现大于等于 target，则更新 min 和窗口
                min = min < right - left + 1 ? min : right - left + 1;
                // 模拟窗口左移（数据过期）
                windows -= nums[left];
                left++;
            }
            right++;
        }
        return min == Integer.MAX_VALUE ?  0 : min;
    }

}
