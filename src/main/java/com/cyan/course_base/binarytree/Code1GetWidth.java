package com.cyan.course_base.binarytree;

/**
 * 求二叉树的宽度
 *
 * @author Cyan Chau
 * @create 2023-03-02
 */
public class Code1GetWidth {

    // 层次遍历模型
    public int getBinaryTreeWidth(Node head) {

        int maxsize = 100;
        // 定义哨兵
        Node sentinel;
        // 定义队列
        Node[] que = new Node[maxsize]; // MaxSize根据实际题目而定
        // 定义指针
        int front = 0, rear = 0;
        // 记录当前最大宽度
        int max = 0;
        // 定义当前层次节点数
        int count = 0;
        // 记录当前层次最后一个节点
        int last = 1;

        // 根节点入队
        rear = (rear + 1) % maxsize;
        que[rear] = head;

        while (front != rear) {
            // 出队一个节点
            front = (front + 1) % maxsize;
            sentinel = que[front];

            // 记录当前节点
            count++;

            // 哨兵节点的左孩子入队
            if (sentinel.left != null) {
                rear = (rear +1) % maxsize;
                que[rear] = sentinel.left;
            }

            // 哨兵节点的右孩子入队
            if (sentinel.right != null) {
                rear = (rear +1) % maxsize;
                que[rear] = sentinel.right;
            }

            // 更新层次
            if (front == last) {
                // last指向下一层层次最后一个节点
                last = rear;
                // 更新最大节点数
                max = count > max ? count : max;
                // 重置节点数
                count = 0;
            }
        }
        return max;
    }
}
