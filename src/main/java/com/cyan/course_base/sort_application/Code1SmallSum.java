package com.cyan.course_base.sort_application;

/**
 * 小和问题
 *
 * @author Cyan Chau
 * @create 2023-02-14
 */
public class Code1SmallSum {

    public int merge(int[] arr, int left, int mid, int right) {
        int[] temp = new int[right - left + 1];
        int i = 0;
        // p1代表左侧第一个
        int p1 = left;
        // p2代表右侧第一个
        int p2 = mid + 1;
        int res = 0;
        while (p1 <= mid && p2 <= right) {
            // 求小和
            res += arr[p1] < arr[p2] ? (right - p2 + 1) * arr[p1] : 0;
            // 归并
            temp[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        // 当左边元素有剩余
        while (p1 <= mid) {
            temp[i++] = arr[p1++];
        }
        // 当右边元素有剩余
        while (p2 <= right) {
            temp[i++] = arr[p2++];
        }
        // 重新拷贝回原始数组对应位置
        for (int j = 0; j < temp.length ; j++) {
            arr[left + j] = temp[j];
        }
        return res;
    }

    public int mergeSort(int[] arr, int left, int right) {
        if (left == right) {
            return 0;
        }
        int mid = left + ((right - left) >> 1);
        return mergeSort(arr, left, mid)
                + mergeSort(arr, mid + 1, right)
                + merge(arr, left, mid, right);
    }

    public int smallSum(int[] arr) {
        if (arr == null || arr.length < 2) {
            return 0;
        }
        return mergeSort(arr, 0, arr.length - 1);
    }


}
