package com.cyan.algorithm_pass.pass7.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 中序遍历
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class InorderTraversal {

    public List<Integer> inorderTraversal(TreeNode head) {
        List<Integer> ans = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        if (head == null) {
            return ans;
        }

        while (!stack.isEmpty() || head != null) {
            // 一直往左分支走
            while (head != null) {
                stack.push(head);
                head = head.left;
            }

            if (!stack.isEmpty()) {
                head = stack.pop();
                ans.add(head.val); // Visit(head)
                // 转向右分支
                head = head.right;
            }
        }
        return ans;
    }

}
