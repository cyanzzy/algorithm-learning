package com.cyan.course_base.sort_application;

/**
 * 逆序对
 *
 * @author Cyan Chau
 * @create 2023-02-15
 */
public class Code2ReversePair {

    public int merge(int[] arr, int left, int mid, int right) {
        int[] temp = new int[right - left + 1];
        int i = left, j = mid + 1, k = 0, count = 0;

        while (i <= mid && j <= right) {
            // 从mid开始划分成两段数组进行归并并找出逆序对
            if (arr[i] <= arr[j]) {
                temp[k++] = arr[i++];
            } else { // 当B段小于A段的数时，便找到了逆序对
                // 当B段小于A段的数时，便找到了逆序对
                temp[k++] = arr[j++];
                // 此时逆序对数为A段剩余元素数量
                count += mid - i + 1;
            }
        }

        while (i <= mid) {
            temp[k++] = arr[i++];
        }

        while (j <= right) {
            temp[k++] = arr[j++];
        }

        for (i = 0; i < temp.length; i++) {
            arr[left + i] = temp[i];
        }

        return count;
    }

    public int mergeSort(int[] arr, int left, int right) {
        if (left == right) {
            return 0;
        }
        int mid = left + ((right - left) >> 1);
        return mergeSort(arr, left, mid) +
                mergeSort(arr, mid + 1, right) +
                merge(arr, left, mid, right);
    }

    public int reverseSort(int[] arr) {
        if (arr == null || arr.length < 1) {
            return 0;
        }
        return mergeSort(arr,0,arr.length-1);
    }

}
