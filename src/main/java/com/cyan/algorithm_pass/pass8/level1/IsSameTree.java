package com.cyan.algorithm_pass.pass8.level1;

import com.cyan.algorithm_pass.TreeNode;

/**
 * 相同的树
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class IsSameTree {

    public boolean isSameTree(TreeNode p, TreeNode q) {

        // 如果两个都为空
        if (p == null && q == null) {
            return true;
        } else if (p == null || q == null) { // 不都为空
            return true;
        } else if (p.val != q.val){
            return false;
        } else {
            // 判断其左左和右右是否相同
            return isSameTree(p.left, q.left) && isSameTree(p.right, q.right);
        }
    }

}
