package com.cyan.algorithm_pass.pass13.level2;

/**
 * 2 的幂
 *
 * @author Cyan Chau
 * @create 2024-04-28
 */
public class IsPowerOfTwo {

    public boolean isPowerOfTwo(int n) {

        if (n <= 0) {
            return false;
        }
        while (n % 2 == 0) {
            n /= 2;
        }
        return n == 1;
    }

//    // 二进制表示
//    public boolean isPowerOfTwo(int n) {
//        return n > 0 && (n & (n - 1)) == 0;
//    }
//    public boolean isPowerOfTwo(int n) {
//        return n > 0 && (n & -n) == n;
//    }

}
