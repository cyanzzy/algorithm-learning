# About
Algorithm notes with tutorials on LeetCode, data structures, etc., coded by CyanChau.
To see the specific tutorial please refer to the [link](https://cyanzzy.github.io/)




## Course_Base
This course is an introduction to basic and advanced data structures.<br>To be updated.

> sort

[SelectionSort]()<br>
[BubbleSort]()<br>
[InsertionSort]()<br>
[MergeSort]()<br>
[HeapSort]()<br>
[QuickSort]()<br>
[RadixSort]()<br>

> sort application

[MergeSort Application1: SmallSumQuestion]()<br>
[MergeSort Application2: InverseOrderQuestion]()<br>
[HeapSort Application: SortedArrDistanceLessK]()<br>
[QuickSort Application: NetherlandsQuestion]()<br>
[BinarySearch Application: SeriesQuestion]()

> linked list

[IsPalindrome]()<br>
[ListPartition]()<br>
[CopyListWithRandom]()<br>
[IsCircle]()

> binary tree

[PreOrder <Non> Recursive]()<br>
[InOrder <Non> Recursive]()<br>
[PostOrder <Non> Recursive]()<br>
[getBinaryTreeWidth]()<br>
[IsBST (Three Solutions)]()<br>
[IsCBT]()<br>
[IsBalancedTree]()<br>
[IsFBT]()<br>
[LowestAncestor]()<br>
[GetNextNodeInOrder]()<br>
[SerialBinaryTree]()

> graph

[DFS <Non> Recursive]()<br>
[BFS]()<br>
[TopologySort]()<br>
[Prim]()<br>
[Kruskal]()<br>
[Dijkstra <Version2>]()<br>
[Floyd]()

> trie

[Trie]()

> greedy algorithm

Wait

> brute force recursion

[Hanoi]()<br>
[AllSubSequences]()<br>
[ReverseStack]()<br>
[NumberOfCombinations]()<br>
[BagProblems]()<br>
[CardGame]()<br>
[NQueens]()

## Course_Advance

> hash

[RandomPool]()<br>
[Islands]()<br>
[UnionFind]()<br>
[KMP]()<br>

## LeetCode
### tutor
The tutor section focuses on common types of topics to help you get started

> array  
<br>

[1.Binary Search](https://leetcode.cn/problems/binary-search/)<br>
[2.Remove Element](https://leetcode.cn/problems/remove-element/)<br>
[3.Squares of a Sorted Array](https://leetcode.cn/problems/squares-of-a-sorted-array/)<br>
[4.Minimum Size Subarray Sum](https://leetcode.cn/problems/minimum-size-subarray-sum/)<br>
[5.Spiral Matrix II](https://leetcode.cn/problems/spiral-matrix-ii/)

> link table 

[1.Remove Linked List Elements](https://leetcode.cn/problems/remove-linked-list-elements/)<br>
[2.Design Linked List](https://leetcode.cn/problems/design-linked-list/)




## Algorithm_Pass

 Essential algorithms for preparedness work.
 
 
 ### theme