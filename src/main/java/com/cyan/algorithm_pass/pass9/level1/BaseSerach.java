package com.cyan.algorithm_pass.pass9.level1;

/**
 * 基本查找
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class BaseSerach {

    public int search(int[] arr, int key) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == key) {
                return i;
            }
        }
        return -1;
    }
}
