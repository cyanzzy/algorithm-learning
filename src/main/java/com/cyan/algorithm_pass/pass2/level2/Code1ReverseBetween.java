package com.cyan.algorithm_pass.pass2.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.List;

/**
 * 反转链表
 *
 * @author Cyan Chau
 * @create 2024-01-17
 */
public class Code1ReverseBetween {


    // 穿针引线
    public ListNode reverseBetween1(ListNode head, int left, int right) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;

        ListNode pre = dummyNode;

        // step 1：从虚拟节点开始走 left - 1 步，来到 left 节点的前一个节点
        for (int i = 0; i < left - 1; i++) {
            pre = pre.next;
        }

        // step 2：从 pre 再走 right - left + 1 步，来到 right 节点
        ListNode rightNode = pre;
        for (int i = 0; i < right - left + 1; i++) {
            rightNode = rightNode.next;
        }

        // step 3：截取链表
        ListNode leftNode = pre.next;
        ListNode curr = rightNode.next;

        // 断链
        pre.next = null;
        rightNode.next = null;

        // step 4：反转子区间的链表
        reverseLinkedList(leftNode);

        // step 5：将反转后的子区间链表接入原始链表
        pre.next = rightNode;
        leftNode.next = curr;

        return dummyNode.next;
    }

    public void reverseLinkedList(ListNode head) { // 头插法反转

        ListNode L = new ListNode(0);
        L.next = head;

        ListNode p = head;
        // 断链
        L.next = null;

        while (p != null) {
            ListNode q = p.next;
            p.next = L.next;
            L.next = p;
            p = q;
        }

    }

    // 头插法
    public ListNode reverseBetween(ListNode head, int left, int right) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;
        ListNode pre = dummyNode;

        // `pre`：永远指向待反转区域的第一个节点 `left` 的前一个节点，在循环过程中不变。
        for (int i = 0; i < left - 1; i++) {
            pre = pre.next;
        }

        // `curr`：指向待反转区域的第一个节点 `left`；
        ListNode cur = pre.next;

        // `next`：永远指向 `curr` 的下一个节点，循环过程中，`curr` 变化以后 `next` 会变化；
        ListNode next;

        for (int i = 0; i < right - left; i++) {

            // 先将 `curr` 的下一个节点记录为 `next`；
            next = cur.next;
            // 执行操作 ①：把 `curr` 的下一个节点指向 `next` 的下一个节点；
            cur.next = next.next;
            // 执行操作 ②：把 `next` 的下一个节点指向 `pre` 的下一个节点；
            next.next = pre.next;
            // 执行操作 ③：把 `pre` 的下一个节点指向 `next`。
            pre.next = next;
        }

        return dummyNode.next;
    }
}
