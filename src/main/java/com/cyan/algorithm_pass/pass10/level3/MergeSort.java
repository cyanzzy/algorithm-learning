package com.cyan.algorithm_pass.pass10.level3;

/**
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class MergeSort {

    public void merge(int[] arr, int left, int mid, int right) {
        int[] temp = new int[right - left + 1];
        int i = 0;
        int p1 = left;
        int p2 = mid + 1;

        // 当左右两部分都有剩余元素
        while (p1 <= mid && p2 <= right) {
            temp[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }

        // 当左部分有剩余元素
        while (p1 <= mid) {
            temp[i++] = arr[p1++];
        }

        // 当右部分有剩余元素
        while (p2 <= right) {
            temp[i++] = arr[p2++];
        }

        // 重新拷贝回原始数组对应位置
        for (int j = 0; j < temp.length ; j++) {
            arr[left + j] = temp[j];
        }
    }

    public void mergeSort(int[] arr, int left, int right) {
        if (left == right) {
            return;
        }
        int mid = left + ((right - left) >> 1);
        // 递归归并左部分
        mergeSort(arr, left, mid);
        // 递归归并右部分
        mergeSort(arr, mid + 1, right);
        merge(arr, left, mid, right);
    }

    public void mergeSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        mergeSort(arr, 0, arr.length - 1);
    }
}
