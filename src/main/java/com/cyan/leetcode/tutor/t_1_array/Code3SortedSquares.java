package com.cyan.leetcode.tutor.t_1_array;

import java.util.Arrays;

/**
 * 有序数组的平方
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/squares-of-a-sorted-t_1_array/description/
 * @author Cyan Chau
 * @create 2023-02-15, 2023-10-22
 * @update 2024-04-03
 */
public class Code3SortedSquares {

     // 往期答案
    // 排序法
    public int[] sortedSquares1(int[] nums) {
        int[] ans = new int[nums.length];
        for (int i = 0; i < nums.length; i++) {
            ans[i] = nums[i] * nums[i];
        }
        Arrays.sort(ans);
        return ans;
    }

    // 双指针+归并排序
    public int[] sortedSquares2(int[] nums) {
        int neg = -1;

        // 找出正负分界线
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] < 0) {
                neg = i;
            } else {
                break;
            }
        }
        int[] ans = new int[nums.length];
        int index = 0, i = neg, j = neg + 1;

        // 平方后，分界线左侧呈递减排列，右侧呈递增排列
        while (i >= 0 || j < nums.length) {
            if (i < 0) { // 全是正数
                ans[index] = nums[i] * nums[i];
                ++j;
            } else if (j == nums.length) { // 全是负数
                ans[index] = nums[i] * nums[i];
                --i;
            } else if (nums[i] * nums[i] < nums[j] * nums[j]) { // 如果负数平方小于正数平方
                ans[index] = nums[i] * nums[i];
                // 负数范围缩减
                --i;
            } else { // 如果正数平方大于负数平方
                ans[index] = nums[j] * nums[j];
                ++j;
            }
            // 继续判断下一组
            ++index;
        }
        return ans;
    }

    // 双指针
    public int[] sortedSquares3(int[] nums) {
        int[] ans = new int[nums.length];

        /**
         * 双指针设置在首尾处，然后将平方将较大的逆序放入数组中
         * 大前提：数组是有序的，只能找到最大的
         */
        for (int i = 0, j = nums.length - 1, pos = nums.length - 1; i <= j; i++) {
            if (nums[i] * nums[i] > nums[j] * nums[j]) {
                ans[pos] = nums[i] * nums[i];
                ++i;
            } else {
                ans[pos] = nums[j] * nums[j];
                --j;
            }
            // 逆序放置
            --pos;
        }
        return ans;
    }

    // 一刷 AC
    public int[] sortedSquares(int[] nums) {

        int index = nums.length - 1;
        int left = 0, right = nums.length - 1;
        int[] list = new int[nums.length];

        // 双指针置于首尾，每次平方后比较大小，选择结果较大的倒序放置
        while (left <= right) {
            if (nums[left] * nums[left] <= nums[right] * nums[right]) {
                list[index--] =  nums[right] * nums[right];
                right--;
            } else {
                list[index--] =  nums[left] * nums[left];
                left++;
            }

        }
        return list;
    }

}
