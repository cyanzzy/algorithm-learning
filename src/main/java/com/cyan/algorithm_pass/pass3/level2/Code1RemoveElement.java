package com.cyan.algorithm_pass.pass3.level2;

/**
 * 移除元素
 *
 * @author Cyan Chau
 * @create 2024-01-18
 */
public class Code1RemoveElement {

    // 快慢指针
    public int removeElement1(int[] nums, int val) {

        int slow = 0;

        for (int fast = 0; fast < nums.length; ++fast) {

            if (nums[fast] != val) {
                nums[slow++] = nums[fast];
            }
        }

        return slow;
    }

    // 对撞指针
    public int removeElement(int[] nums, int val) {

        int left = 0;
        int right = nums.length - 1;

        while (left <= right) {
            if (nums[left] == val) {
                nums[left] = nums[right--];
            } else {
                left++;
            }
        }
        return left;
    }

}
