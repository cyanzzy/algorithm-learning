package com.cyan.algorithm_pass.pass4.level2;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 最大栈
 *
 * @author Cyan Chau
 * @create 2024-02-22
 */
public class MaxStack {

    // 存储原始元素
    private Deque<Integer> stack;
    // 存储对应最大元素
    private Deque<Integer> maxStack;

    public MaxStack() {
        this.stack = new LinkedList<>();
        this.maxStack = new LinkedList<>();
    }

    public void push(int x) {
        int max = maxStack.isEmpty() ? x : maxStack.peek();
        maxStack.push(max > x ? max : x);
        stack.push(x);
    }

    public int pop() {
        maxStack.pop();
        return stack.pop();
    }

    public int top() {
        return stack.peek();
    }

    public int peekMax() {
        return maxStack.peek();
    }

    public int popMax() {
        int max = peekMax();
        Deque<Integer> temp = new LinkedList<>();
        while (top() != max) {
            temp.push(pop());
        }
        pop();
        while (!temp.isEmpty()) {
            push(temp.pop());
        }
        return max;
    }
}
