package com.cyan.leetcode.tutor.t_5_stack_queue;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 用队列实现栈
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/implement-stack-using-queues/
 * @author Cyan Chau
 * @create 2024-04-21
 */
public class Code2MyStack {
    /**
     * 失败原因：
     * 算法思想大致没问题，但是思路略不清晰，栈本质是先进后出
     *
     * 正确思路：
     * 把队列当成一个环用，每次都把除了队列末端的元素都出队然后依次加到原本末端元素的后端，
     * 这样原本最后的元素就被推到了队列最前端，实现了 Last In First Out。
     */

//    class MyStack {
//
//        // 维持 Top
//        private Queue<Integer> top;
//        // 存储元素的队列
//        private Queue<Integer> queue;
//
//        public MyStack() {
//            this.top = new LinkedList<>();
//            this.queue = new LinkedList<>();
//        }
//
//        // 入栈：正常入队 queue 即可
//        public void push(int x) {
//            queue.add(x);
//        }
//
//        // 出栈：将 queue 元素依次出队，直到元素剩余一个为止
//        // 将两个队元素互换，此时 pop 指向的元素仅有一个就是栈顶
//        public int pop() {
//
//            if (!top.isEmpty()) {
//                return top.poll();
//            } else {
//                int size = queue.size() - 1;
//                for (int i = 0; i < size; i++) {
//                    top.add(queue.poll());
//                }
//                Queue<Integer> temp = top;
//                top = queue;
//                queue = temp;
//                return top.poll();
//            }
//        }
//
//        public int top() {
//            if (!top.isEmpty()) {
//                return top.peek();
//            } else {
//                int size = queue.size() - 1;
//                for (int i = 0; i < size; i++) {
//                    top.add(queue.poll());
//                }
//                Queue<Integer> temp = top;
//                top = queue;
//                queue = temp;
//                return top.peek();
//            }
//        }
//
//        public boolean empty() {
//            return top.isEmpty() && queue.isEmpty();
//        }
//    }
//
//    public static void main(String[] args) {
//
//    }
}
