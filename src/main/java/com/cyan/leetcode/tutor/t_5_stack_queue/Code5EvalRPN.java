package com.cyan.leetcode.tutor.t_5_stack_queue;

import java.util.Stack;

/**
 * 逆波兰表达式求值
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/evaluate-reverse-polish-notation/
 * @author Cyan Chau
 * @create 2024-04-22
 */
public class Code5EvalRPN {

    public int evalRPN(String[] tokens) {

        // 逆波兰表达式严格遵循「从左到右」的运算
        // 计算逆波兰表达式的值时，使用一个栈存储操作数，从左到右遍历逆波兰表达式
        //  1. 如果遇到操作数，则将操作数入栈；
        //  2. 如果遇到运算符，则将两个操作数出栈，其中先出栈的是右操作数，后出栈的是左操作数，
        // 使用运算符对两个操作数进行运算，将运算得到的新操作数入栈。

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < tokens.length; i++) {
            String token = tokens[i];
            if (isNumber(token)) { // 如果遇到操作数，将操作数入栈
                stack.push(Integer.parseInt(token));
            } else { // 如果遇到运算符，先出栈右操作数，再出栈左操作数
                // 右操作数
                int rightNum = stack.pop();
                // 左操作数
                int leftNum = stack.pop();
                stack.push(calExpression(leftNum, rightNum, token));
            }
        }
        return stack.pop();
    }

    // 计算表达式
    public Integer calExpression(int leftNum, int rightNum, String operator) {

        int ans = 0;
        switch (operator) {
            case "+":
                ans = leftNum + rightNum;
                break;
            case "-":
                ans = leftNum - rightNum;
                break;
            case "*":
                ans = leftNum * rightNum;
                break;
            case "/":
                ans = leftNum / rightNum;
                break;
            default:
        }
        return ans;
    }

    public boolean isNumber(String token) {
        return !("+".equals(token) ||
                 "-".equals(token) ||
                 "*".equals(token) ||
                 "/".equals(token));
    }


}
