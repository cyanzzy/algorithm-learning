package com.cyan.algorithm_pass.pass12.level2;

import java.util.Arrays;

/**
 * 有效字母的异位词
 *
 * @author Cyan Chau
 * @create 2024-03-22
 */
public class IsAnagram {

//    // 排序
//    public boolean isAnagram(String s, String t) {
//        if (s.length() != t.length()) {
//            return false;
//        }
//        char[] chars1 = s.toCharArray();
//        char[] chars2 = t.toCharArray();
//
//        Arrays.sort(chars1);
//        Arrays.sort(chars2);
//
//        return Arrays.equals(chars1, chars2);
//    }

    // 哈希
    public boolean isAnagram(String s, String t) {
        if (s.length() != t.length()) {
            return false;
        }
        int[] frequency = new int[26];
        for (int i = 0; i < s.length(); i++) {
            // 小写字母频次
            frequency[s.charAt(i) - 'a']++;
        }

        for (int i = 0; i < t.length(); i++) {
            frequency[t.charAt(i) - 'a']--;
            if (frequency[t.charAt(i) - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }

}
