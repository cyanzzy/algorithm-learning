package com.cyan.course_base.binarytree.bst;

import com.cyan.course_base.binarytree.Node;

/**
 * BST: 树型DP
 *
 * @author Cyan Chau
 * @create 2023-03-06
 */
public class Code3IsBST {

    public class ReturnData {

        // 该子树是否为BST
        public boolean isBST;
        // 该子树中最小值
        public int min;
        // 该子树中最大值
        public int max;

        public ReturnData(boolean isBST, int min, int max) {
            this.isBST = isBST;
            this.min = min;
            this.max = max;
        }

    }

    public ReturnData process(Node head) {

        // 如果头节点为空，返回null
        if (head == null) {
            return null;
        }

        // 递归检查左子树
        ReturnData leftData = process(head.left);
        // 递归检查右子树
        ReturnData rightData = process(head.right);

        /*核心代码*/

        // 记录右子树的最小值（实际是整颗子树中的最大最小值都取出来）
        int min = head.val;
        // 记录左子树的最大值（实际是整颗子树中的最大最小值都取出来）
        int max = head.val;

        // 求出左孩子最小值与最大值
        if (leftData != null) {
            min = Math.min(min, leftData.min);
            max = Math.max(max, leftData.max);
        }
        // 求出右孩子最小值与最大值
        if (rightData != null) {
            min = Math.min(min, rightData.min);
            max = Math.max(max, rightData.max);
        }

        boolean isBST = true;

        // 当左孩子不是BST或左孩子中的最大值>=head时，返回false
        if (leftData != null && (!leftData.isBST || leftData.max >= head.val)) {
            isBST = false;
        }
        // 当右孩子不是BST或右孩子中的最小值<=head时，返回false
        if (rightData != null && (!rightData.isBST || head.val >= rightData.min)) {
            isBST = false;
        }

        // 返回整合后的信息结构
        return new ReturnData(isBST, max, min);
    }

    public boolean isBST(Node head) {
        return process(head).isBST;
    }
}
