package com.cyan.algorithm_pass.pass4.level2;

import java.util.Deque;
import java.util.LinkedList;

/**
 * 最小栈
 *
 * @author Cyan Chau
 * @create 2024-02-22
 */
public class MinStack {

    // 存储原始元素
    private Deque<Integer> stack;
    // 存储对应最小值
    private Deque<Integer> minStack;


    public MinStack() {
        stack = new LinkedList<>();
        minStack = new LinkedList<>();
        minStack.push(Integer.MAX_VALUE);
    }

    // 入栈
    public void push(int x) {
        // 压入原始值
        stack.push(x);
        // 叫小部分压入栈
        minStack.push(Math.min(x, minStack.peek()));
    }

    // 出栈
    public void pop() {
        stack.pop();
        minStack.pop();
    }

    // 顶部元素
    public int top() {
        return stack.peek();
    }

    // getMin
    public int getMin() {
        return minStack.peek();
    }

}
