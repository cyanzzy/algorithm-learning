package com.cyan.algorithm_pass.pass8.level2;

import com.cyan.algorithm_pass.TreeNode;

/**
 * 平衡二叉树
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class IsBalanced {

    // 自底向上
//    public boolean isBalanced(TreeNode root) {
//        return height(root) >= 0;
//    }
//
//    public int height(TreeNode node) {
//        if (node == null) {
//            return 0;
//        }
//        int leftHeight = height(node.left);
//        int rightHeight = height(node.right);
//        if (leftHeight == -1 || rightHeight == -1 || Math.abs(leftHeight - rightHeight) > 1) {
//            return -1;
//        } else {
//            return Math.max(leftHeight, rightHeight) + 1;
//        }
//
//    }

    // 自顶向下
    public boolean isBalanced(TreeNode root) {
        if (root == null) {
            return true;
        } else {
            return Math.abs(height(root.left) - height(root.right)) <= 1 && isBalanced(root.left) && isBalanced(root.right);
        }

    }

    public int height(TreeNode root) {
        if (root == null) {
            return 0;
        } else {
            return Math.max(height(root.left), height(root.right)) + 1;
        }
    }


}
