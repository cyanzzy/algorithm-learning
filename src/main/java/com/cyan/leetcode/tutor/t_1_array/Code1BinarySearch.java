package com.cyan.leetcode.tutor.t_1_array;

/**
 *  二分查找
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/binary-search/
 * @author Cyan Chau
 * @create 2023-01-04, 2023-10-22
 * @update 2024-04-03
 */
public class Code1BinarySearch {

    public int search(int[] nums, int target) {

        // 定义双指针
        int left = 0, right = nums.length - 1;

        while (left <= right) {

            // 防止溢出
            int mid = left + ((right - left) >> 1);
            // 二分搜索
            if (target < nums[mid]) { // 说明在左半段
                right = mid - 1;
            } else if (target > nums[mid]) { // 说明在右半段
                left = mid + 1;
            } else {
                return mid; // 命中
            }
        }
        // 找不到返回 -1
        return -1;
    }

}
