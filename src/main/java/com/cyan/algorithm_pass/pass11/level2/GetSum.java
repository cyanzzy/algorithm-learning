package com.cyan.algorithm_pass.pass11.level2;

/**
 * 两整数之和
 *
 * @author Cyan Chau
 * @create 2024-03-12
 */
public class GetSum {

    /**
     *对于整数 `a` 和 `b`：
     * - 在不考虑进位的情况下，其无进位加法结果为 `a⊕b`
     * - 而所有需要进位的位为 `a & b`，进位后的进位结果为 `(a & b) << 1`
     * 可以将整数 `a` 和 `b` 的和，拆分为 `a` 和 `b` 的无进位加法结果与进位结果的和
     */
    public int getSum(int a, int b) {

        while (b != 0) {
            int carry = (a & b) << 1;
            a = a ^ b;
            b = carry;
        }
        return a;
    }
}
