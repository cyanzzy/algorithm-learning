package com.cyan.course_base.sort_application;

/**
 * 荷兰旗问题
 *
 * @author Cyan Chau
 * @create 2023-02-20
 */
public class Code4Netherlands {

    public void partition(int[] arr, int left, int right, int target) {
        // 声明小于等于的边界
        int lessAndEqualBoundary = left - 1;
        int i = 0;

        while (i < arr.length) {
            if (arr[i] <= target) {
                // 如果arr[i] <= target。arr[i]和小于等于边界下一个数交换
                swap(arr, ++lessAndEqualBoundary, i++);
            } else {
                // 如果arr[i] > target，i++
                i++;
            }
        }
    }

    public int[] Netherlands(int[] arr, int left, int right, int target) {
        int i = left - 1; // i 表示小于区域的右边界
        int k = right + 1; // k 表示大于区域的左边界

        while (left < k) {
            if (arr[left] < target) {
                swap(arr, ++i, left++); // 如果当前数小于num，当前数和小于区域下一个数交换，小于区域右扩（++i），left++
            } else if (arr[left] > target) { // 如果当前数大于num，当前数和大于区域前一个数交换，大于区域左扩（--k），由于双方刚交换完毕，left位置的数是大于区域交换过来的，未经检查，不能动left
                swap(arr, --k, left);
            } else { // 如果当前数等于num，直接下一轮迭代，也就是left++
                left++;
            }
        }
        // target边界
        return new int[] {i + 1, k - 1}; // 返回等于区域的左右边界
    }

    public void swap(int[] arr, int i, int j) {
        int temp;
        temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }
}
