package com.cyan.algorithm_pass.pass13.level1;

/**
 * 阶乘尾数
 *
 * @author Cyan Chau
 * @create 2024-03-23
 */
public class TrailingZeroes {

    // 比如10！中出现了2个5，所以10！就有2个尾数0
    public int trailingZeroes(int n) {
        int ans = 0;
        while (n != 0) {
            n /= 5;
            ans += n;
        }
        return ans;
    }
}
