package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 移除链表元素
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code9RemoveElements {

    // 迭代删除
    public ListNode removeElements1(ListNode head, int val) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;
        ListNode cur = dummyNode;

        while (cur.next != null) {
            if (cur.next.val != val) {
                cur = cur.next;
            } else {
                cur.next = cur.next.next;
            }
        }
        return dummyNode.next;
    }

    // 递归删除
    public ListNode removeElements(ListNode head, int val) {
        // base case
        if (head == null) {
            return head;
        }
        head.next = removeElements(head.next,val);
        return head.val == val ? head.next : head;
    }
}
