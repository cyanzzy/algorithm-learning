package com.cyan.course_base.trie;

/**
 * 前缀树节点结构
 *
 * @author Cyan Chau
 * @create 2023-08-08
 */
public class TrieNode {
    // 节点到达次数
    public int path;
    // 标记是否到达终点
    public int end;
    // 数组大小根据实际字符集长度决定
    public TrieNode[] children;
    // nexts[0] == null 没有走向'a'的路
    // nexts[1] != null 有走向'a'的路
    // ....
    // nexts[25] != null 有走向'z'的路

    public TrieNode() {
        path = 0;
        end = 0;
        children = new TrieNode[26];
    }
}
