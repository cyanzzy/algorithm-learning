package com.cyan.algorithm_pass.pass2.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.Stack;

/**
 * 链表加一
 *
 * @author Cyan Chau
 * @create 2024-01-17
 */
public class Code3PlusOne {


    public ListNode plusOne(ListNode head) {

        Stack<Integer> stack = new Stack<>();

        // 将链表元素入栈
        while (head != null) {
            stack.push(head.val);
            head = head.next;
        }

        // 记录进位
        int carry = 0;
        ListNode dummyNode = new ListNode(0);
        int adder = 1;

        while (!stack.isEmpty() || carry > 0) {
            // 弹出数字位
            int digit = stack.empty() ? 0 : stack.pop();
            int sum = digit + adder + carry;
            // 进位
            carry = sum >= 10 ? 1 : 0;
            // 考虑进位后的 sum
            sum = sum >= 10 ? sum - 10 : sum;
            ListNode cur = new ListNode(sum);
            cur.next = dummyNode.next;
            dummyNode.next = cur;
            adder = 0;
        }

        return dummyNode.next;
    }

}
