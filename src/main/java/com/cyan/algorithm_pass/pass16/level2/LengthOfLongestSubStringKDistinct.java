package com.cyan.algorithm_pass.pass16.level2;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * 至多包含 K 个不同字符的最长子串
 *
 * @author Cyan Chau
 * @create 2024-04-29
 */
public class LengthOfLongestSubStringKDistinct {

    public int lengthOfLongestSubStringKDistinct(String s, int k) {
        if (s.length() < k + 1) {
            return s.length();
        }
        int left = 0, right = 0;
        Map<Character, Integer> hashMap = new HashMap<>();
        int maxLen = k;

        while (right < s.length()) {
            if (hashMap.size() < k + 1) {
                hashMap.put(s.charAt(right), right++);
            }
            // 如果数量达到 3
            if (hashMap.size() == k + 1) {
                // 最左侧待删除的为止
                int del_idex = Collections.min(hashMap.values());
                hashMap.remove(s.charAt(del_idex));
                // 窗口 left 的新位置
                left = del_idex + 1;
            }
            maxLen = Math.max(maxLen, right - left);
        }
        return maxLen;
    }
}
