package com.cyan.algorithm_pass.pass3.level3;

import java.util.HashMap;
import java.util.Map;

/**
 * 数组出现次数超过一半的数字
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code1MajorityElement {

    // 哈希统计
    public int majorityElement1(int[] arr) {

        if (arr == null) {
            return 0;
        }
        Map<Integer, Integer> ans = new HashMap<>();
        for (int i = 0; i < arr.length; i++) {
            ans.put(arr[i], ans.getOrDefault(arr[i], 0) + 1);
            if (ans.get(arr[i]) > arr.length / 2) {
                return arr[i];
            }
        }
        return 0;
    }

    // 摩尔投票
    public int majorityElement(int[] nums) {

        int candidate = 0;
        int count = 0;

        for (int num : nums) {
            if (count == 0) {
                candidate = num;
            }

            count += (num == candidate) ? 1 : -1;
        }

        return candidate;
    }
}
