package com.cyan.leetcode.tutor.t_5_stack_queue;

import com.sun.org.apache.regexp.internal.RE;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * 有效的括号
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/valid-parentheses/
 * @author Cyan Chau
 * @create 2024-04-21
 */
public class Code3IsValid {

    public boolean isValid(String s) {

        // base case
        // 如果不是偶数，直接 pass
        if (s.length() % 2 == 1) {
            return false;
        }

        // 匹配字典
        Map<Character, Character> map = new HashMap<>();
        map.put(')', '(');
        map.put(']', '[');
        map.put('}', '{');

        Stack<Character> stack = new Stack<>();
        // 循环遍历字符串
        for (int i = 0; i < s.length(); i++) {
            // 遍历当前字符
            Character ch = s.charAt(i);
            // 如果是右括号
            if (map.containsKey(ch)) {
                // 1. 如果栈为空，说明无法匹配
                // 2. 如果右括号与栈顶括号（左括号）不匹配
                if (stack.isEmpty() ||
                    map.get(ch) != stack.peek()) {
                    return false;
                }
                // 如果右括号能匹配相应的左括号，则出栈栈顶元素
                stack.pop();
            } else {
                // 如果是左括号
                stack.push(ch);
            }
        }
        // 栈中没有元素，菜鸟证明完全匹配
        return stack.isEmpty();
    }

}
