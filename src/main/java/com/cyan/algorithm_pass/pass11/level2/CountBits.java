package com.cyan.algorithm_pass.pass11.level2;

/**
 * 比特位计数
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class CountBits {

//    // 原始方法
//    // 对 0-->num 每个数计算比特数，每个 int 型数都可以用
//    public int[] countBits(int num) {
//        int length = num + 1;
//        int[] bits = new int[length];
//        for (int i = 0; i < length; i++) {
//            for (int j = 0; j < 32; j++) {
//                // 用于获取整数 i 二进制表示中第 j 位的值
//                bits[i] += (i >> j) & 1;
//            }
//        }
//        return bits;
//    }

    // 优化算法
    public int[] countBits(int n) {
        int[] bits = new int[n + 1];
        for (int i = 0; i <= n; i++) {
            bits[i] = countOnes(i);
        }
        return bits;
    }

    public int countOnes(int x) {
        int ones = 0;
        while (x > 0) {
            x &= (x - 1);
            ones++;
        }
        return ones;
    }

}
