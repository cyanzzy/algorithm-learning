package com.cyan.algorithm_pass.pass11.level1;

/**
 * 位运算常见技巧
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class Skills {


    /**
     * n & (n−1)
     * 其运算结果恰为把 `n` 的二进制位中的最低位的 `1` 变为 `0`
     */
    public int setLowestBinIndex(int num) {
        return num & (num - 1);
    }

    /**
     *用于获取整数 `num` 的二进制表示中指定位置 `index` 的位的函数
     */
    public boolean getBit(int num, int index) {
        return ((num & (1 << index)) != 0);
    }

    /**
     * 用于将整数 `num` 的二进制表示中指定位置 `index` 的位设置为 `1` 的函数
     */
    public int setBit(int num, int index) {
        return num | (1 << index);
    }

    /**
     * 用于将整数 `num` 的二进制表示中指定位置 `index` 的位清零（设置为 0）的函数
     */
    public int clearBit(int num, int index) {
        int mask = ~(1 << index);
        return num & mask;
    }

    /**
     *  用于更新整数 `num` 的二进制表示中指定位置 `index` 的位为给定值 `value` 的函数
     */
    public int updateBit(int num, int index, int value) {
        int mask = ~(1 << index);
        return (num & mask) | (value << index);
    }
}
