package com.cyan.algorithm_pass;

/**
 * LeetCode ListNode Structure
 *
 * @author Cyan Chau
 * @create 2023-12-05
 */
public class ListNode {

    public int val;
    public ListNode next;
    
    public ListNode(int x) {
        val = x;
        next = null;
    }
}
