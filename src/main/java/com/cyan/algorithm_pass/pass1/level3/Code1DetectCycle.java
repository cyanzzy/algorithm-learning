package com.cyan.algorithm_pass.pass1.level3;

import com.cyan.algorithm_pass.ListNode;

import java.util.HashSet;
import java.util.Set;

/**
 * 环形链表 Ⅱ
 *
 * @author Cyan Chau
 * @create 2024-01-14
 */
public class Code1DetectCycle {

    // 哈希
    public ListNode detectCycle1(ListNode head) {
        ListNode pos = head;

        Set<ListNode> visited = new HashSet<>();

        while (pos != null) {
            if (visited.contains(pos)) {
                return pos; // 入环口
            } else {
                visited.add(pos);
            }
            pos = pos.next;
        }
        return null;
    }

    // 快慢指针
    public ListNode detectCycle(ListNode head) {

        if (head == null) {
            return null;
        }
        ListNode slow = head, fast = head;

        while (fast != null) {
            slow = slow.next;
            if (fast.next != null) {
                fast = fast.next.next;
            } else {
                return null; // 说明无环
            }

            if (fast == slow) { // 如果快慢指针相遇，说明存在环
                ListNode ptr = head;
                while (ptr != slow) {
                    ptr = ptr.next;
                    slow = slow.next;
                }
                // 返回入环口
                return ptr;
            }
        }
        return null;
    }
}
