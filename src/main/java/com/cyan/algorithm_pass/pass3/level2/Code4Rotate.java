package com.cyan.algorithm_pass.pass3.level2;

/**
 * 轮转数组
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code4Rotate {

    public void rotate(int[] nums, int k) {

        k %= nums.length;

        reverse(nums, 0, nums.length - 1);
        reverse(nums, 0, k - 1);
        reverse(nums, k, nums.length - 1);

    }

    public void reverse(int[] nums, int begin, int end) {

        while (begin < end) {
            int temp = nums[begin];
            nums[begin] = nums[end];
            nums[end] = temp;
            begin++;
            end--;
        }
    }

}
