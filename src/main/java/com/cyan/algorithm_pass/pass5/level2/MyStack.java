package com.cyan.algorithm_pass.pass5.level2;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 使用队列实现栈
 *
 * @author Cyan Chau
 * @create 2024-03-02
 */
//public class MyStack { // 双队列实现
//
//    // 用于存储栈内的元素
//    private Queue<Integer> queue1;
//    // 入栈操作的辅助队列
//    private Queue<Integer> queue2;
//
//    public MyStack() {
//        queue1 = new LinkedList<>();
//        queue2 = new LinkedList<>();
//    }
//
//    // 入栈
//    public void push(int x) {
//        // 首先将元素入队到 queue2
//        queue2.offer(x);
//        // 然后将 `queue1` 的全部元素依次出队并入队到 `queue2`
//        // 此时 `queue2` 的前端的元素即为新入栈的元素
//        while (!queue1.isEmpty()) {
//            queue2.offer(queue1.poll());
//        }
//        // 再将 `queue1` 和 `queue2` 互换，则 `queue1` 的元素即为栈内的元素
//        // `queue1` 的前端和后端分别对应栈顶和栈底
//        Queue<Integer> temp = queue1;
//        queue1 = queue2;
//        queue2 = temp;
//    }
//
//    // 出栈
//    public int pop() {
//        return queue1.poll();
//    }
//
//    // 取栈顶
//    public int top() {
//        return queue1.peek();
//    }
//
//    // 判空
//    public boolean empty() {
//        return queue1.isEmpty();
//    }
//}
class MyStack { // 单队列实现
    Queue<Integer> queue;

    /** Initialize your data structure here. */
    public MyStack() {
        queue = new LinkedList<Integer>();
    }

    /** Push element x onto stack. */
    public void push(int x) {
        int n = queue.size();
        queue.offer(x);
        for (int i = 0; i < n; i++) {
            queue.offer(queue.poll());
        }
    }

    /** Removes the element on top of the stack and returns that element. */
    public int pop() {
        return queue.poll();
    }

    /** Get the top element. */
    public int top() {
        return queue.peek();
    }

    /** Returns whether the stack is empty. */
    public boolean empty() {
        return queue.isEmpty();
    }
}
