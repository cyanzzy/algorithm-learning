package com.cyan.course_base.binarytree.order.unrecursion;

import com.cyan.course_base.binarytree.Node;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 中序遍历 非递归版本
 *
 * @author Cyan Chau
 * @create 2023-03-01
 */
public class InOrder {

    public List<Node> inorder(Node head) {
        List<Node> ans = new ArrayList<>();
        Stack<Node> stack = new Stack<>();

        if (head == null) {
            return ans;
        }

        while (!stack.isEmpty() || head != null) {
            // 一直往左分支走
            while (head != null) {
                stack.push(head);
                head = head.left;
            }

            if (!stack.isEmpty()) {
                head = stack.pop();
                ans.add(head); // Visit(head)
                // 转向右分支
                head = head.right;
            }

        }
        return ans;
    }
}
