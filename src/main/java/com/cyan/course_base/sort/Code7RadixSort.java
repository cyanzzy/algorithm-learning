package com.cyan.course_base.sort;

/**
 * RadixSort
 *  复盘：
 *
 *
 * @author Cyan Chau
 * @create 2023-03-20
 */
public class Code7RadixSort {

    /**
     * 算法思想：
     * 多关键字排序，最低位优先。
     * 最低位关键字通过”分配“和”收集“的思想排成若干子序列，再对每个子序列进行更高位排序
     *
     */

    // 求最大值的十进制位数，比如max = 254256,那么返回6，用于控制迭代轮数
    public int maxbits(int[] arr) {
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < arr.length; i++) {
            max = Math.max(max, arr[i]);
        }
        int res = 0;
        while (max != 0) {
            res++;
            max /= 10;
        }
        return res;
    }

    // 将d位上的数字取出
    public int getDigit(int x, int d) {
        return ((x / ((int) Math.pow(10, d - 1))) % 10);
    }

    public void radixSort(int[] arr, int begin, int end, int digit) {

        // 十位数字 0...9
        final int radix = 10;
        int i = 0, j = 0;

        // 定义辅助数组
        int[] bucket = new int[end - begin + 1];
        for (int d = 1; d <= digit; d++) { // 迭代轮数
            // count数组记录词频前缀和
            int[] count = new int[radix];
            // 位数词频统计，d = 1 代表个位，d = 2 代表十位..
            for (i = begin; i <= end; i++) {
                j = getDigit(arr[i], d);
                count[j]++;
            }

            // 优化成前缀和数组
            for (i = 1; i < radix; i++) {
                count[i] = count[i] + count[i - 1];
            }

            // 更新辅助数组，一趟进出桶
            for (i = end; i >= begin; i--) {
                j = getDigit(arr[i], d);
                bucket[count[j] - 1] = arr[i];
                count[j]--;
            }

            // 结束前将桶元素放入数组中
            for (i = begin, j = 0; i <= end; i++, j++) {
                arr[i] = bucket[j];
            }
        }
    }

    // 主函数调用
    public void radixSort(int[] arr) {
        //非法情况
        if (arr == null || arr.length < 2) {
            return;
        }
        radixSort(arr, 0, arr.length - 1, maxbits(arr));
    }

    public void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    // for test
    public int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Math.abs((int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random()));
        }
        return arr;
    }
    public static void main(String[] args) {
        Code7RadixSort sort = new Code7RadixSort();
       // int testTime = 500000;
        int maxSize = 100000;
        int maxValue = 10000000;

        int[] arr = sort.generateRandomArray(maxSize, maxValue);
        System.out.println("原始数据");
        sort.printArray(arr);
        System.out.println("数据量\t" + arr.length);

        long begin = System.currentTimeMillis();

        System.out.println("排序后数据");
        sort.radixSort(arr);
        sort.printArray(arr);
        long end = System.currentTimeMillis();

        System.out.println("实际运行时间\t" + (end - begin) + "ms");


    }

}
