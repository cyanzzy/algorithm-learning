package com.cyan.algorithm_pass.pass13.level1;

/**
 * 数组元素积的符号
 *
 * @author Cyan Chau
 * @create 2024-03-22
 */
public class ArraySign {

    public int arraySign(int[] nums) {
        int sign = 1;
        for (int num : nums) {
            if (num == 0) {
                return 0;
            }
            if (num < 0) {
                sign = -sign;
            }
        }
        return sign;
    }
}
