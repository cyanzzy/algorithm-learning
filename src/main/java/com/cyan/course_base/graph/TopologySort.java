package com.cyan.course_base.graph;

import java.util.*;

/**
 * @author Cyan Chau
 * @create 2023-04-03
 */
public class TopologySort {

    public List<Node> sortedTopology(Graph graph) {
        // inMap存储节点的入度
        Map<Node, Integer> inMap = new HashMap<>();
        // zeroInQueue存储入度为0的节点
        Queue<Node> zeroInQueue  = new LinkedList<>();

        // 图的所有顶点的入度记录下来
        for (Node node : graph.nodes.values()) {
            // 存入node的入度
            inMap.put(node, node.in);
            if (node.in == 0) {
                zeroInQueue.add(node);
            }
        }
        // 存储拓扑排序结果
        List<Node> result = new ArrayList<>();

        while (!zeroInQueue.isEmpty()) {
            // 队列中挑选一个入度为0的节点存储在结果集中
            Node cur = zeroInQueue.poll();
            result.add(cur);
            for (Node next : cur.nexts) {
                // 更新cur节点所有邻接节点的入度
                inMap.put(next, inMap.get(next) - 1);
                // 一旦入度为0，则送入队列
                if (inMap.get(next) == 0) {
                    zeroInQueue.add(next);
                }
            }
        }
        return result;
    }
}
