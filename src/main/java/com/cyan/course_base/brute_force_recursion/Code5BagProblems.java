package com.cyan.course_base.brute_force_recursion;

/**
 * 背包问题
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code5BagProblems {

    /**
     * 背包问题
     *
     * @param weights 货物重量
     * @param values 货物价值
     * @param i i位置
     * @param alreadyweight 之前选择货物所达到的重量
     * @param bag 载重
     * @return  [i..]的货物自由选则，形成最大价值返回
     */
    public int process(int[] weights, int[] values, int i, int alreadyweight, int bag) {

        if (alreadyweight > bag) { // 违规条件
            return 0;
        }

        if (i == weights.length) { // 终止条件
            // 到了末尾没有东西可拿，此刻价值为0
            return 0;
        }


        /**
         * 我们来到 i 位置，提供两条路径
         *
         * 路径1：选择当前 i 位置的货物
         * 路径2：不选择当前 i 位置的货物
         *
         * 最后返回两种路径中最大的货物价值s
         */
        return Math.max(
                // 路径1 选择当前货物，价值则加上选择的，同时背包重量相应增加，并进入下一层递归，继续选择
                values[i] + process(weights, values, i + 1, alreadyweight + weights[i], bag),
                // 路径2 不选择当前货物，价值和背包重量不变，并进入下一层递归，继续选择
                process(weights, values, i + 1, alreadyweight, bag));
    }
}
