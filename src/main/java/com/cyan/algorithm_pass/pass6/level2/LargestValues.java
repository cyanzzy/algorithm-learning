package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * 在每个树行中找最大值
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class LargestValues {

    public List<Integer> largestValues(TreeNode root) {

        if (root == null) {
            return new ArrayList<>();
        }
        // 结果集
        List<Integer> res = new ArrayList<>();
        // 队列
        Queue<TreeNode> queue = new ArrayDeque<>();
        // 根节点入队
        queue.offer(root);

        // 当队列不为空时
        while (!queue.isEmpty()) {
            // 当前层元素个数
            int cnt = queue.size();
            // max val
            int levelMaxVal = Integer.MIN_VALUE;

            for (int i = 0; i < cnt; ++i) {
                TreeNode node = queue.poll();
                levelMaxVal = Math.max(levelMaxVal, node.val);
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            res.add(levelMaxVal);
        }
        return res;
    }

}
