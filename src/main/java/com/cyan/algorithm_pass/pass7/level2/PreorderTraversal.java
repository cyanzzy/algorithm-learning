package com.cyan.algorithm_pass.pass7.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.*;

/**
 * 前序遍历
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class PreorderTraversal {

    public List<Integer> preorderTraversal(TreeNode head) {

        // res
        List<Integer> res = new ArrayList<>();
        if (head != null) {
            // 定义辅助栈
            Stack<TreeNode> stack = new Stack<>();
            // 根节点入栈
            stack.add(head);

            // 先访问根节点，再访问左子树，然后访问右子树
            while (!stack.isEmpty()) {
                // 弹出栈顶一个节点
                head = stack.pop();
                res.add(head.val);
                // 先入栈该节点的右孩子 （栈的先进后出性质）
                if (head.right != null) {
                    stack.push(head.right);
                }
                // 再入栈该节点的左孩子
                if (head.left != null) {
                    stack.push(head.left);
                }
            }
        }
        return res;
    }

}
