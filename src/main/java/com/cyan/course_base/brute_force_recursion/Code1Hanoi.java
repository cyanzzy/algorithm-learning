package com.cyan.course_base.brute_force_recursion;

/**
 * 汉诺塔问题
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code1Hanoi {

    public void process(int index, String from, String to, String other) {
        if (index == 1) { // base case 如果仅剩一个盘子，则直接放到to位置
            System.out.println("将" + index + "从" + from + "移动到" + to);
        } else {
            // i - 1, from -> other
            process(index - 1, from, other, to);
            // i, fom -> to
            System.out.println("将" + index + "从" + from + "移动到" + to);
            //i - 1, other -> to
            process(index - 1, other, to, from);
        }
    }
    public void hanoi(int n) {
        if (n > 0) {
            process(n, "左", "右", "中");
        }
    }

    public static void main(String[] args) {
        new Code1Hanoi().hanoi(100);
    }
}
