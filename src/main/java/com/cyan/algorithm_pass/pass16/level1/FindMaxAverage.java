package com.cyan.algorithm_pass.pass16.level1;

/**
 * 子数组最大平均数
 *
 * @author Cyan Chau
 * @create 2024-04-29
 */
public class FindMaxAverage {

    // 固定窗口大小
    public double findMaxAverage(int[] nums, int k) {

        int sum = 0;
        int n = nums.length;

        // 计算初始窗口的值，窗口大小 k
        for (int i = 0; i < k; i++) {
            sum += nums[i];
        }
        int maxSum = sum;
        // 窗口右移动：end++
        for (int end = k; end < n; end++) {
            sum = sum - nums[end - k] + nums[end];
            maxSum = Math.max(maxSum, sum);
        }
        return 1.0 * maxSum / k;
    }
}
