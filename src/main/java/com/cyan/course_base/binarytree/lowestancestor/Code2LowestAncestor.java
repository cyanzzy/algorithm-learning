package com.cyan.course_base.binarytree.lowestancestor;

import com.cyan.course_base.binarytree.Node;

import java.util.HashMap;
import java.util.HashSet;

/**
 * 结构1： 原始结构
 *
 * @author Cyan Chau
 * @create 2023-03-23
 */
public class Code2LowestAncestor {

    public class Record {
        // 定义哈希表表示 <子结点, 父结点>
        private HashMap<Node, Node> map;

        public Record(Node head) {
            map = new HashMap<>();
            // 如果没有父结点
            if (head != null) {
                map.put(head, null);
            }
            setMap(head);
        }

        // 构造map
        public void setMap(Node x) {
            // 如果结点为空，则返回
            if (x == null) {
                return;
            }
            // 如果左孩子存在，设置左孩子的父结点
            if (x.left != null) {
                map.put(x.left, x);
            }
            // 如果右孩子存在，设置右孩子的父结点
            if (x.right != null) {
                map.put(x.right, x);
            }
            // 递归调用左子树
            setMap(x.left);
            // 递归调用右子树
            setMap(x.right);
        }

        public Node query(Node o1, Node o2) {
            // 存储包括o1结点在内的所有o1结点的祖先结点
            // path表示从结点o1到头结点这条路径上所有结点的和
            HashSet<Node> path = new HashSet<>();

            // 填充path
            while (map.containsKey(o1)) {
                path.add(o1);
                //获取o1的父节点
                o1 = map.get(o1);
            }
            // 遍历o2的所有父结点与path中进行核对，找出最近公共祖先
            while (!path.contains(o2)) {
                o2 = map.get(o2);
            }
            return o2;
        }
    }
}
