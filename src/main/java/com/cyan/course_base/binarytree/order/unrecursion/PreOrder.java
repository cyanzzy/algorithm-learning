package com.cyan.course_base.binarytree.order.unrecursion;

import com.cyan.course_base.binarytree.Node;

import java.util.Stack;

/**
 * 先序遍历 非递归版本
 *
 * @author Cyan Chau
 * @create 2023-03-01
 */
public class PreOrder {

    public void preOrder(Node head) {

        if (head != null) {
            // 定义辅助栈
            Stack<Node> stack = new Stack<>();
            // 根节点入栈
            stack.add(head);

            while (!stack.isEmpty()) {
                // 弹出栈顶一个节点
                head = stack.pop(); // 先访问根节点
                // Visit(head);
                // 先入栈该节点的右孩子（栈的先进后出性质）
                if (head.right != null) {
                    stack.push(head.right);
                }
                // 再入栈该节点的左孩子
                if (head.left != null) {
                    stack.push(head.left);
                }
            }
        }
    }
}
