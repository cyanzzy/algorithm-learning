package com.cyan.course_base.sort;

/**
 * QuickSort
 *
 * @author Cyan Chau
 * @create 2023-02-13
 */
public class Code6QuickSort {
    /**
     * 算法思想：
     *
     * 通过多次枢轴划分实现操作，每趟选择当前所有子序列中的一个关键字作为枢轴，
     * 将子序列中比枢轴小的移动到枢轴前面，比枢轴大的移动到枢轴后面，划分结束，
     * 继续下一趟划分直到整个序列有序。
     */

    // 处理arr[1..r]的函数
    // 默认以arr[r]做划分，arr[r] --> p， <p ==p >p
    // 返回等于区域(左边界，右边界)
//    public int[] partition(int[] arr, int left, int right) {
////        int lessZoneRightBoundary = left - 1; // 小于区域右边界
////        int moreZoneLeftBoundary = right; // 大于区域左边界
////
////        while (left < moreZoneLeftBoundary) { // 小于区域右边界
////            if (arr[left] < arr[right]) { // 当前数小于划分值
////                swap(arr, ++lessZoneRightBoundary, left++); // 把当前数和小于区域的下一个数交换，小于区域右括，left++
////            } else if (arr[left] > arr[right]) { // 当前数大于划分值
////                swap(arr, --moreZoneLeftBoundary, left); // 把当前数和大于区域的前一个数交换，大于区域左扩，left不变，因为数从右边区域交换完没有检查
////            } else { // 等于区域
////                left++; // 直接跳下一个
////            }
////        }
////        swap(arr, moreZoneLeftBoundary, right);
////        return new int[] {lessZoneRightBoundary + 1, moreZoneLeftBoundary}; // 等于区域的左右边界
////    }

    public int[] partition(int[] arr, int left, int right) {
        int lessZoneRightBoundary = left - 1; // 小于区域右边界
        int moreZoneLeftBoundary = right + 1; // 大于区域左边界

        while (left < moreZoneLeftBoundary) { // 小于区域右边界
            if (arr[left] < arr[right]) { // 当前数小于划分值
                swap(arr, ++lessZoneRightBoundary, left++); // 把当前数和小于区域的下一个数交换，小于区域右括，left++
            } else if (arr[left] > arr[right]) { // 当前数大于划分值
                swap(arr, --moreZoneLeftBoundary, left); // 把当前数和大于区域的前一个数交换，大于区域左扩，left不变，因为数从右边区域交换完没有检查
            } else { // 等于区域
                left++; // 直接跳下一个
            }
        }

        return new int[] {lessZoneRightBoundary + 1 , moreZoneLeftBoundary - 1}; // 等于区域的左右边界
    }

    public void quickSort(int[] arr, int left, int right) {
        if (left < right) {
            // 等概率随机选一个位置，把他跟最右边的数字交换
            swap(arr, left + (int)(Math.random() * (right - left + 1)), right);
            // 进行partition，返回数组长度为2，划分值等于区域范围的左右边界
            int[] p = partition(arr, left, right);
            quickSort(arr, left, p[0] - 1); // p[0] - 1：小于区域右边界
            quickSort(arr, p[1] + 1, right); // p[1] + 1：大于区域左边界
        }
    }

    public void quickSort(int[] arr) {
        if (arr == null || arr.length < 2) {
            return;
        }
        quickSort(arr, 0, arr.length - 1);
    }

    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    public void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    // for test
    public int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Math.abs((int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random()));
        }
        return arr;
    }
    public static void main(String[] args) {
        Code6QuickSort sort = new Code6QuickSort();
        // int testTime = 500000;
        int maxSize = 100000;
        int maxValue = 10000000;

        int[] arr = sort.generateRandomArray(maxSize, maxValue);
        System.out.println("原始数据");
        sort.printArray(arr);
        System.out.println("数据量\t" + arr.length);

        long begin = System.currentTimeMillis();

        System.out.println("排序后数据");
        sort.quickSort(arr);
        sort.printArray(arr);
        long end = System.currentTimeMillis();

        System.out.println("实际运行时间\t" + (end - begin) + "ms");


    }
}
