package com.cyan.algorithm_pass.pass3.level2;

import java.util.ArrayList;
import java.util.List;

/**
 * 缺失的区间
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code6FindMissingRanges {

    public List<String> findMissingRanges(int[] nums, int lower, int upper) {

        List<String> result = new ArrayList<>();
        // 初始化 `prev` 为 `lower - 1`，用于跟踪上一个处理过的元素
        long prev = (long) lower - 1;  // 使用 long 类型避免整数溢出问题

        for (int num : nums) {
            // 若当前元素（`num`）恰好比 `prev` 大 1，说明它们之间没有缺失的区间，直接继续下一个元素（连续序列）

            // 若当前元素（`num`）恰好比 `prev` 大 2，说明它们之间缺失一个序列（缺失单个序列）
            if (num == prev + 2) {
                result.add(String.valueOf(prev + 1));
            } else if (num > prev + 2) {
                // 若两者相差超过 2，说明它们之间缺失连续序列（缺失若干序列）
                result.add((prev + 1) + "->" + (num - 1));
            }
            prev = num;
        }

        // 检查最后一个区间
        if (upper > prev + 1) {
            // `prev + 1` 和 `upper` 之间存在缺失的区间
            result.add((prev + 1) + "->" + upper);
        } else if (upper == prev + 1) {
            // 将单个缺失的数字 `upper` 添加到 `result` 列表中
            result.add(String.valueOf(upper));
        }
        return result;
    }
}
