package com.cyan.algorithm_pass.pass18.level2;

import java.util.ArrayList;
import java.util.List;

/**
 * 括号生成
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class GenerateParenthesis {

    public List<String> generateParenthesis(int n) {

        List<String> ans = new ArrayList<>();

        dfs(ans, new StringBuilder(), 0, 0, n);
        return ans;
    }

    /**
     * @param ans 当前递归得到的结果
     * @param cur 当前的括号串
     * @param open 左括号已经使用的个数
     * @param close 右括号已经使用的个数
     * @param max 序列长度最大值
     */
    public void dfs(List<String> ans, StringBuilder cur, int open, int close, int max) {
        if (cur.length() == max * 2) {
            ans.add(cur.toString());
            return;
        }
        if (open < max) {
            cur.append('(');
            dfs(ans, cur, open + 1, close, max);
            cur.deleteCharAt(cur.length() - 1);
        }
        if (close < open) {
            cur.append(')');
            dfs(ans, cur, open, close + 1, max);
            cur.deleteCharAt(cur.length() - 1);
        }
    }


}
