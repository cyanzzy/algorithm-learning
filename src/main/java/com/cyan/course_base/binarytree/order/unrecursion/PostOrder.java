package com.cyan.course_base.binarytree.order.unrecursion;

import com.cyan.course_base.binarytree.Node;

import java.util.Stack;

/**
 * 后序遍历 非递归版本
 *
 * @author Cyan Chau
 * @create 2023-03-01
 */
public class PostOrder {

    // 逆先序
    // 先序遍历非递归版本进栈顺序是：根右左
    // 后序遍历非递归版本采用双栈，进栈顺序是：根左右
    public void postorder(Node head) {

        if (head != null) {
            // 输入栈
            Stack<Node> in = new Stack<>();
            // 输出栈
            Stack<Node> out = new Stack<>();

            // 将根节点放入输入栈
            in.push(head);

            while (!in.isEmpty()) {

                // 从输入栈取出栈顶元素放入输出栈中
                head = in.pop();
                out.push(head);

                // 先 左
                if (head.left != null) {
                    in.push(head.left);
                }

                // 后 右
                if (head.right != null) {
                    out.push(head.right);
                }
            }

            // 输出
            while (!out.isEmpty()) {
                out.pop();
            }
        }
    }

}
