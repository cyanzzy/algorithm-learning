package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 左叶子之和
 *
 * @author Cyan Chau
 * @create 2024-03-08
 */
public class SumOfLeafLeaves {

//    // DFS
//    public int sumOfLeftLeaves(TreeNode root) {
//        return root != null ? dfs(root) : 0;
//    }
//
//    public int dfs(TreeNode node) {
//        int ans = 0;
//        if (node.left != null) {
//            ans += isLeafNode(node.left) ? node.left.val : dfs(node.left);
//        }
//        if (node.right != null && !isLeafNode(node.right)) {
//            ans += dfs(node.right);
//        }
//        return ans;
//    }
//
    public boolean isLeafNode(TreeNode node) {
        return node.left == null && node.right == null;
    }

    // BFS
    public int sumOfLeftLeaves(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<TreeNode> queue = new LinkedList<>();
        queue.offer(root);
        int ans = 0;

        while (!queue.isEmpty()) {
            TreeNode node = queue.poll();
            if (node.left != null) {
                if (isLeafNode(node.left)) {
                    ans += node.left.val;
                } else {
                    queue.offer(node.left);
                }
            }
            if (node.right != null) {
                if (!isLeafNode(node.right)) {
                    queue.offer(node.right);
                }
            }
        }
        return ans;
    }

}
