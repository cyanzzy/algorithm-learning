package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * 两个链表的第一个重合节点
 *
 * @author Cyan Chau
 * @create 2023-12-07
 */
public class Code1GetIntersectionNode {

    // 哈希集合
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {

        // 将链表 A 装入集合
        Set<ListNode> set = new HashSet<>();
        ListNode temp = headA;
        while (temp != null) {
            set.add(temp);
            temp = temp.next;
        }

        // 校验操作
        temp = headB;
        while (temp != null) {
            if (set.contains(temp)) {
                return temp;
            }
           temp = temp.next;
        }
        return null;
    }


    // 双指针
    public ListNode getIntersectionNode2(ListNode headA, ListNode headB) {
        // base case
        if (headA == null || headB == null) {
            return null;
        }
        // 哨兵节点
        ListNode pA = headA, pB = headB;
        // 判断逻辑
        while (pA != pB) {

            // 如果指针 pA 不为空，则指向下一个位置；如果为空，则指向链表 B 的头节点
            pA = pA == null ? headB : pA.next;
            // 如果指针 pB 不为空，则指向下一个位置；如果为空，则指向链表 A 的头节点
            pB = pB == null ? headA : pB.next;
        }
        return pA;
    }

    // 栈结构
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {

        Stack<ListNode> stackA = new Stack<>();
        Stack<ListNode> stackB = new Stack<>();

        // 将链表 A 装入栈 A
        while (headA != null) {
            stackA.push(headA);
            headA = headA.next;
        }

        // 将链表 B 装入栈 B
        while (headB != null) {
            stackB.push(headB);
            headB = headB.next;
        }

        ListNode preNode = null;

        while (stackA.size() > 0 && stackB.size() > 0) {
            if (stackA.peek() == stackB.peek()) {
                preNode = stackA.pop();
                stackB.pop();
            } else {
                break; // 如果不一致，说明不可能存在交点
            }
        }
        return preNode;
    }
}
