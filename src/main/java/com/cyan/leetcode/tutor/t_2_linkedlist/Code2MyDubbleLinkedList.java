package com.cyan.leetcode.tutor.t_2_linkedlist;

/**
 *  设计【双】链表
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/design-linked-list/description/
 * @author Cyan Chau
 * @create 2023-02-24
 * update 2024-04-08
 */
public class Code2MyDubbleLinkedList {

    public class MyLinkedList {

        int size;

        // dummyNode
        DListNode head, tail;

        public MyLinkedList() {
            size = 0;
            head = new DListNode(0);
            tail = new DListNode(0);
            head.next = tail;
            tail.pre = head;
        }

        // Get the value of the index-th node in the linked list. If the index is invalid, return -1.
        public int get(int index) {

            // if index is invalid
            if (index < 0 || index >= size) return -1;

            // choose the fastest way: to move from the head
            // or to move from the tail
            DListNode cur = head;
            if (index + 1 < size - index)
                for(int i = 0; i < index + 1; ++i) cur = cur.next;
            else {
                cur = tail;
                for(int i = 0; i < size - index; ++i) cur = cur.pre;
            }

            return cur.val;
        }

        // Add a node of value val before the first element of the linked list.
        // After the insertion, the new node will be the first node of the linked list.
        public void addAtHead(int val) {
            DListNode pre = head, succ = head.next;

            ++size;
            DListNode toAdd = new DListNode(val);
            toAdd.pre = pre;
            toAdd.next = succ;
            pre.next = toAdd;
            succ.pre = toAdd;
        }

        // Append a node of value val to the last element of the linked list.
        public void addAtTail(int val) {
            DListNode succ = tail, pre = tail.pre;

            ++size;
            DListNode toAdd = new DListNode(val);
            toAdd.pre = pre;
            toAdd.next = succ;
            pre.next = toAdd;
            succ.pre = toAdd;
        }

        // Add a node of value val before the index-th node in the linked list.
        // If index equals to the length of linked list, the node will be appended to the end of linked list.
        // If index is greater than the length, the node will not be inserted.
        public void addAtIndex(int index, int val) {

            // If index is greater than the length,
            // the node will not be inserted.
            if (index > size) return;

            // [so weird] If index is negative,
            // the node will be inserted at the head of the list.
            if (index < 0) index = 0;

            // find predecessor and successor of the node to be added
            DListNode pre, succ;
            if (index < size - index) {
                pre = head;
                for(int i = 0; i < index; ++i) pre = pre.next;
                succ = pre.next;
            }
            else {
                succ = tail;
                for (int i = 0; i < size - index; ++i) succ = succ.pre;
                pre = succ.pre;
            }

            // insertion itself
            ++size;
            DListNode toAdd = new DListNode(val);
            toAdd.pre = pre;
            toAdd.next = succ;
            pre.next = toAdd;
            succ.pre = toAdd;
        }

        // Delete the index-th node in the linked list,
        // if the index is valid.
        public void deleteAtIndex(int index) {

            // if the index is invalid, do nothing
            if (index < 0 || index >= size) return;

            // find predecessor and successor of the node to be deleted
            DListNode pre, succ;
            if (index < size - index) {
                pre = head;
                for(int i = 0; i < index; ++i) pre = pre.next;
                succ = pre.next.next;
            }
            else {
                succ = tail;
                for (int i = 0; i < size - index - 1; ++i) succ = succ.pre;
                pre = succ.pre.pre;
            }

            // delete pred.next
            --size;
            pre.next = succ;
            succ.pre = pre;
        }

    }
    public class DListNode {
        int val;
        DListNode next;
        DListNode pre;

        public DListNode(int val) {
            this.val = val;
        }
    }

}
