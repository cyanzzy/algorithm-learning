package com.cyan.leetcode.tutor.t_2_linkedlist;

import com.cyan.algorithm_pass.pass11.level2.HammingWeight;

import java.util.ArrayList;
import java.util.List;

/**
 * ListNode
 *
 * @author Cyan Chau
 * @create 2023-02-23
 */
public class ListNode {

    public int val;
    public ListNode next;

    public ListNode() {
    }

    public ListNode(int val) {
        this.val = val;
    }

    public ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }

    public static void printList(ListNode head) {
        List<Integer> list = new ArrayList<>();
        while (head != null) {
            list.add(head.val);
            head = head.next;
        }
        System.out.println(list);
    }
}
