package com.cyan.algorithm_pass.pass3.level3;

/**
 * 颜色分类
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code3SortColors {

    // 单指针
    public void sortColors1(int[] nums) {

        // 使用一个指针 `ptr` 表示「头部」的范围
        // `ptr` 值表示从位置 `0` 到位置 `ptr−1` 都属于「头部」
        int ptr = 0;

        // 在第一次遍历中，从左向右遍历整个数组，
        // 如果找到 `0`，那么就需要将 `0` 与「头部」位置的元素进行交换，
        // 并将「头部」向后扩充一个位置。
        // 遍历结束后，所有的 `0` 都被交换到「头部」的范围，并且「头部」只包含 `0`。
        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 0) {
                int temp = nums[i];
                nums[i] = nums[ptr];
                nums[ptr] = temp;
                ++ptr;
            }
        }

        // 在第二次遍历中，从「头部」开始，从左向右遍历整个数组
        // 如果找到 `1`，那么就需要将 `1` 与「头部」位置的元素进行交换，
        // 并将「头部」向后扩充一个位置。
        // 在遍历结束之后，所有的 `1` 都被交换到「头部」的范围，并且都在 `0` 之后
        // 此时 `2` 只出现在「头部」之外的位置，因此排序完成。
        for (int i = ptr; i < nums.length; ++i) {
            if (nums[i] == 1) {
                int temp = nums[i];
                nums[i] = nums[ptr];
                nums[ptr] = temp;
                ++ptr;
            }
        }

    }

    // 双指针
    public void sortColors(int[] nums) {

        int index = 0;
        int left = 0, right = nums.length - 1;

        while (index <= right) {
            if (nums[index] == 0) {
                swap(nums, index++, left++);
            } else if (nums[index] == 2) {
                swap(nums, index, right--);
            } else
                index++;
        }

    }

    public void swap(int[] nums, int left, int right) {

        int temp = nums[left];
        nums[left] = nums[right];
        nums[right] = temp;
    }
}
