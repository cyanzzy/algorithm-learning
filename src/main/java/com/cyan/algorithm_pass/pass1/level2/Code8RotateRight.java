package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 旋转链表
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code8RotateRight {

    // 快慢指针
    public ListNode rotateRight1(ListNode head, int k) {

        // base case
        if (head == null || k == 0) {
            return head;
        }

        int len = 0;
        // H 记录 head 位置
        ListNode H = head;
        // 快指针
        ListNode fast = head;
        // 慢指针
        ListNode slow = head;

        while (head != null) {
            head = head.next;
            len++;
        } // 此情况下的 head 指向 null
        if (k % len == 0) {
            return H;
        }

        // 此时 fast 从头开始走 K 步
        // 要注意 K 超出 len 的部分，使用模运算抵消
        // len = 5 时，k = 2 和 7 一样效果
        while ((k % len) > 0) {
            k--;
            fast = fast.next;
        }

        // 在快指针走了 k 步后，快慢指针一起行动
        // 当 fast 走到最后一个节点（fast.next == null）时
        // slow 处于倒数 k 个节点的前驱
        while (fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }
        // 记录新链表头节点的位置
        ListNode L = slow.next;
        // 拆分链表
        slow.next = null;
        fast.next = H;

        return L;
    }

    // 环形链表
    public ListNode rotateRight(ListNode head, int k) {

        // base case
        if (k == 0 || head == null || head.next == null) {
            return head;
        }
        // 从 0 开始计数
        int len = 1;
        ListNode p = head;
        while (p.next != null) {
            p = p.next;
            len++;
        }
        // pos = (n - 1) - (k % n)
        // 新链表的最后一个节点为原链表的第 pos 个节点（从 0 开始计数）
        int count = len - k % len;
        if (count == len) {
            return head;
        }
        // 构造循环链表
        p.next = head;

        // 寻找新链表的尾节点位置
        while (count-- > 0) {
            p = p.next;
        }

        // 记录新链表的头节点位置
        ListNode L = p.next;
        // 将环断开
        p.next = null;

        return L;
    }
}
