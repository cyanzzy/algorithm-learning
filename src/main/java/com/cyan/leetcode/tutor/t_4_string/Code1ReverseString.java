package com.cyan.leetcode.tutor.t_4_string;

/**
 * 反转字符串
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/reverse-string/description/
 * @author Cyan Chau
 * @create 2023-04-02
 * update 2024-04-12
 */
public class Code1ReverseString {

    public void reverseString(char[] s) {
        int left = 0, right = s.length - 1;

        for (; left < right; left++, right--) {
            char temp = s[left];
            s[left] = s[right];
            s[right] = temp;
        }
    }
}
