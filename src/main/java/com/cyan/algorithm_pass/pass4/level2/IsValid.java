package com.cyan.algorithm_pass.pass4.level2;

import javax.print.DocFlavor;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 *  括号匹配
 * @author Cyan Chau
 * @create 2024-02-22
 */
public class IsValid {

    public boolean isValid(String s) {
        // 不是偶数，直接 pass
        if (s.length() % 2 == 1) {
            return false;
        }
        Map<Character, Character> pairs = new HashMap<>();
        pairs.put(')', '(');
        pairs.put('}', '{');
        pairs.put(']', '[');

        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); i++) {
            char ch = s.charAt(i);
            if (pairs.containsKey(ch)) {
                if (stack.isEmpty() || stack.peek() != pairs.get(ch)) {
                    return false;
                }
                stack.pop();
            } else {
                stack.push(ch);
            }
        }
        return stack.isEmpty();
    }

}
