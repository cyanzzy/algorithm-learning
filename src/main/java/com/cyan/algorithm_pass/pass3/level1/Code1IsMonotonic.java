package com.cyan.algorithm_pass.pass3.level1;

/**
 * 单调数列
 *
 * @author Cyan Chau
 * @create 2024-01-18
 */
public class Code1IsMonotonic {

    // 两次遍历
    public boolean isMonotonic1(int[] nums) {
        return isSorted(nums, true) || isSorted(nums, false);
    }

    private boolean isSorted(int[] nums, boolean increasing) {

        if (increasing) { // 判断单调增
            for (int i = 0; i < nums.length - 1; i++) {
                if (nums[i] > nums[i + 1]) {
                    return false;
                }
            }
        } else { // 判断单调减
            for (int i = 0; i < nums.length - 1; i++) {
                if (nums[i] < nums[i + 1]) {
                    return false;
                }
            }
        }
        // 不满足上述失败情况，则符合要求
        return true;
    }

    // 一次遍历
    public boolean isMonotonic(int[] nums) {

        boolean inc = true, dec = true;

        for (int i = 0; i < nums.length - 1; i++) {

            if (nums[i] > nums[i + 1]) {
                inc = false;
            }
            if (nums[i] < nums[i +1]) {
                dec = false;
            }
        }
        return inc || dec;
    }

}
