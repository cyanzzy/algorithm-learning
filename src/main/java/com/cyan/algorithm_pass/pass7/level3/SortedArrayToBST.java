package com.cyan.algorithm_pass.pass7.level3;

import com.cyan.algorithm_pass.TreeNode;

import java.util.Random;

/**
 * 将有序数组转换为二叉搜索树
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class SortedArrayToBST {

//    public TreeNode sortedArrayToBST(int[] nums) {
//
//        return helper(nums, 0, nums.length - 1);
//    }

//    // 中序遍历，总是选择中间位置左边的数字作为根节点
//    public TreeNode helper(int[] nums, int left, int right) {
//        if (left > right) {
//            return null;
//        }
//
//        // 总是选择中间位置左边的数字作为根节点
//        int mid = (left + right) / 2;
//
//        TreeNode root = new TreeNode(nums[mid]);
//        root.left = helper(nums, left, mid - 1);
//        root.right = helper(nums, mid + 1, right);
//        return root;
//    }

//    // 中序遍历，总是选择中间位置右边的数字作为根节点
//    public TreeNode helper(int[] nums, int left, int right) {
//        if (left > right) {
//            return null;
//        }
//
//        // 总是选择中间位置右边的数字作为根节点
//        int mid = (left + right + 1) / 2;
//
//        TreeNode root = new TreeNode(nums[mid]);
//        root.left = helper(nums, left, mid - 1);
//        root.right = helper(nums, mid + 1, right);
//        return root;
//    }


    // 中序遍历，选择任意一个中间位置数字作为根节点
    Random rand = new Random();

    public TreeNode sortedArrayToBST(int[] nums) {
        return helper(nums, 0, nums.length - 1);
    }

    public TreeNode helper(int[] nums, int left, int right) {
        if (left > right) {
            return null;
        }

        // 选择任意一个中间位置数字作为根节点
        int mid = (left + right + rand.nextInt(2)) / 2;

        TreeNode root = new TreeNode(nums[mid]);
        root.left = helper(nums, left, mid - 1);
        root.right = helper(nums, mid + 1, right);
        return root;
    }

}
