package com.cyan.leetcode.tutor.t_2_linkedlist;

/**
 * 两两交换链表中的节点
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/swap-nodes-in-pairs/description/
 * @author Cyan Chau
 * @create 2023-02-28
 * @update 2024-04-08
 */
public class Code4SwapPairs {

    // 迭代法
    public ListNode swapPairs1(ListNode head) {

        // 构造虚拟节点
        ListNode dummy = new ListNode(0);
        dummy.next = head;

        // 计算链表长度
        int len = 0;
        // 定义哨兵节点p
        ListNode p = head;
        // 定义每组翻转的最后一个节点
        ListNode tail = null;
        // 定义哨兵节点q
        ListNode q = null;
        // 定义每次翻转时的临时头节点
        ListNode s = null;

        // 计算链表长度
        if (p != null) {
            len++;
            p  = p.next;
        }

        // p重新指向第一个元素节点
        p = dummy.next;
        // s、q指向虚拟节点
        s = dummy;
        q = dummy;

        // 外部循环用于控制翻转次数
        for (int i = 0; i < len / 2; i++) {

            // tail用于记录每组翻转后的最后一个节点
            tail = p;
            // 断链操作
            s.next = null;
            // 内部循环用于控制每组的局部翻转 (此时每组只有两个节点)
            for (int j = 0; j < 2; j++) {
                // 即将操作该节点
                q = p;
                // p此时充当哨兵的作用
                p = p.next;

                // 经典头插法
                q.next = s.next;
                s.next = q;
            }

            // 修补断链
            tail.next = p;
            // s指向的位置，供下一次翻转
            s = tail;
        }
        return dummy.next;
    }

    // 递归法
    public ListNode swapPairs(ListNode head) {
        if (head == null || head.next == null) {
            return head;
        }
        ListNode oneNode = head;
        ListNode twoNode = oneNode.next;
        ListNode threeNode = twoNode.next;

        twoNode.next = oneNode;
        oneNode.next = swapPairs(threeNode);

        return twoNode;
    }
}
