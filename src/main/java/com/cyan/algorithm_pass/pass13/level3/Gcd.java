package com.cyan.algorithm_pass.pass13.level3;

/**
 * 最大公约数
 *
 * @author Cyan Chau
 * @create 2024-04-28
 */
public class Gcd {

    public int gcd(int a, int b) {
        int k = 0;

        do {
            // 取模
            k = a % b;
            // 将被除数赋给除数
            a = b;
            // 将余数赋给除数
            b = k;
        } while (k != 0);

        // 返回被除数
        return a;
    }

}
