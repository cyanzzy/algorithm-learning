package com.cyan.algorithm_pass.pass7.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 前序遍历
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class PreorderTraversal {

    public List<Integer> preorderTraversal(TreeNode root) {

        List<Integer> ans = new ArrayList<>();
        preorder(root, ans);
        return ans;
    }

    public void preorder(TreeNode root, List<Integer> res) {

        if (root == null) {
            return;
        }
        res.add(root.val);
        preorder(root.left, res);
        preorder(root.right, res);
    }
}
