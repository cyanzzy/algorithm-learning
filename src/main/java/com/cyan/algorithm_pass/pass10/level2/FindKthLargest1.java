package com.cyan.algorithm_pass.pass10.level2;

/**
 * 数组中第 k 大元素
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class FindKthLargest1 {

    // 基于快速排序的选择方法
    public int findKthLargest(int[] _nums, int k) {
        // 第 K 大 --> 第 N - K 小
        return search(_nums, 0, _nums.length - 1, _nums.length - k);
    }

    public int search(int[] nums, int begin, int end, int k) {
        int pivotIndex = partition(nums, begin, end);

        if (pivotIndex == k) {
            return nums[pivotIndex];
        } else if (pivotIndex > k) {
            return search(nums, begin, pivotIndex - 1, k);
        } else {
            return search(nums, pivotIndex + 1, end, k);
        }
    }

    /**
     * @see com.cyan.algorithm_pass.pass10.level1.QuickSort
     * @param nums
     * @param startIndex
     * @param endIndex
     * @return
     */
    public int partition(int[] nums, int startIndex, int endIndex) {
        int pivot = nums[startIndex];
        int leftPoint = startIndex;
        int rightPoint = endIndex;

        while (leftPoint < rightPoint) {
            while (leftPoint < rightPoint
                    && nums[rightPoint] > pivot) {
                rightPoint--;
            }
            while (leftPoint < rightPoint
                    && nums[leftPoint] <= pivot) {
                leftPoint++;
            }
            if (leftPoint < rightPoint) {
                int temp = nums[leftPoint];
                nums[leftPoint] = nums[rightPoint];
                nums[rightPoint] = temp;
            }
        }
        // 最终将分界值与当前指针数据交换
        nums[startIndex] = nums[rightPoint];
        nums[rightPoint] = pivot;
        // 返回分界值所在下标
        return rightPoint;
    }

}
