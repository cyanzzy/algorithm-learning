package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 回文链表
 *
 * @author Cyan Chau
 * @create 2023-12-07
 */
public class Code2IsPalindrome {

    // 快慢指针 + 反转链表
    public boolean isPalindrome(ListNode head) {

        // base case
        if (head == null) {
            return true;
        }

        // 找到链表中点（前半部分链表的尾节点）
        ListNode firstHalfEnd = endOfFirstHalf(head);

        // 将后半部分链表进行反转
        ListNode secondHalfStart = reverseList(firstHalfEnd.next);

        // 回文判断
        ListNode p1 = head; // p1 指向原始链表表头
        ListNode p2 = secondHalfStart; // p2 指向反转后的链表表头
        boolean ans = true;

        while (ans && p2 != null) {
            if (p1.val != p2.val) {
                ans = false;
            }
            p1 = p1.next;
            p2 = p2.next;
        }

        // 还原链表
        firstHalfEnd.next = reverseList(secondHalfStart);

        return ans;
    }

    // 反转链表（默认不带虚拟节点）
    private ListNode reverseListNondummyNode(ListNode head) { // 头插法
        ListNode L = null;
        ListNode p = head;

        while (p != null) {

            // 保存下一个节点位置
            ListNode q = p.next;
            p.next = L;
            L = p;
            p = q;

        }
        return L;
    }

    // AC
    // 反转链表（扩展：如果带虚拟节点可以这样写：）
    private ListNode reverseList(ListNode head) { // 头插法

        ListNode L = new ListNode(0);
        // 将虚拟节点挂到表头
        L.next = head;
//        // head 指向虚拟节点 L
//        head = L;
        // 定义哨兵指针
        ListNode p = head; // p 指向虚拟节点后的第一个元素
        // q 永远指向 p 的后继，方便下次反转
        ListNode q = null;

        // 断链操作
        L.next = null;
        while (p != null) {
            q = p.next;
            p.next = L.next;
            L.next = p;
            p = q;
        }

        return L.next;
    }

    // 快慢指针遍历中点
    private ListNode endOfFirstHalf(ListNode head) {

        // 快指针
        ListNode fast = head;
        // 慢指针
        ListNode slow = head;

        while (fast.next != null && fast.next.next != null) {
            fast = fast.next.next;
            slow = slow.next;
        }

        return slow;
    }




}
