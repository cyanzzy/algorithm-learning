package com.cyan.algorithm_pass.pass14.level2;

/**
 * 数组中第 K 大的元素
 *
 * @author Cyan Chau
 * @create 2024-03-25
 */
public class FindKthLargest {

    public int findKthLargest(int[] nums, int k) {
        int heapSize = nums.length;
        // 建立初始大根堆
        buildMaxHeap(nums, heapSize);
        // 进行 K-1 次删除操作
        for (int i = nums.length - 1; i >= nums.length - k + 1; --i) {
            swap(nums, 0, i);
            --heapSize;
            // 堆化
            heapify(nums, 0, heapSize);
        }
        return nums[0];
    }

    public void swap(int[] a, int i, int j) {
        int temp = a[i];
        a[i] = a[j];
        a[j] = temp;
    }

    // 构造大根堆
    public void buildMaxHeap(int[] arr, int heapSize) {

        // 从最后一个非叶子节点开始调整堆，编号从 0 开始
        // heapSize / 2 是最后一个非叶子节点
        for (int i = heapSize / 2; i >= 0; --i) {
            heapify(arr, i, heapSize);
        }
    }

    // 堆调整
    public void heapify(int[] arr, int index, int size) {
        // left 左孩子
        int left = index * 2 + 1;
        while (left < size) {
            // largest 表示index节点两个孩子较大的节点
            int largest = left + 1 < size && arr[left + 1] > arr[left] ? left + 1 : left;
            // 当前节点和largest比较
            largest = arr[largest] > arr[index] ? largest : index;
            // 如果当前节点大于较大孩子节点，则不进行操作
            if (largest == index) {
                break;
            }
            // 如果当前节点小于较大孩子节点，则与孩子节点交换
            int temp = arr[index];
            arr[index] = arr[largest];
            arr[largest] = temp;

            // 向下继续调整
            index = largest;
            left = index * 2 + 1;
        }
    }

}
