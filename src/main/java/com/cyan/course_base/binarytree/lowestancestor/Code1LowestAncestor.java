package com.cyan.course_base.binarytree.lowestancestor;

import com.cyan.course_base.binarytree.Node;

/**
 * 最近公共祖先：原始解法
 *
 * @author Cyan Chau
 * @create 2023-03-23
 */
public class Code1LowestAncestor {

    public Node lowestAncestor(Node head, Node o1, Node o2) {
        // 如果发现cur等于null，或者o1、o2，则返回cur
        if (head == null || head == o1 || head == o2) {
            return head;
        }
        // 检查左子树
        Node left = lowestAncestor(head.left, o1, o2);
        // 检查右子树
        Node right = lowestAncestor(head.right, o1, o2);

        // 如果left和right都不为空，说明左子树发现过o1或o2，右子树也发现过o1或o2，
        // 说明o1向上与o2向上的过程中，首次在cur相遇，返回cur
        if (left != null && right != null) {
            return head;
        }

        // 左右两棵树并不都有返回值
        // 如果left和right有一个为空，另一个不为空，假设不为空的那个记为node，此时node要么是o1或o2的一个，要么是o1和o2的最近公共祖先，直接返回node
        // 如果left和right都为空，说明cur整棵子树没有发现过o1或o2，返回null
        return left != null ? left : right;
    }


}
