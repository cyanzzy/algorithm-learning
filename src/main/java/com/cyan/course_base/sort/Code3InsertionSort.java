package com.cyan.course_base.sort;

import java.util.Arrays;

/**
 * InsertionSort
 *  复盘：
 *  第一个作为有序序列，后续部分作为无序序列，
 *  以无序序列开始，从有序序列的末尾往前比较寻找合适的插入位置
 *
 * @author Cyan Chau
 * @create 2023-02-13
 */
public class Code3InsertionSort {

    /**
     * 算法思想：
     *
     * 每趟排序中将待排序的关键字按照其值大小插入到已经排好序的部分有序序列的适当位置，
     * 直到所有待排关键字都被插入到有序序列中为止。
     *
     * @param arr
     */
    public static void insertionSort(int[] arr) {
        // 非法情况
        if (arr == null || arr.length < 2) {
            return;
        }
        // 控制无序序列走向
        for (int i = 1; i < arr.length; ++i) {
            // 在排序完的部分有序序列中找出合适位置
            for (int j = i - 1; j >= 0 && arr[j] > arr[j + 1]; --j) { // 控制有序序列
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
            }
        }
    }

    // for test
    public static void comparator(int[] arr) {
        Arrays.sort(arr);
    }

    // for test
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }

    // for test
    public static int[] copyArray(int[] arr) {
        if (arr == null) {
            return null;
        }
        int[] res = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            res[i] = arr[i];
        }
        return res;
    }

    // for test
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    // for test
    public static void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            insertionSort(arr1);
            comparator(arr2);
            if (!isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");

        int[] arr = generateRandomArray(maxSize, maxValue);
        printArray(arr);
        insertionSort(arr);
        printArray(arr);
    }

}
