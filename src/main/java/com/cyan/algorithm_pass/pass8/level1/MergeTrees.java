package com.cyan.algorithm_pass.pass8.level1;

import com.cyan.algorithm_pass.TreeNode;

/**
 * 合并二叉树
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class MergeTrees {

    public TreeNode mergeTrees(TreeNode root1, TreeNode root2) {

        return dfs(root1, root2);
    }

    public TreeNode dfs(TreeNode p, TreeNode q) {

        if (p == null) {
            return q;
        }
        if (q == null) {
            return p;
        }
        TreeNode mergeNode = new TreeNode(p.val + q.val);
        mergeNode.left = dfs(p.left, q.left);
        mergeNode.right = dfs(p.right, q.right);
        return mergeNode;
    }
}
