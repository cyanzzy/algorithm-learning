package com.cyan.algorithm_pass.pass2.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.List;

/**
 * K组链表反转
 *
 * @author Cyan Chau
 * @create 2024-01-17
 */
public class Code5ReverseKGroup {

    public ListNode reverseKGroup1(ListNode head, int k) {

        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;

        // 计算长度
        int length = 0;
        ListNode cur = head;
        while (cur != null) {
            length++;
            cur = cur.next;
        }

        // tail 始终指向当前组反转后的最后一个节点
        // 比如 [1,2,3,4] --> [2,1,4,3]，tail 便记录节点元素 1
        ListNode tail = dummyHead;
        ListNode next = dummyHead;

        // 外循环控制反转次数
        for (int i = 0; i < length / k; i++) {
            // 初始化 cur 的位置
            cur = tail.next;
            // 临时保存当前组反转后的最后一个节点
            ListNode temp = cur;
            // tail 作为上一组反转链表的最后一个节点开始断链
            tail.next = null;

            // 内循环控制每组反转
            for (int j = 0; j < k; j++) { // 头插法
                next = cur.next;
                cur.next = tail.next;
                tail.next = cur;
                cur = next;
            }
            // 更新 tail
            tail = temp;
            // 恢复链表
            tail.next = cur;
        }

        return dummyHead.next;
    }


    public ListNode reverseKGroup(ListNode head, int k) {

        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;

        ListNode pre = dummyNode;
        ListNode end = dummyNode;

        while (end.next != null) {
            // 寻找待处理区间的尾部
            for (int i = 0; i < k && end != null; i++) {
                end = end.next;
            }
            if (end == null) {
                break;
            }
            // 截取区间片段
            ListNode begin = pre.next;
            ListNode next = end.next;
            end.next = null;
            // 反转操作
            pre.next = reverse(begin);
            // 连接上区间反转的链表
            begin.next = next;
            // 调整 pre 的指向
            pre = begin;
            end = pre;
        }
        return dummyNode.next;
    }

    public ListNode reverse(ListNode head) {

        ListNode L = new ListNode(0);
        L.next = head;
        ListNode curr = head;

        L.next = null;
        while(curr != null) {
            ListNode next = curr.next;
            curr.next = L.next;
            L.next = curr;
            curr = next;
        }
        return L.next;
    }
}
