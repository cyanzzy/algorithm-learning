package com.cyan.leetcode.tutor.t_2_linkedlist;

/**
 * 环形链表 Ⅱ
 *
 * @accepted Failed
 * @link https://leetcode.cn/problems/linked-list-cycle-ii/
 * @author Cyan Chau
 * @create 2023-03-01
 * @update 2024-04-09
 */
public class Code7DetectCycle {


    // 算法思想：
    // 使用快慢指针，快指针每次走两步，慢指针每次走一步
    // 如果不存在环，快指针一定能走完
    // 如果存在环，快指针一定能追上慢指针
    // 当快指针追上慢指针时，将快指针放在 head，此时快慢指针同步移动，最终在入环口相遇
    public ListNode detectCycle(ListNode head) {

        // 违规条件
        if (head == null) {
            return null;
        }

        // 定义快慢指针
        ListNode slow = head, fast = head;

        while (fast != null) {
            slow = slow.next;
            if (fast.next != null) {
                fast = fast.next.next;
            } else {
                return null;
            }
            // 如果有环，快指针一定会追上慢指针
            if (fast == slow) {
                fast = head;
                // 同步移动，直到相遇，则为入环口
                while (fast != slow) {
                    fast = fast.next;
                    slow = slow.next;
                }
                return fast;
            }
        }
        return null;
    }

}
