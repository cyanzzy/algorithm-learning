package com.cyan.algorithm_pass.pass9.level2;

/**
 * 寻找旋转排序数组中的最小值
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class FindMin {

    public int findMin(int[] nums) {
        int low = 0;
        int high = nums.length - 1;
        while (low < high) {
            int pivot = low + (high - low) / 2;
            if (nums[pivot] < nums[high]) {
                high = pivot;
            } else {
                low = pivot + 1;
            }
        }
        return nums[low];
    }
}
