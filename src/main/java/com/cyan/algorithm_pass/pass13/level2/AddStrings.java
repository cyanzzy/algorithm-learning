package com.cyan.algorithm_pass.pass13.level2;

import org.omg.PortableInterceptor.INACTIVE;

/**
 * 字符串相加
 *
 * @author Cyan Chau
 * @create 2024-03-24
 */
public class AddStrings {

    // "456" + “77”
    public String addStrings(String num1, String num2) {
        int i = num1.length() - 1, j = num2.length() - 1;
        int adder = 0;
        StringBuilder sb = new StringBuilder();
        while (i >= 0 || j >= 0 || adder != 0) {
            int x = i >= 0 ? num1.charAt(i) - '0' : 0;
            int y = j >= 0 ? num1.charAt(j) - '0' : 0;

            int sum = x + y + adder;
            sb.append(sum % 10);
            // 进位
            adder = sum / 10;
            i--;
            j--;
        }
        return sb.reverse().toString();
    }
}
