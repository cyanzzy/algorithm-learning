package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 返回倒数第 K 个元素
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code7KthToLast {

    // 快慢指针
    public int kthToLast(ListNode head, int k) {

        ListNode fast = head;
        ListNode slow = head;

        while (k > 0) { // 快指针先走 k 步
            fast = fast.next;
            k--;
        }
        if (fast == null) { // 说明链表长度不足 k
            return head.val;
        }
        // 快慢指针
        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }
        return slow.val;
    }
}
