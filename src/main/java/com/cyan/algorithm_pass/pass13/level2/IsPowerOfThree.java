package com.cyan.algorithm_pass.pass13.level2;

/**
 * 3 的幂
 *
 * @author Cyan Chau
 * @create 2024-04-28
 */
public class IsPowerOfThree {

    public boolean isPowerOfThree(int n) {

        if (n <= 0) {
            return false;
        }
        while (n % 3 == 0) {
            n /= 3;
        }
        return n == 1;
    }
}
