package com.cyan.course_base.brute_force_recursion;

/**
 * 纸牌博弈问题
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code6CardGame {

    /**
     * 先手拿牌的玩家返回的分数，先手必然拿的最好的牌
     *
     * @param arr 牌组
     * @param i 左边界
     * @param j 右边界
     * @return 返回分数
     */
    public int first(int[] arr, int i, int j) {

        if (i == j) { // 递归终止条件
            // 当牌只有一张，先手必然拿到
            return arr[i];
        }

        /**
         * 1.先手拿走了arr[i]，剩下的必然后手
         * 2.先手拿走了arr[j]，剩下的必然后手
         * 3.返回较大的分数
         */
        return Math.max(
                // 先手拿走了arr[i]，剩下的必然后手
                arr[i] + second(arr, i + 1, j),
                // 先手拿走了arr[j]，剩下的必然后手
                arr[j] + second(arr, i, j - 1)
        );
    }

    /**
     * 后手拿牌的玩家返回的分数，后手必然拿的差的牌
     *
     * @param arr 牌
     * @param i 左边界
     * @param j 右边界
     * @return 返回分数
     */
    public int second(int[] arr, int i, int j) {
        if (i == j) { // 递归终止条件
            // 只剩一张牌时，作为后手必然拿不到
            return 0;
        }
        /**
         * 1.对手先拿牌，对手拿走了arr[i]，后手从arr[i+1..j]拿
         * 2.对手先拿牌，对手拿走了arr[j]，后手从arr[i..j-1]拿
         * 3.返回最差的分数
         */
        return Math.min(
                // 对手先拿牌，对手拿走了arr[i]，后手从arr[i+1..j]拿的分数
                first(arr, i + 1, j),
                // 对手先拿牌，对手拿走了arr[j]，后手从arr[i..j-1]拿的分数
                first(arr, i, j - 1)
        );
    }

    public int win(int[] arr) {
        if(arr == null || arr.length == 0) {
            return 0;
        }
        return Math.max(first(arr, 0, arr.length - 1), second(arr, 0, arr.length - 1));
    }


}
