package com.cyan.algorithm_pass.pass18.level2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 组合总和
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class CombinationSum {

    List<List<Integer>> ans;
    List<Integer> path;

    public List<List<Integer>> combinationSum(int[] candidates, int target) {
        ans = new ArrayList<>();
        path = new ArrayList<>();
        // 对集合排序，便于剪枝
        Arrays.sort(candidates);
        dfs(candidates, target, 0, 0);
        return ans;
    }

    public void dfs(int[] candidates, int target, int startIndex, int curSum) {
        // 递归终止条件
        if (curSum == target) {
            ans.add(new ArrayList<>(path));
            return;
        }

        // 单层遍历逻辑
        for (int i = startIndex; i < candidates.length && curSum + candidates[i] <= target; ++i) { // for控制横向遍历
            // 将当前节点的值放入path，同时更新单个路径上的sum
            curSum += candidates[i];
            path.add(candidates[i]);
            // 遍历当前分支的下一层
            // 由于每条路径上的答案可以重复，因此不需要i+1
            dfs(candidates, target, i, curSum);
            // 返回递归树的上一层
            curSum -= candidates[i];
            path.remove(path.size() - 1);
        }
    }


}
