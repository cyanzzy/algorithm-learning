package com.cyan.algorithm_pass.pass4.level1;

/**
 * 链栈
 *
 * @author Cyan Chau
 * @create 2024-02-22
 */
public class LStack<T> {

    // 链表结构
    class Node<T> {
        public T data;
        public Node next;
    }

    // 头指针
    public Node<T> head;

    public LStack() {
        this.head = null;
    }

    // 入栈
    public void push(T t) {
        if (t == null) {
            throw new NullPointerException("参数不能为空");
        }
        if (head == null) {
            head = new Node<>();
            head.data = t;
            head.next = null;
        } else {
            Node<T> temp = head;
            head = new Node<>();
            head.data = t;
            head.next = temp;
        }
    }

    // 出栈
    public T pop() {
        if (head == null) {
            return null;
        }
        T t = head.data;
        head = head.next;
        return t;
    }

    // 取栈顶元素
    public T peek() {
        if (head == null) {
            return null;
        }

        return head.data;
    }

    // 判空
    public boolean isEmpty() {
        return head == null;
    }

    public static void main(String[] args) {
        LStack<String> stack = new LStack<>();
        System.out.println(stack.isEmpty());
        stack.push("1");
        stack.push("2");
        stack.push("3");
        System.out.println(stack.peek());
        System.out.println(stack.pop());
        System.out.println(stack.peek());
        System.out.println(stack.isEmpty());
    }
}
