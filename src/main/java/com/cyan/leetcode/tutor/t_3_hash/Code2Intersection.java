package com.cyan.leetcode.tutor.t_3_hash;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * 两个数组的交集
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/intersection-of-two-arrays/description/
 * @author Cyan Chau
 * @create 2023-03-23
 * update 2024-04-11
 */
public class Code2Intersection {

    // 双指针 + 排序
    public int[] intersection1(int[] nums1, int[] nums2) {
        // 排序操作
        Arrays.sort(nums1);
        Arrays.sort(nums2);

        int len1 = nums1.length;
        int len2 = nums2.length;

        int[] ans = new int[len1  + len2];

        int cur = 0, i = 0, j = 0;

        while (i < len1 && j < len2) {
            if (nums1[i] == nums2[j]) {
                if (cur == 0 || nums1[i] != ans[cur - 1]) {
                    ans[cur++] = nums1[i];
                }
                i++;
                j++;
            } else  if (nums1[i] < nums2[j]) {
                i++;
            } else {
                j++;
            }
        }
        return Arrays.copyOfRange(ans, 0, cur);
    }

    // 哈希
    public int[] intersection2(int[] nums1, int[] nums2) {
        Set<Integer> set1 = new HashSet<>();
        Set<Integer> set2 = new HashSet<>();

        for (int num : nums1) {
            set1.add(num);
        }

        for (int num : nums2) {
            set2.add(num);
        }
        return getInterSection(set1, set2);
    }

    public int[] getInterSection(Set<Integer> smallSet, Set<Integer> bigSet) {
        // 保持调用形式
        if (smallSet.size() > bigSet.size()) {
            return getInterSection(bigSet, smallSet);
        }

        Set<Integer> ans = new HashSet<>();

        // 遍历小集合
        for (Integer num : smallSet) {
            if (bigSet.contains(num)) {
                ans.add(num);
            }
        }

        // Set-->int[]
        int[] intersection = new int[ans.size()];
        int index = 0;

        for (int el : ans) {
            intersection[index++] = el;
        }

        return intersection;
    }
}
