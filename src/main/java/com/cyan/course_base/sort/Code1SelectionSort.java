package com.cyan.course_base.sort;

/**
 * SelectionSort
 *
 *   复盘：
 *   序列划分成有序和无序部分，第一个元素作为有序序列，
 *   每轮迭代，在无序序列中遍历最小元素，然后与当前关键字进行交换，继续下一轮迭代
 *
 * @author Cyan Chau
 * @create 2023-02-13
 */
public class Code1SelectionSort {
    /**
     * 算法思想：
     *
     * 从序列起始顺序扫描，找出最小的关键字，与第一个关键字交换，
     * 然后从剩下的关键字中继续选择最小的关键字，并交换。如此反复，最终使得序列有序
     * @param arr
     */
    public void selectionSort(int[] arr) {
        // 非法情况
        if (arr == null || arr.length < 2) {
            return;
        }
        // 核心代码
        for (int i = 0; i < arr.length - 1; i++) {
            // minIndex 记录最小关键字的下标
            int minIndex = i;
            // 内循环表示无序序列部分
            for (int j = i + 1; j < arr.length; j++) {
                minIndex = arr[j] < arr[minIndex] ? j : minIndex;
            }
            // 当前关键字与找到的最小关键字交换
            int temp = arr[i];
            arr[i] = arr[minIndex];
            arr[minIndex] = temp;
        }
    }
}
