package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 层次遍历
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class LevelOrder {

    public List<List<Integer>> levelOrder(TreeNode root) {

        // 存储结果
        List<List<Integer>> ret = new ArrayList<>();
        // base case
        if (root == null) {
            return ret;
        }
        // 队列
        Queue<TreeNode> queue = new LinkedList<>();
        // 根元素入队
        queue.offer(root);

        // 当队列不为空时
        while (!queue.isEmpty()) {
            // 当前层次
            List<Integer> level = new ArrayList<>();
            // 当前队列长度（当前层元素个数）
            int currentLevelSize = queue.size();
            // 依次取 currentLevelSize 元素拓展
            for (int i = 0; i < currentLevelSize; i++) {
                // 出队该元素
                TreeNode node = queue.poll();
                // 将 node 元素放入 level1
                level.add(node.val);
                // 入队左孩子
                if (node.left != null) {
                    queue.offer(node.left);
                }
                // 入队右孩子
                if (node.right != null) {
                    queue.offer(node.left);
                }
            }
            // 将当前层次放入结果中，准备下一次迭代
            ret.add(level);
        }
        return ret;
    }
}
