package com.cyan.course_base.graph;

/**
 * @author Cyan Chau
 * @create 2023-03-27
 */
public class GraphGenerator {

    /**
     * 根据三元组构造图
     *
     * @param matrix 邻接矩阵
     * @return 邻接表
     */
    public Graph createGraph(Integer[][] matrix) {
        Graph graph = new Graph();

        for (int i = 0; i < matrix.length; i++) {
            // 设置权值
            Integer weight = matrix[i][0];
            // 设置起始节点的值
            Integer from = matrix[i][1];
            // 设置终止节点的值
            Integer to = matrix[i][2];

            // 如果起始节点第一次出现，将（起始节点的值，起始节点）放入哈希表
            if (!graph.nodes.containsKey(from)) {
                graph.nodes.put(from, new Node(from));
            }
            
            if (!graph.nodes.containsKey(to)) {
                graph.nodes.put(to, new Node(to));
            }

            // 初始化起始节点
            Node fromNode = graph.nodes.get(from);
            // 初始化终止节点
            Node toNode = graph.nodes.get(to);
            // 初始化边节点
            Edge newEdge = new Edge(weight, fromNode, toNode);

            // 起始节点的邻接点设置成toNode
            fromNode.nexts.add(toNode);
            // 起始节点的出度++
            fromNode.out++;
            // 终止节点的入度++
            toNode.in++;
            // 更新起始节点的边信息
            fromNode.edges.add(newEdge);
            // 更新图的边信息
            graph.edges.add(newEdge);

        }
        return graph;
    }
}
