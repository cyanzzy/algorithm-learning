package com.cyan.algorithm_pass.pass3.level2;

/**
 * 删除有序数组中的重复项
 *
 * @author Cyan Chau
 * @create 2024-02-03
 */
public class Code2RemoveDuplicates {

    public int removeDuplicates(int[] nums) {

        if (nums.length == 0) {
            return 0;
        }

        int fast = 1, slow = 1;
        while (fast < nums.length) {

            if (nums[fast] != nums[fast - 1]) {
                nums[slow] = nums[fast];
                ++slow;
            }
            ++fast;
        }
        return slow;
    }
}
