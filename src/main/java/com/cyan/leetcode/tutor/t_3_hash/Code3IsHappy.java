package com.cyan.leetcode.tutor.t_3_hash;

import java.util.HashSet;
import java.util.Set;

/**
 * 快乐数
 *
 * @accepted Failed 思路对的
 * @link https://leetcode.cn/problems/happy-number/
 * @author Cyan Chau
 * @create 2023-03-25
 * update 2024-04-11
 */
public class Code3IsHappy {

    public int getNext(int n) {
        int totalSum = 0;

        // 对于 19
        // 低 --> 高
        while (n > 0) {
            // digit = 19 % 10 = 9
            int digit = n % 10;

            // n = 19 / 10 = 1
            n = n / 10;
            totalSum += digit * digit;
        }
        return totalSum;
    }

    // 暴力解法
    // 不断计算 next，并放入集合，如果发现出现重复的，则出现循环
    public boolean isHappy1(int n) {
        Set<Integer> seen = new HashSet<>();

        //  一旦出现重复的就返回false
        while (n != 1 && !seen.contains(n)) {
            seen.add(n);
            n = getNext(n);
        }
        return n == 1;
    }

    public boolean isHappy2(int n) {
        int slow = n;
        int fast = getNext(n);

        while (fast != 1 && slow != fast) {
            // slow = slow.next
            slow = getNext(slow);

            // fast = fast.next.next
            fast = getNext(getNext(fast));
        }
        return fast == 1;
    }
}
