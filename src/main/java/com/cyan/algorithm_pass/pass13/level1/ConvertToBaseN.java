package com.cyan.algorithm_pass.pass13.level1;

/**
 * N 进制
 *
 * @author Cyan Chau
 * @create 2024-03-23
 */
public class ConvertToBaseN {

    public static final String[] base = {
            "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
            , "A", "B", "C", "D", "E", "F"};
    public String convertToBaseN(int m, int n) {
        if (m == 0) {
            return "0";
        }
        boolean negative = m < 0;
        m = Math.abs(m);
        StringBuffer digits = new StringBuffer();
        while (m > 0) {
            digits.append(base[m % n]);
            m /= n;
        }
        if (negative) {
            digits.append('-');
        }
        return digits.reverse().toString();
    }


}
