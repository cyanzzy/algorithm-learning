package com.cyan.course_base.sort_application;

import java.util.PriorityQueue;

/**
 * 已知一个几乎有序的数组，几乎有序是指，如果把数组排好序的话，
 * 每个元素的移动距离可以不超过K，并且K相对于数组来说比较小，
 * 请选择一个合适的排序算法针对该数组进行排序
 *
 * @author Cyan Chau
 * @create 2023-02-16
 */
public class Code3SortedArrDistanceLessK {

    /**
     * 算法思想；
     * 遍历前7个数字，放入小根堆，小根堆最小值放0位置上。首先弹出小根堆放入0位置，然后将7位置上的值放入小根堆，
     * 弹出小根堆堆顶放入1位置，然后将8位置上的值放入小根堆，…，快结束后将整个小根堆弹出一次放入数组即可
     */

    public void sortedArrDistanceLessK(int[] arr, int k) {
        // 小根堆
        PriorityQueue<Integer> heap = new PriorityQueue<>();
        int index = 0;
        // 遍历数组放入小根堆
        for (; index < Math.min(arr.length, k); index++) {
            heap.add(arr[index]);
        }
        int i = 0;
        for (; index < arr.length; i++, index++) {
            // 加入根堆
            heap.add(arr[index]);
            // 弹出小根堆堆顶
            arr[i] = heap.poll();
        }
        // 快结束后，将整个小根堆放入数组
        while (!heap.isEmpty()) {
            arr[i++] = heap.poll();
        }
    }

}
