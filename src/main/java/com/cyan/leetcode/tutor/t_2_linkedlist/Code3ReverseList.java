package com.cyan.leetcode.tutor.t_2_linkedlist;

/**
 * 反转链表
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/reverse-linked-list/description/
 * @author Cyan Chau
 * @create 2023-02-27
 * @update 2024-04-08
 */
public class Code3ReverseList {


    // 头插法（原始版本）
    public ListNode myReverseList(ListNode head) {
        // 虚拟节点
        ListNode dummyNode = new ListNode(0);
        dummyNode.next = head;
        // 断链
        dummyNode.next = null;
        // 待反转的节点
        ListNode toReverseNode = head;
        // 哨兵节点
        ListNode sentinel = head;

        while(sentinel != null) {
            sentinel = sentinel.next;
            toReverseNode.next = dummyNode.next;
            dummyNode.next = toReverseNode;
            toReverseNode = sentinel;
        }
        return dummyNode.next;
    }

//    public ListNode reverseList1(ListNode head) {
//
//        // 添加虚拟节点
//        ListNode dummy = new ListNode(0);
//        dummy.next = head;
//
//        // 定义哨兵节点
//        ListNode sentinel;
//
//        // 断链
//        dummy.next = null;
//
//        while (head != null) {
//            sentinel = head;
//            head = head.next;
//            sentinel.next = dummy.next;
//            dummy.next = sentinel;
//        }
//
//        return dummy.next;
//    }

    // 迭代法（进阶版本）TODO 重要思想
    public ListNode reverseList2(ListNode head) {
        // 前驱节点
        ListNode pre = null;
        // 当前节点
        ListNode cur = head;

        while (cur != null) {
            // 保存当前节点的后继
            ListNode next = cur.next;
            // 将当前节点的后继指向其前驱
            cur.next = pre;
            // pre后移
            pre = cur;
            // 当前节点指向原链表下一个位置
            cur = next;
        }

        return pre;
    }

    // 递归法
    public ListNode reverseList(ListNode head) {
        if (head == null || head.next == null) {
            /**
             直到当前节点的下一个节点为空时返回当前节点
             由于5没有下一个节点了，所以此处返回节点5
             */
            return head;
        }
        //递归传入下一个节点，目的是为了到达最后一个节点
        ListNode newHead = reverseList(head.next);
        /**
         第一轮出栈，head为5，head.next为空，返回5
         第二轮出栈，head为4，head.next为5，执行head.next.next=head也就是5.next=4，
         把当前节点的子节点的子节点指向当前节点
         此时链表为1->2->3->4<->5，由于4与5互相指向，所以此处要断开4.next=null
         此时链表为1->2->3->4<-5
         返回节点5
         第三轮出栈，head为3，head.next为4，执行head.next.next=head也就是4.next=3，
         此时链表为1->2->3<->4<-5，由于3与4互相指向，所以此处要断开3.next=null
         此时链表为1->2->3<-4<-5
         返回节点5
         第四轮出栈，head为2，head.next为3，执行head.next.next=head也就是3.next=2，
         此时链表为1->2<->3<-4<-5，由于2与3互相指向，所以此处要断开2.next=null
         此时链表为1->2<-3<-4<-5
         返回节点5
         第五轮出栈，head为1，head.next为2，执行head.next.next=head也就是2.next=1，
         此时链表为1<->2<-3<-4<-5，由于1与2互相指向，所以此处要断开1.next=null
         此时链表为1<-2<-3<-4<-5
         返回节点5
         出栈完成，最终头节点5->4->3->2->1
         */
        head.next.next = head;
        head.next = null;
        return newHead;
    }
}
