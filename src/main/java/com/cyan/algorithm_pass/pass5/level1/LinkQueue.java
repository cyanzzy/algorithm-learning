package com.cyan.algorithm_pass.pass5.level1;

import com.cyan.algorithm_pass.ListNode;

/**
 * 实现队列（基于链表）
 *
 * @author Cyan Chau
 * @create 2024-03-02
 */
public class LinkQueue {

    private ListNode front;
    private ListNode rear;
    private int size;

    public LinkQueue() {
        this.front = new ListNode(0);
        this.rear = new ListNode(0);
    }

    // 入队
    public void push(int value) {
        ListNode newNode = new ListNode(value);
        ListNode temp = front;

        while (temp.next != null) {
            temp = temp.next;

        }
        temp.next = newNode;
        size++;
    }

    // 出队
    public int pull() {
        if (front.next == null) {
            throw new RuntimeException("队列为空");
        }
        ListNode firstNode = front.next;
        front.next = firstNode.next;
        size--;
        return firstNode.val;
    }

    // 遍历
    public void traverse() {
        ListNode temp = front.next;
        while (temp != null) {
            System.out.print(temp.val + "\t");
            temp = temp.next;
        }
    }

//    public static void main(String[] args) {
//        LinkQueue linkQueue = new LinkQueue();
//
//        linkQueue.push(1);
//        linkQueue.push(2);
//        linkQueue.push(3);
//        linkQueue.pull();
//        linkQueue.traverse();
//        linkQueue.pull();
//        linkQueue.pull();
//        linkQueue.pull();
//
//    }
}
