package com.cyan.leetcode.tutor.t_3_hash;

import java.util.Arrays;

/**
 * 有效的字母异位词
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/valid-anagram/description/
 * @author Cyan Chau
 * @create 2023-02-23
 * update 2024-04-11
 */
public class Code1IsAnagram {

    // 排序法
    public boolean isAnagram1(String s, String t) {
        // 长度不相等，之u姐舍弃
        if (s.length() != t.length()) {
            return false;
        }
        // 排序比较
        char[] chars1 = s.toCharArray();
        char[] chars2 = t.toCharArray();

        Arrays.sort(chars1);
        Arrays.sort(chars2);

        return Arrays.equals(chars1, chars2);
    }

    public boolean isAnagram2(String s, String t) {
        // 长度不相等，之u姐舍弃
        if (s.length() != t.length()) {
            return false;
        }

        int[] table = new int[26];
        // 统计词频
        for (int i = 0; i < s.length(); i++) {
            table[s.charAt(i) - 'a']++;
        }
        // 遇到一样的字符，对应减去即可
        for (int i = 0; i < t.length(); i++) {
            table[t.charAt(i) - 'a']--;
            if (table[t.charAt(i) - 'a'] < 0) {
                return false;
            }
        }
        return true;
    }


}
