package com.cyan.algorithm_pass.pass6.level2;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

/**
 * N叉树层次遍历
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class NTreeLevelOrder {

    public List<List<Integer>> levelOrder(Node root) {
        // base case
        if (root == null) {
            return new ArrayList<>();
        }

        List<List<Integer>> ans = new ArrayList<>();
        // 队列
        Queue<Node> queue = new ArrayDeque<>();
        // 入队根元素
        queue.offer(root);

        // 当队列不为空时
        while (!queue.isEmpty()) {
            // 当前队列中包含的节点个数 cnt
            int cnt = queue.size();
            // 临时列表
            List<Integer> level = new ArrayList<>();
            for (int i = 0; i < cnt; ++i) {
                // 出队 cur
                Node cur = queue.poll();
                // 将 cur 放入临时列表
                level.add(cur.val);
                for (Node child : cur.children) {
                    queue.offer(child);
                }
            }
            ans.add(level);
        }

        return ans;
    }


    class Node {
        public int val;
        public List<Node> children;

        public Node() {}

        public Node(int _val) {
            val = _val;
        }

        public Node(int _val, List<Node> _children) {
            val = _val;
            children = _children;
        }
    }
}
