package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

/**
 * 链表的中间节点
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code6MiddleNode {

    public ListNode middleNode(ListNode head) {

        ListNode slow = head, fast = head;

        while (fast != null && fast.next != null) {
            slow = slow.next;
            fast = fast.next;
        }
        return slow;
    }

}
