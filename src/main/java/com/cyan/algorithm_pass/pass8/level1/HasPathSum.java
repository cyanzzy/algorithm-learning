package com.cyan.algorithm_pass.pass8.level1;

import com.cyan.algorithm_pass.TreeNode;

/**
 * 路径总和
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class HasPathSum {

    public boolean hasPathSum(TreeNode root, int sum) {
        if (root == null) {
            return false;
        }
        // 核心逻辑
        if (root.left == null && root.right == null) {
            return sum == root.val;
        }
        // 如果存在就满足，||
        return hasPathSum(root.left, sum - root.val) ||
                hasPathSum(root.right, sum - root.val);
    }

}
