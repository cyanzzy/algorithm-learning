package com.cyan.course_base.trie;

/**
 * 前缀树操作
 *
 * @author Cyan Chau
 * @create 2023-08-08
 */
public class Trie {
    // 定义根结点，根节点不参与存储
    private TrieNode root;

    public Trie() {
        this.root = new TrieNode();
    }

    // 插入记录
    public void insert(String word) {
        // 边界条件
        if (word == null) {
            return;
        }
        char[] chs = word.toCharArray();
        // node指向root
        TrieNode node = root;
        int index = 0;
        for (int i = 0; i < chs.length; i++) {
            // 获取字母字符在数组中的下标
            index = chs[i] - 'a';
            // 如果该字符第一次出现，则新建一个节点
            if (node.children[index] == null) {
                node.children[index] = new TrieNode();
            }
            // node指向下一个位置
            node = node.children[index];
            // 经过节点次数++
            node.path++;
        }
        // word字符串存储完毕，记录一下位置
        node.end++;
    }

    // 删除记录
    public void delete(String word) {
        if (search(word) != 0) {
            char[] chs = word.toCharArray();
            TrieNode node = root;
            int index = 0;
            for (int i = 0; i < chs.length; i++) {
                index = chs[i] - 'a';
                //当前节点的下级节点的path
                if (--node.children[index].path == 0) {
                    //下级节点标空
                    node.children[index] = null;
                    return;
                }
                node = node.children[index];
            }
            node.end--;
        }
    }

    // 判断记录是否在前缀树中
    public int search(String word) {
        if (word == null) {
            return 0;
        }
        char[] chs = word.toCharArray();
        TrieNode node = root;
        int index = 0;
        for (int i = 0; i < chs.length; i++) {
            index = chs[i] - 'a';
            if (node.children[index] == null) {
                return 0;
            }
            node = node.children[index];
        }
        return node.end;
    }

    // 所有加入的字符串中，有几个时以pre这个字符串为前缀的
    public int prefixNumber(String pre) {
        if (pre == null) {
            return 0;
        }
        char[] chs = pre.toCharArray();
        TrieNode node = root;
        int index = 0;
        for (int i = 0; i < chs.length; i++) {
            index = chs[i] - 'a';
            if (node.children[index] == null) {
                return 0;
            }
            node = node.children[index];
        }
        return node.path;
    }
}
