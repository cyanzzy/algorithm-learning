package com.cyan.algorithm_pass.pass11.level2;

/**
 * 颠倒二进制位
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class ReverseBits {

    public int reverseBits(int n) {
        int rev = 0;
        for (int i = 0; i < 32 && n != 0; ++i) {
            rev |= (n & 1) << (31 - i);
            // 逻辑右移（无符号右移）
            n >>>= 1;
        }
        return rev;
    }

}
