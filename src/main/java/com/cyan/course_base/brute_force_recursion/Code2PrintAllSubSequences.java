package com.cyan.course_base.brute_force_recursion;

/**
 * 打印一个字符串的全部子序列，包括空串
 *
 * @author Cyan Chau
 * @create 2023-08-12
 */
public class Code2PrintAllSubSequences {

    /**
     * 当前来到 index 位置两个选择
     *
     * 1.走这条路
     * 2.不走这条路
     */
    public void process(char[] chs, int index) {

        if (index == chs.length) { // 递归终止条件
            System.out.println(String.valueOf(chs));
            return;
        }

        // 选择1 我们要这个字符，并进入递归树的下一层
        process(chs, index + 1);

        // 将选择的字符保存起来
        char temp = chs[index];

        // 选择2 我们不要这个字符，并进入递归树的下一层
        chs[index] = 0; // 咱不要它
        process(chs, index + 1);
        // 恢复到丢弃之前的状态
        chs[index] = temp;

    }

    public void printAllSubSquences(String str) {
        char[] chs = str.toCharArray();
        process(chs, 0);
    }

    public static void main(String[] args) {
        new Code2PrintAllSubSequences().printAllSubSquences("abc");
    }
}
