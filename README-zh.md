# 关于

该算法笔记主要集中于数据结构和力扣教学，更多具体参考教程请看[链接](https://cyanzzy.github.io/)

## 基础教学

该模块主要侧重于算法与（高级）数据结构


> sort（排序）

[SelectionSort（选择排序）]()<br>
[BubbleSort（冒泡排序）]()<br>
[InsertionSort（插入排序）]()<br>
[MergeSort（归并排序）]()<br>
[HeapSort（堆排序）]()<br>
[QuickSort（快速排序）]()<br>
[RadixSort（桶排序）]()<br>

> sort application（排序应用）

[MergeSort Application1: SmallSumQuestion（小和问题）]()<br>
[MergeSort Application2: InverseOrderQuestion（逆序对问题）]()<br>
[HeapSort Application: SortedArrDistanceLessK（距离小于k的排序问题）]()<br>
[QuickSort Application: NetherlandsQuestion（荷兰国旗问题）]()<br>
[BinarySearch Application: SeriesQuestion（二分系列问题）]()

> linked list（链表）

[IsPalindrome（链表回文问题）]()<br>
[ListPartition（链表枢轴划分）]()<br>
[CopyListWithRandom（复制带有随机节点的链表）]()<br>
[IsCircle（环形链表问题）]()

> binary tree（二叉树）

[PreOrder <Non> Recursive（先序遍历）]()<br>
[InOrder <Non> Recursive（中序遍历）]()<br>
[PostOrder <Non> Recursive（后序遍历）]()<br>
[getBinaryTreeWidth（二叉树的宽度）]()<br>
[IsBST (Three Solutions)（二叉搜索树）]()<br>
[IsCBT（完全二叉树）]()<br>
[IsBalancedTree（平衡二叉树）]()<br>
[IsFBT（满二叉树）]()<br>
[LowestAncestor（最近公共祖先问题）]()<br>
[GetNextNodeInOrder（中序遍历下一个节点）]()<br>
[SerialBinaryTree（二叉树的序列化）]()

> graph（图）

[DFS <Non> Recursive（深度优先搜索）]()<br>
[BFS（广度优先搜索）]()<br>
[TopologySort（拓扑排序）]()<br>
[Prim（普利姆算法）]()<br>
[Kruskal（克鲁斯卡尔算法）]()<br>
[Dijkstra <Version2> （迪杰斯特拉算法）]()<br>
[Floyd（弗洛里的算法）]()


> trie（前缀树）

[Trie（前缀树）]()

> 贪心算法

后续补充

> 暴力递归

[Hanoi（汉诺塔问题）]()<br>
[AllSubSequences（字符串的全部子序列）]()<br>
[ReverseStack（使用递归逆序栈）]()<br>
[NumberOfCombinations（数字字符串转字母组合的方法数）]()<br>
[BagProblems（背包问题）]()<br>
[CardGame（纸牌博弈问题）]()<br>
[NQueens（N皇后）]()

## 基础提升
> hash（哈希表与哈希函数）

[RandomPool（自定义结构）]()<br>
[Islands（岛问题）]()<br>
[UnionFind（并查集结构）]()<br>
[KMP]()<br>

## 力扣
### 代码随想录专题
算法零基础训练

> array （数组） 
<br>

[1.Binary Search（二分查找）](https://leetcode.cn/problems/binary-search/)<br>
[2.Remove Element（移除元素）](https://leetcode.cn/problems/remove-element/)<br>
[3.Squares of a Sorted Array（有序数组的平方）](https://leetcode.cn/problems/squares-of-a-sorted-array/)<br>
[4.Minimum Size Subarray Sum（长度最小的子数组）](https://leetcode.cn/problems/minimum-size-subarray-sum/)<br>
[5.Spiral Matrix II（螺旋矩阵Ⅱ）](https://leetcode.cn/problems/spiral-matrix-ii/)

> link table （链表）

[1.Remove Linked List Elements（移除链表元素）](https://leetcode.cn/problems/remove-linked-list-elements/)<br>
[2.Design Linked List（设计链表）](https://leetcode.cn/problems/design-linked-list/)


## 算法通关村
算法零基础学习


## 算法专项

