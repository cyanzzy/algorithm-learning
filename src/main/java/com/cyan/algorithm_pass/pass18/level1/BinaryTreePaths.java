package com.cyan.algorithm_pass.pass18.level1;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 二叉树的所有路径
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class BinaryTreePaths {

    List<String> ans = new ArrayList<>();

    public List<String> binaryTreePaths(TreeNode root) {
        dfs(root, new ArrayList<>());
        return ans;
    }

    public void dfs(TreeNode root, List<Integer> list) {
        // 递归终止条件
        if (root == null) {
            return;
        }
        list.add(root.val);
        // 记录结果
        if (root.left == null && root.right == null) {
            ans.add(getPathString(list));
        }
        dfs(root.left, list);
        dfs(root.right, list);
        list.remove(list.size() - 1);
    }

    public String getPathString(List<Integer> list) {
        StringBuilder sb = new StringBuilder();
        sb.append(list.get(0));
        for (int i = 1; i < list.size(); i++) {
            sb.append("->").append(list.get(i));
        }
        return sb.toString();
    }
}
