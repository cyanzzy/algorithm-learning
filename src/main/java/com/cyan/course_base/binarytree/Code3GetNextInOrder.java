package com.cyan.course_base.binarytree;

/**
 * 找到一个节点中序遍历下的后继节点
 *
 * @author Cyan Chau
 * @create 2023-03-25
 */
public class Code3GetNextInOrder {

    public class Node {
        public int value;
        public Node left;
        public Node right;
        public Node parent;

        public Node(int data) {
            this.value = data;
        }
    }


    /**
     * 1. 如果node有右子树，那么后继节点就是右子树最左边的节点
     * 2. 如果node没有右子树，可以先看node是不是node父节点的左孩子
     *  如果node是其父亲节点的左孩子，那么node的父亲节点是它的后继节点
     *  如果node是其父亲节点的右孩子，那么就向上移动到一个一种情况：该节点是其父亲节点的左孩子
     * 3. 如果在情况2中一直寻找，都移动到空节点时还是没有发现node的后继节点，说明node根本不存在后继节点
     */

    // 找到一棵二叉树的最左边的节点
    public Node getLeftMost(Node node) {
        if (node == null) {

            return node;
        }
        while (node.left != null) {
            node = node.left;
        }
        return node;

    }

    public Node getNextNode(Node node) {
        // 如果节点为空
        if (node == null) {
            return node;
        }

        // 情况1 如果node有右子树，那么node的后继节点就是该右子树上最左边的节点
        if (node.right != null) {
            return getLeftMost(node.right);
        } else {
            // 如果node没有右子树，西安看node是不是node父节点的左孩子节点
            Node parent = node.parent;
            // 如果是右孩子节点，就向上寻找node的后继节点
            // 假设向上移动到的节点记为s，s的父节点记为p
            // 如果发现s是p的左孩子节点，那么节点p就是node的后继节点，否则就一直向上移动
            while (parent != null && parent.left != node) {
                node = parent;
                parent = node.parent;
            }
            // 如果是左孩子节点，那么此时node的父亲节点就是node的后继节点
            return parent;
        }
    }
}
