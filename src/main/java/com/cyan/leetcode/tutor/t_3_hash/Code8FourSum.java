package com.cyan.leetcode.tutor.t_3_hash;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Cyan Chau
 * @create 2023-04-04
 */
public class Code8FourSum {

    public List<List<Integer>> fourSum(int[] nums, int target) {

        List<List<Integer>> ans = new ArrayList<>();
        // 违规条件
        if (nums.length < 4 || nums == null) {
            return ans;
        }
        // 排序，满足前提条件
        Arrays.sort(nums);
        int len = nums.length;

        // 第一轮循环 确定第一个数
        for (int i = 0; i < len - 3; i++) {
            // 去重操作
            if (i > 0 && nums[i - 1] == nums[i]) {
                continue;
            }
            // 剪枝操作1
            if ((long) nums[i] + nums[i + 1] + nums[i + 2] + nums[i + 3] > target) {
                break;
            }
            // 剪枝操作2
            if ((long) nums[i] + nums[len - 1] + nums[len - 2] + nums[len - 3] < target) {
                break;
            }

            // 第二轮循环 确定前两个数
            for (int j = 0; j < len - 2; j++) {
                // 去重操作
                if (j > 0 && nums[j - 1] == nums[j]) {
                    continue;
                }
                // 剪枝操作3
                if ((long) nums[i] + nums[j] + nums[j + 1] + nums[j + 2] > target) {
                    break;
                }
                // 剪枝操作4
                if ((long) nums[i] + nums[j] + nums[len - 2] + nums[len - 1] < target) {
                    break;
                }

                // 第三重循环 使用双指针 枚举剩下两个数
                int left = j + 1, right = len - 1;
                while (left < right) {
                    // sum
                    long sum = (long) nums[i] + nums[j] + nums[left] + nums[right];

                    if (sum == target) {
                        ans.add(Arrays.asList(nums[i], nums[j], nums[left], nums[right]));
                        // 将双指针移动到下一个不同数的位置处
                        while (left < right && nums[left + 1] == nums[left]) {
                            left++;
                        }
                        left++;
                        while (left < right && nums[right] == nums[right - 1]) {
                            right--;
                        }
                        right--;
                    } else if (sum < target) {
                        left++;
                    } else {
                        right--;
                    }

                }

            }
        }
        return ans;
    }

}
