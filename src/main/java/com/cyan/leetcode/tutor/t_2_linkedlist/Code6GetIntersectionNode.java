package com.cyan.leetcode.tutor.t_2_linkedlist;

import java.util.HashSet;
import java.util.Set;

/**
 * 链表相交
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/intersection-of-two-linked-lists-lcci/
 * @author Cyan Chau
 * @create 2023-03-01
 * @update 2024-04-09
 */
public class Code6GetIntersectionNode {

    // 暴力法
    public ListNode getIntersectionNode1(ListNode headA, ListNode headB) {
        // 定义双指针
        ListNode curA = headA;
        ListNode curB = headB;
        // 定义len
        int lenA = 0, lenB = 0;

        // lenA
        while (curA != null) {
            lenA++;
            curA = curA.next;
        }

        // lenB
        while (curB != null) {
            lenB++;
            curB = curB.next;
        }

        // 重新指向头节点
        curA = headA;
        curB = headB;

        // curA指向最长链表的头节点
        if (lenA < lenB) {
            // 交换lenA与lenB
            int tempLen = lenA;
            lenA = lenB;
            lenB = tempLen;

            // 交换curA和curB
            ListNode tempNode = curA;
            curA = curB;
            curB = tempNode;
        }

        // 长度差
        int gap = lenA - lenB;

        // 让curA和curB处在同一起点
        while (gap-- > 0) {
            curA = curA.next;
        }

        // 双指针同步移动
        while (curA != null) {
            if (curA == curB) {
                return curA;
            }
            curA = curA.next;
            curB = curB.next;
        }
        return  null;
    }

    // 哈希法
    public ListNode getIntersectionNode2(ListNode headA, ListNode headB) {
        Set<ListNode> visited = new HashSet<>();
        ListNode temp = headA;

        // 将A链表放入哈希表中
        while (temp != null) {
            visited.add(temp);
            temp = temp.next;
        }

        temp = headB;

        // 遍历B链表
        while (temp != null) {
            if (visited.contains(temp)) {
                return temp;
            }
            temp = temp.next;
        }
        return null;
    }

    // 双指针法
    public ListNode getIntersectionNode(ListNode headA, ListNode headB) {
        if (headA == null || headB == null) {
            return null;
        }

        ListNode pA = headA, pB = headB;

        while (pA != pB) {
            pA = pA == null ? headB : pA.next;
            pB = pB == null ? headA : headB.next;
        }
        return pA;
    }
}
