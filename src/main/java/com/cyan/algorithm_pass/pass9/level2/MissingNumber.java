package com.cyan.algorithm_pass.pass9.level2;

/**
 * 缺失的数字
 *
 * @author Cyan Chau
 * @create 2024-03-09
 */
public class MissingNumber {

    public int missingNumber(int[] arr) {
        int left = 0, right = arr.length - 1;
        while (left < right) {
            int mid = left + ((right - left) >> 1);
            if (arr[mid] == mid) {
                // 向右查找
                left = mid + 1;
            } else { // 向左查找
                right = mid - 1;
            }
        }
        return left;
    }
}
