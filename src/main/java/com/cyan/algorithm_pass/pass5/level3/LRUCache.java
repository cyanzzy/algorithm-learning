package com.cyan.algorithm_pass.pass5.level3;

import com.cyan.algorithm_pass.pass4.level2.MaxStack;

import java.util.HashMap;
import java.util.Map;

/**
 * LRUCache
 *
 * @author Cyan Chau
 * @create 2024-03-02
 */
public class LRUCache {

    // 双链表结构
    class DLinkedNode {
        private int key;
        private int value;
        private DLinkedNode prev;
        private DLinkedNode next;

        public DLinkedNode() {
        }

        public DLinkedNode(int _key, int _value) {
            this.key = _key;
            this.value = _value;
        }
    }

    // 哈希结构
    private Map<Integer, DLinkedNode> cache = new HashMap<>();
    // 缓存实际占用元素数量
    private int size;
    // 缓存容量
    private int capacity;
    private DLinkedNode head, tail;

    public LRUCache(int capacity) {
        this.size = 0;
        this.capacity = capacity;
        // 使用虚拟头尾节点
        head = new DLinkedNode();
        tail = new DLinkedNode();
        head.next = tail;
        tail.prev = head;
    }

    public int get(int key) {
        DLinkedNode node = cache.get(key);
        if (node == null) {
            return -1;
        }
        // 如果 key 存在，先通过哈希表定位，再移动到头部
        moveToDLHead(node);
        return node.value;
    }

    public void put(int key, int value) {
        DLinkedNode node = cache.get(key);

        if (node == null) {
            // 如果 key 不存在，创建一个新的节点
            DLinkedNode newNode = new DLinkedNode(key, value);
            // 添加进哈希表
            cache.put(key, newNode);
            // 添加至双链表头部
            addToDLHead(newNode);
            ++size;
            if (size > capacity) {
                // 如果超出容量，删除双向链表的尾部节点
                DLinkedNode tail = removeDLTail();
                // 删除哈希表中对于的项
                cache.remove(tail.key);
                --size;
            }
        } else {
            // 如果 key 存在，先通过哈希表定位，再修改 value，并移到头部
            node.value = value;
            moveToDLHead(node);
        }
    }

    // 在双链表头部添加节点
    private void addToDLHead(DLinkedNode node) {
        node.prev = head;
        node.next = head.next;
        head.next.prev = node;
        head.next = node;

    }

    // 移除双链表节点
    private void removeDLNode(DLinkedNode node) {
        node.prev.next = node.next;
        node.next.prev = node.prev;
    }

    // 移除双链表尾节点
    private DLinkedNode removeDLTail() {
        DLinkedNode res = tail.prev;
        removeDLNode(res);
        return res;
    }

    // 将节点移至双链表头部
    public void moveToDLHead(DLinkedNode node) {
        removeDLNode(node);
        addToDLHead(node);
    }
}
