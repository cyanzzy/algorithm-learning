package com.cyan.algorithm_pass.pass18.level2;

import java.util.ArrayList;
import java.util.List;

/**
 * 全排列
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class Permute {

    List<List<Integer>> ans;
    List<Integer> path;
    boolean [] used;
    public List<List<Integer>> permute(int[] nums) {

        if (nums.length == 0){
            return ans;
        }
        used = new boolean[nums.length];
        ans = new ArrayList<>();
        path = new ArrayList<>();
        dfs(nums, used);
        return ans;
    }

    public void dfs(int[] nums, boolean[] used) {
        if (path.size() == nums.length) {
            ans.add(new ArrayList<>(path));
            return;
        }
        for (int i = 0; i < nums.length; i++) {
            if (used[i] == true) continue; // path里已经收录的元素，直接跳过
            used[i] = true;
            path.add(nums[i]);
            dfs(nums, used);
            path.remove(path.size() - 1);
            used[i] = false;
        }
    }
}
