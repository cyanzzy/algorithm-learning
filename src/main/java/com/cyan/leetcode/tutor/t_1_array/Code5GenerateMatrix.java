package com.cyan.leetcode.tutor.t_1_array;

/**
 * 螺旋矩阵 Ⅱ
 *
 * @accepted
 * @link https://leetcode.cn/problems/spiral-matrix-ii/
 * @author Cyan Chau
 * @create 2023-02-20
 *
 */
public class Code5GenerateMatrix {
    // 全局角度 （打印版本）
    public void generateMatrix1(int[][] matrix) {
        int tR = 0;
        int tC = 0;
        int dR = matrix.length - 1;
        int dC = matrix[0].length - 1;
        while (tR <= dR && tC <= dC) {
            printEdge(matrix, tR++, tC++, dR--, dC--);
        }
    }

    public void printEdge(int[][] matrix, int tR, int tC, int dR, int dC) {
        if (tR == dR) { // 子矩阵只有一行时
            for (int i = tC; i <= dC; i++) {
                System.out.print(matrix[tR][i] + " ");
            }
        } else if (tC == dC) { // 子矩阵只有一列时
            for (int i = tR; i <= dR; i++) {
                System.out.print(matrix[i][dC] + " ");
            }
        } else { // 一般情况
            int curC = tC;
            int curR = tR;
            // 从左往右
            while (curC != dC) {
                System.out.println(matrix[tR][curC] + " ");
                curC++;
            }
            // 从上往下
            while (curR != dR) {
                System.out.print(matrix[curR][dC] + " ");
                curR++;
            }
            // 从右往左
            while (curC != tC) {
                System.out.print(matrix[dR][curC] + " ");
                curC--;
            }
            // 从下往上
            while (curR != tR) {
                System.out.print(matrix[curR][tC] + " ");
                curR--;
            }
        }
    }

    // 层级处理
    public int[][] generateMatrix2(int n) {

        // 控制左上角框框
        int tR = 0, tC = 0;
        // 控制右下角框框
        int dR = n - 1, dC = n - 1;

        int[][] ans = new int[n][n];

        int index = 1;

        // 两个标志元素相遇后结束循环
        while (index <= n * n) {

            // 从左向右 打印行
            for (int col = tC; col <= dC; col++) {
                ans[tR][col] = index;
                index++;
            }
            tR++;
            // 从上往下 打印列
            for (int run = tR; run <= dR; run++) {
                ans[run][dC] = index;
                index++;
            }
            dC--;
            // 从右往左 打印行
            for (int col = dC; col >= tC; col--) {
                ans[dR][col] = index;
                index++;
            }
            dR--;
            // 从下往上 打印列
            for (int run = dR; run >= tR; run--) {
                ans[run][tC] = index;
                index++;
            }
            tC++;

        }
        return ans;
    }

    // 全局角度
    int index = 1;
    public int[][] generateMatrix3(int n) {
        int tR = 0;
        int tC = 0;
        int dR = n - 1;
        int dC = n - 1;
        int[][] matrix = new int[n][n];

        while (tR <= dR && tC <= dC) {
            printEdges(matrix, tR++, tC++, dR--, dC--);
        }
        return matrix;
    }

    public void printEdges(int[][] matrix, int tR, int tC, int dR, int dC) {
        if (tR == dR) { // 子矩阵只有一行时
            for (int i = tC; i <= dC; i++) {
                matrix[tR][i] = index++;
            }
        } else if (tC == dC) { // 子矩阵只有一列时
            for (int i = tR; i <= dR; i++) {
                matrix[i][dC] = index++;
            }
        } else { // 一般情况
            int curC = tC;
            int curR = tR;
            // 从左往右
            while (curC != dC) {
                matrix[tR][curC] = index++;
                curC++;
            }
            // 从上往下
            while (curR != dR) {
                matrix[curR][dC] = index++;
                curR++;
            }
            // 从右往左
            while (curC != tC) {
                matrix[dR][curC] = index++;
                curC--;
            }
            // 从下往上
            while (curR != tR) {
                matrix[curR][tC] = index++;
                curR--;
            }
        }
    }

    // 方法1 模拟

    // 方法2 层级处理（先打印最外层的一圈，然后打印内层的一圈，依次循环即可）

}
