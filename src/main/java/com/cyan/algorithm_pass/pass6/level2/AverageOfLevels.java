package com.cyan.algorithm_pass.pass6.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * 二叉树的层次平均值
 *
 * @author Cyan Chau
 * @create 2024-03-05
 */
public class AverageOfLevels {

    public List<Double> averageOfLevels(TreeNode root) {
        // 平均值
        List<Double> averages = new ArrayList<>();
        // 队列
        Queue<TreeNode> queue = new LinkedList<>();
        // 入队根元素
        queue.offer(root);

        // 当队列不为空时
        while (!queue.isEmpty()) {
            // sum
            double sum = 0;
            // 当前层次元素数
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                TreeNode node = queue.poll();
                sum += node.val;
                if (node.left != null) {
                    queue.offer(node.left);
                }
                if (node.right != null) {
                    queue.offer(node.right);
                }
            }
            averages.add(sum / size);
        }
        return averages;
    }
}
