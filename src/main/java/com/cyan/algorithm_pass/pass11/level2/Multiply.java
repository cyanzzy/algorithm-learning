package com.cyan.algorithm_pass.pass11.level2;

/**
 * 递归乘法
 *
 * @author Cyan Chau
 * @create 2024-03-12
 */
public class Multiply {

    public int multiply(int a, int b) {
        int res = 0;
        while (b != 0) {
            if ((b & 1) != 0) {
                res += a;
            }
            //a左移1位
            a <<= 1;
            //b右移1位
            b >>>= 1;
        }
        return res;
    }
}
