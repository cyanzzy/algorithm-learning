package com.cyan.algorithm_pass.pass7.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * 后序遍历
 *
 * @author Cyan Chau
 * @create 2024-03-06
 */
public class PostorderTraversal {

    // 逆先序
    // 先序遍历非递归版本进栈顺序是：根右左
    // 后序遍历非递归版本采用双栈，进栈顺序是：根左右
    public List<Integer> postorderTraversal(TreeNode head) {

        List<Integer> res = new ArrayList<>();

        if (head != null) {
            // 输入栈
            Stack<TreeNode> in = new Stack<>();
            // 输出栈
            Stack<TreeNode> out = new Stack<>();

            // 将根节点放入输入栈
            in.push(head);

            while (!in.isEmpty()) {

                // 从输入栈取出栈顶元素放入输出栈中
                head = in.pop();
                out.push(head);

                // 先 左
                if (head.left != null) {
                    in.push(head.left);
                }

                // 后 右
                if (head.right != null) {
                    in.push(head.right);
                }
            }

            // 输出
            while (!out.isEmpty()) {
                res.add(out.pop().val);
            }
        }
        return res;
    }

    public static void main(String[] args) {
        TreeNode root = new TreeNode(1);
        root.left = null;
        TreeNode right = new TreeNode(2);
        root.right = right;
        TreeNode rl = new TreeNode(3);
        right.left = rl;
        List<Integer> a = new PostorderTraversal().postorderTraversal(root);
        System.out.println(a);
    }

}
