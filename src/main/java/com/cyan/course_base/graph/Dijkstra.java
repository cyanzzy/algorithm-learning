package com.cyan.course_base.graph;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * @author Cyan Chau
 * @create 2023-04-09
 */
public class Dijkstra {

    // 未优化版本
    //  从head出发，所有head能到达的节点，生成到达每个节点的最小路径记录并返回
    public HashMap<Node, Integer> dijkstra(Node head, int size) {
        /**
         * distanceMap 表示从head出发到所有点的最小距离
         * Node 从head出发到Node
         * Integer 从head出发到达Node的最小距离
         * 如果在表中，没有T的记录，含义是从head出发到T这个点的距离为正无穷
         */
        HashMap<Node, Integer> distanceMap = new HashMap<>();
        // 初始化distanceMap
        distanceMap.put(head, 0);
        // 已经求过距离的节点存储在selectedNodes
        HashSet<Node> selectedNodes = new HashSet<>();
        // 得到最小距离并且没有求过的节点
        Node minNode = getMinDistanceAndUnselectedNode(distanceMap, selectedNodes);
        while (minNode != null) {
            // 从head出发到minNode的最小距离
            int distance = distanceMap.get(minNode);
            for (Edge edge : minNode.edges) {
                // edge的另一个邻节点
                Node toNode = edge.to;
                // 如果toNode第一次出现过，计算距离
                if (!distanceMap.containsKey(toNode)) {
                    // 距离更新成 distance[head-->minNode] + edge.weight[midNode-->toNode]
                    distanceMap.put(toNode, distance + edge.weight);
                } else {
                    distanceMap.put(edge.to,
                            // 从源节点出发到toNode的最小距离[head-->toNode]   原路径
                            Math.min(distanceMap.get(toNode),
                                    // 当前处理结点 找到的新路径[head-->minNode]+[minNode-->toNode] 新路径
                                    distance + edge.weight));
                }
                // 设置成已求过
                selectedNodes.add(minNode);
                minNode = getMinDistanceAndUnselectedNode(distanceMap, selectedNodes);
            }
        }
        return distanceMap;
    }


    // 获取最小距离且没有选择的结点，返回没有被选入最短路径且，head到nodes距离更短的节点
    public Node getMinDistanceAndUnselectedNode(HashMap<Node, Integer> distanceMap, HashSet<Node> touchedNodes) {

        Node minNode = null;
        int minDistance = Integer.MAX_VALUE;
        for (Map.Entry<Node, Integer> entry : distanceMap.entrySet()) {
            //获取视图的键，即图顶点
            Node node = entry.getKey();
            //获取距离[head-->node]
            int distance = entry.getValue();
            //如果当前nodes没有被选入最短路径中，且[head-->nodes]距离更短，更新一下minNode和最小距离
            if (!touchedNodes.contains(node) && distance < minDistance) {
                minNode = node;
                minDistance = distance;
            }
        }
        return minNode;
    }
}