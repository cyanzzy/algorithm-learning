package com.cyan.course_base.graph;

/**
 * Floyd
 *
 * @author Cyan Chau
 * @create 2023-07-23
 */
public class Floyd {

    public static void Floyd(int[][] graph, int[][] path) {
        int i, j, k;
        int[][] temp = new int[graph.length][graph.length];

        for (i = 0; i < graph.length; i++) {
            for (j = 0; j < graph.length; j++) {
                path[i][j] = -1;
                temp[i][j] = graph[i][j];
            }
        }

        /**
         * 以k顶点为中介对所有顶点对(i, j)进行检测和修改
         */
        for (i = 0; i < graph.length; i++) {
            for (j = 0; j < graph.length; j++) {
                for (k = 0; k < graph.length; k++) {
                    if (temp[i][j] > temp[i][k] + temp[k][j]) {
                        temp[i][j] = temp[i][k] + temp[k][j];
                        path[i][j] = k;
                    }
                }
            }
        }
    }
}
