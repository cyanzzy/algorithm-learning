package com.cyan.course_base.sort_application;

/**
 * Code5BinarySearch
 *
 * @author Cyan Chau
 * @create 2023-02-21
 */
public class Code5BinarySearch {

    // 应用1 在一个有序数组中，找某个数是否存在
    public boolean binarySearch(int[] sortedArr, int num) {
        if (sortedArr == null || sortedArr.length == 0) {
            return false;
        }
        int left = 0;
        int right = sortedArr.length - 1;
        int mid = 0;

        while (left < right) {
            mid = left + ((right - left) >> 1);
            if (sortedArr[mid] == num) {
                return true;
            } else if (sortedArr[mid] > num) {
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return sortedArr[left] == num;
    }

    // 应用2 在一个有序数组中，找大于等于某个数最左侧的位置 [重要]
    public int nearestIndex(int[] arr, int value) {
        int left = 0;
        int right = arr.length - 1;
        int mid = 0;
        int index = -1;

        while (left <= right) {
            mid = left + ((right - left) >> 1);
            if (arr[mid] >= value) { // value落在左边
                index = mid;
                right = mid - 1;
            } else { // value落在右边
                left = mid + 1;
            }
        }
        return index;
    }

//    public static void main(String[] args) {
//        int[] arr = {1, 2, 3, 5, 6, 8};
//        int index = new Code5BinarySearch().nearestIndex(arr, 4);
//        System.out.println("arr["+ index + "] = " + arr[index]);
//    }

    // 应用3
    public static int LocalMin(int[] arr) {

        if (arr == null || arr.length == 0) {
            return -1;
        }

        int N = arr.length;
        if (N == 1) {
            return 0;
        }

        if (arr[0] < arr[1]) {
            return 0;
        }

        if (arr[N - 1] < arr[N - 2]) {
            return N - 1;
        }
        int left = 0;
        int right = N - 1;

        /**
         *  L...R 肯定有局部最小
         *  保证L~R之间至少有三个数，我们才能进行二分
         *  如果L~R之间不够三个数,我们直接比较L和R哪个更小，哪个就是局部最小
         */
        while (left < right - 1) {
            int mid = left + ((right - left) >> 1);
            if (arr[mid] < arr[mid - 1] && arr[mid] < arr[mid + 1]) {
                return mid;
            } else {
                // 1 left>mid  mid>right
                // 2 left<mid  mid<right
                // 3 left<mid  mid>right
                if (arr[mid] > arr[mid - 1]) {
                    right = mid - 1;
                } else {
                    left = mid + 1;
                }
            }
        }
        // 循环结束之后,L>=R-1. 数组内要么是两个数，要么是一个数
        return arr[left] < arr[right] ? left : right;
    }


    public static int[] randomArray(int maxLen, int maxValue) {
        int len = (int) (Math.random() * maxLen);
        int[] arr = new int[len];
        if (len > 0) {
            arr[0] = (int) (Math.random() * maxValue);
            for (int i = 1; i < len; i++) {
                do {
                    arr[i] = (int) (Math.random() * maxValue);
                } while (arr[i] == arr[i - 1]);
            }
        }
        return arr;
    }

    public static boolean check(int[] arr, int minIndex) {
        if (arr.length == 0) {
            return minIndex == -1;
        }
        int left = minIndex - 1;
        int right = minIndex + 1;
        boolean leftBigger = left >= 0 ? arr[left] > arr[minIndex] : true;
        boolean rightBigger = right < arr.length ? arr[right] > arr[minIndex] : true;
        return leftBigger && rightBigger;
    }

    public static void printArray(int[] arr) {
        for (int num : arr) {
            System.out.print(num + " ");
        }
        System.out.println();
    }

    public static void main(String[] args) {
        int maxLen = 10;
        int maxValue = 200;
        int testTime = 1000000;
        System.out.println("测试开始");
        for (int i = 0; i < testTime; i++) {
            int[] arr = randomArray(maxLen, maxValue);
            int ans = LocalMin(arr);
            if (!check(arr, ans)) {
                printArray(arr);
                System.out.println("ERROR");
                System.out.println(ans);
                break;
            }
        }
        System.out.println("测试结束");
    }



}
