package com.cyan.course_base.binarytree.order.recursion;

import com.cyan.course_base.binarytree.Node;

/**
 * 后序遍历 递归版本
 *
 * @author Cyan Chau
 * @create 2023-03-01
 */
public class PostOrder {

    public void postorder(Node head) {

        // 递归终止条件
        if (head == null) {
            return;
        }

        // 访问左子树
        postorder(head.left);

        // 访问右子树
        postorder(head.right);

        // 访问根节点
        //Visit(head);
    }
}
