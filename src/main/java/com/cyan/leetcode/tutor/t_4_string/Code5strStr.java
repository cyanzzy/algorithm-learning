package com.cyan.leetcode.tutor.t_4_string;

/**
 *  KMP
 * 找出字符串中第一个匹配项的下标
 *
 * @accepted Failed
 * @see com.cyan.course_advance.kmp
 * @reference https://cyanzzy.github.io/2023/08/17/%E7%AE%97%E6%B3%95%E6%8F%90%E5%8D%87-2-KMP/
 * @reference https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/solutions/575568/shua-chuan-lc-shuang-bai-po-su-jie-fa-km-tb86/
 * @link https://leetcode.cn/problems/find-the-index-of-the-first-occurrence-in-a-string/description/
 * @author Cyan Chau
 * @create 2024-04-13
 */
public class Code5strStr {


    // 暴力方法
    public int nativeSolution(String str, String pattern) {
        int n = str.length(), m = pattern.length();
        char[] s_char = str.toCharArray(), p_char = pattern.toCharArray();

        // 枚举 str 的出发点
        for (int i = 0; i <= n - m; i++) {
            // 从 str 的出发点和模式串的首位开始匹配
            int index = i, begin = 0;
            while (begin < m && s_char[index] == p_char[begin]) {
                index++;
                begin++;
            }
            // [完全匹配] 如果能完全匹配,返回 str 的出发点下标
            if (begin == m) {
                return i;
            }
        }
        return -1;
    }


    // Next 数组
    public int[] getNext(char[] pattern) {
        if (pattern.length == 1) {
            return new int[]{-1};
        }
        int[] next = new int[pattern.length];
        next[0] = -1;
        next[1] = 0;
        int i = 2, j = 0;

        while (i < next.length) {
            if (pattern[i - 1] == pattern[j]) { // 相等，匹配下一个
                next[i++] = ++j;
            } else if (j > 0) {  // 不相等往前跳，继续匹配
                j = next[j];
            } else {
                // 来到最左边情况，说明i之前字符不存在前后缀匹配情况，匹配下一个
                next[i++] = 0;
            }
        }
        return next;
    }

    public int strStr(String str, String pattern) {
        if (str == null || pattern == null ||
            pattern.length() < 1 || str.length() < pattern.length()) {
            return -1;
        }
        char[] mainStr = str.toCharArray();
        char[] subStr = pattern.toCharArray();
        int i = 0, j = 0;
        int[] next = getNext(subStr);
        while (i < mainStr.length && j < subStr.length) {
            if (mainStr[i] == subStr[j]) {
                i++;
                j++;
            } else if (next[j] == -1) {
                // 往前跳到开头都匹配不出来，找主串下一个位置开始匹配
                i++;
            } else {
                // 当前匹配不出来且没有跳到子串开头，往前跳
                j = next[j];
            }
        }
        return j == subStr.length ? i - j : -1;
    }
}
