package com.cyan.algorithm_pass.pass13.level1;

/**
 * 整数反转
 *
 * @author Cyan Chau
 * @create 2024-03-23
 */
public class Reverse {

    public int reverse(int x) {
        int rev = 0;
        while (x != 0) {
            if (rev < Integer.MIN_VALUE / 10 || rev > Integer.MAX_VALUE / 10) {
                return 0;
            }
            int digit = x % 10;
            x /= 10;
            rev = rev * 10 + digit;

        }
        return rev;
    }
}
