package com.cyan.algorithm_pass.pass8.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 二叉树的最小深度
 *
 * @author Cyan Chau
 * @create 2024-03-07
 */
public class MinDepth {

//    // DFS
//    public int minDepth(TreeNode root) {
//        // exit
//        if (root == null) {
//            return 0;
//        }
//        // base case
//        if (root.left == null && root.right == null) {
//            return 1;
//        }
//        int min_depth = Integer.MAX_VALUE;
//        if (root.left != null) {
//            min_depth = Math.min(min_depth, minDepth(root.left));
//        }
//        if (root.right != null) {
//            min_depth = Math.min(min_depth, minDepth(root.right));
//        }
//        return min_depth + 1;
//    }

    // BFS
    class QueueNode {
        TreeNode node;
        int depth;

        public QueueNode(TreeNode node, int depth) {
            this.node = node;
            this.depth = depth;
        }
    }

    public int minDepth(TreeNode root) {
        if (root == null) {
            return 0;
        }
        Queue<QueueNode> queue = new LinkedList<>();
        queue.offer(new QueueNode(root, 1));
        while (!queue.isEmpty()) {
            QueueNode nodeDepth = queue.poll();
            TreeNode node = nodeDepth.node;
            int depth = nodeDepth.depth;
            // 第一个叶子节点
            if (node.left == null && node.right == null) {
                return depth;
            }
            if (node.left != null) {
                queue.offer(new QueueNode(node.left, depth + 1));
            }
            if (node.right != null) {
                queue.offer(new QueueNode(node.right, depth + 1));
            }
        }
        return 0;
    }

}
