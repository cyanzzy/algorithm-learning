package com.cyan.course_base.linkedlist;

/**
 * 链表枢轴划分
 *
 * @author Cyan Chau
 * @create 2023-02-24
 */
public class Code2ListPartition {

    public Node listPartition(Node head, int pivot) {
        // 定义sH、sT（小于pivot的部分）
        Node sH = null; Node sT = null;
        // 定义eH、eT（等于pivot的部分）
        Node eH = null; Node eT = null;
        // 定义bH、bT（大于pivot的部分）
        Node bH = null; Node bT = null;
        Node next = null;

        while (head != null) {
            // 断链
            next = head.next;
            head.next = null;

            // 若当前值小于枢轴
            if (head.val < pivot) {
                // 若左半部分没有任何节点时
                // s头指针sH和s尾指针sT都指向head
                if (sH == null) {
                    sH = head;
                    sT = head;
                } else { // 否则
                    // s尾指针sT连接当前节点
                    sT.next = head;
                    // 更新s尾指针sT
                    sT = head;
                }
            } else if (head.val == pivot) { // 若当前值等于枢轴
                // 若中间部分没有任何节点时
                // e头指针eH和e尾指针eT都指向head
                if (eH == null) {
                    eH = head;
                    eT = head;
                } else {
                    // e尾指针eT连接当前节点
                    eT.next = head;
                    // 更新e尾指针eT
                    eT = head;
                }

            } else { // 若当前值大于枢轴
                // 如果右半部分没有任何结点时
                // b头指针bH和b尾指针bT都指向head
                if (bH == null) {
                    bH = head;
                    bT = head;
                } else {
                    // b尾指针bT连接当前节点
                    bT.next = head;
                    // 更新b尾指针bT
                    bT = head;
                }
            }

            // 更新当前节点位置
            head = next;
        }

        // 如果存在左半段链表[见图解]  （连接左半部分链表）
        if (sT != null) {
            // s尾指针sT后继指向e头指针eH
            sT.next = eH;
            eT = eT == null ? sT : eT;
        }

        // 如果左半段链表和中间段链表不是都没有[见图解] （在上代码基础上如果存在连接好的或者无需连接的左半部分，则连接右半部分）
        if (eT != null){
            eT.next = bH;
        }

        /**
         * 如果左半部分头指针存在，返回sH
         * 如果左半部分头指针不存在，判断中间部分头指针是否存在
         * 如果中间部分头指针存在，返回eH
         * 如果中间部分头指针不存在，返回右半部分的头指针bH
         */
        return sH != null ? sH : eH != null ? eH : bH;
    }

}
