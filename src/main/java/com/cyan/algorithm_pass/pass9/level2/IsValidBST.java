package com.cyan.algorithm_pass.pass9.level2;

import com.cyan.algorithm_pass.TreeNode;

import java.util.Stack;

/**
 * 验证二叉搜索树
 *
 * @author Cyan Chau
 * @create 2024-03-11
 */
public class IsValidBST {

    // 递归
//    public boolean isValidBST(TreeNode root) {
//        return isValidBST(root, Long.MIN_VALUE, Long.MAX_VALUE);
//    }
//
//    public boolean isValidBST(TreeNode node, long lower, long upper) {
//        if (node == null) {
//            return true;
//        }
//        if (node.val <= lower || node.val > upper) {
//            return false;
//        }
//        return isValidBST(node.left, lower, node.val) &&
//                isValidBST(node.right, node.val, upper);
//
//    }

    // 中序遍历
    public boolean isValidBST(TreeNode root) {

        Stack<TreeNode> stack = new Stack<>();
        double inorder = -Double.MAX_VALUE;

        while (!stack.isEmpty() || root != null) {
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            if (!stack.isEmpty()) {
                root = stack.pop();
                // 如果中序遍历得到的节点的值小于等于前一个 inorder，说明不是二叉搜索树
                if (root.val <= inorder) {
                    return false;
                }
                inorder = root.val;
                // 转向右分支
                root = root.right;
            }
        }
        return true;
    }
}
