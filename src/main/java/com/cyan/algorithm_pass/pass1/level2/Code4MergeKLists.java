package com.cyan.algorithm_pass.pass1.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.PriorityQueue;

/**
 * 合并 K 个链表
 *
 * @author Cyan Chau
 * @create 2024-01-08
 */
public class Code4MergeKLists {

    public ListNode mergeTwoLists(ListNode list1, ListNode list2) {
        // base case
        if (list1 == null || list2 == null) {
            return list1 != null ? list1 : list2;
        }
        ListNode L = new ListNode(0);
        ListNode tail = L;

        while (list1 != null && list2 != null) {
            if (list1.val <= list2.val) {
                tail.next = list1;
                list1 = list1.next;
            } else {
                tail.next = list2;
                list2 = list2.next;
            }
            tail = tail.next;
        }
        tail.next = list1 != null ? list1 : list2;

        return L.next;
    }


    // 顺序合并
    public ListNode mergeKLists1(ListNode[] lists) {

        ListNode ans = null;

        for (int i = 0; i < lists.length; i++) {
            ans = mergeTwoLists(ans, lists[i]);
        }
        return ans;
    }


    // 分治合并
    public ListNode mergeKLists2(ListNode[] lists) {
        return merge(lists, 0, lists.length - 1);
    }

    public ListNode merge(ListNode[] lists, int begin, int end) {
        // base case 1
        if (begin == end) {
            return lists[begin];
        }
        // end of recurrence
        if (begin > end) {
            return null;
        }

        int mid =  begin + ((end - begin) >> 1);
        return mergeTwoLists(merge(lists, begin, mid), merge(lists, mid + 1, end));

    }

    // 优先队列合并
    class Status implements Comparable<Status> { // 实现了 Comparable 接口的 Node

        // Node.val
        int val;
        // Node
        ListNode ptr;

        Status(int val, ListNode ptr) {
            this.val = val;
            this.ptr = ptr;
        }

        @Override
        public int compareTo(Status other) {
            return this.val - other.val;
        }
    }

    // 构造小顶堆
    PriorityQueue<Status> queue = new PriorityQueue<>();

    public ListNode mergeKLists(ListNode[] lists) {

        // lists = [[1,4,5],[1,3,4],[2,6]]
        // 此处的每个 node，其实是 lists 中链表数组的头指针
        for (ListNode node : lists) {
            // for loop：把 1, 1, 2 装入小顶堆，
            // 即将 lists 中每个链表数组中最小元素装入小顶堆
            if (node != null) {
                queue.offer(new Status(node.val, node));
            }
        }

        // 虚拟节点和尾指针
        ListNode L = new ListNode(0);
        ListNode tail = L;

        while (!queue.isEmpty()) {
            // 弹出（小顶）堆的堆顶元素
            Status top = queue.poll();
            // 尾插法
            tail.next = top.ptr;
            tail = tail.next;

            // 如果弹出的 top 节点所属的链表还有剩余节点的情况下
            if (top.ptr.next != null) {
                // 将 top 下一个指向节点送入优先队列
                queue.offer(new Status(top.ptr.next.val, top.ptr.next));
            }
        }
        return L.next;
    }
}
