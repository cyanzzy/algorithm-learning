package com.cyan.algorithm_pass.pass2.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.List;

/**
 * 两两交换链表中的节点
 *
 * @author Cyan Chau
 * @create 2024-01-17
 */
public class Code2SwapPairs {

    // 迭代
    public ListNode swapPairs1(ListNode head) {

        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;
        // `temp` 表示当前到达的节点
        ListNode temp = dummyHead;

        while (temp.next != null && temp.next.next != null) {
            ListNode node1 = temp.next;
            ListNode node2 = temp.next.next;
            temp.next = node2;
            node1.next = node2.next;
            node2.next = node1;
            temp = node1;
        }

        return dummyHead.next;
    }


    // 头插法
    public ListNode swapPairs(ListNode head) {

        ListNode dummyHead = new ListNode(0);
        dummyHead.next = head;

        // 计算长度
        int length = 0;
        ListNode cur = head;
        while (cur != null) {
            length++;
            cur = cur.next;
        }
        // tail 始终指向当前组反转后的最后一个节点
        // 比如 [1,2,3,4] --> [2,1,4,3]，tail 便记录节点元素 1
        ListNode tail = dummyHead;
        ListNode next = dummyHead;

        // 外循环控制反转次数
        for (int i = 0; i < length / 2; i++) {
            // 初始化 cur 的位置
            cur = tail.next;
            // 临时保存当前组反转后的最后一个节点
            ListNode temp = cur;
            // tail 作为上一组反转链表的最后一个节点开始断链
            tail.next = null;

            // 内循环控制每组反转
            for (int j = 0; j < 2; j++) { // 头插法
                next = cur.next;
                cur.next = tail.next;
                tail.next = cur;
                cur = next;
            }
            // 更新 tail
            tail = temp;
            // 恢复链表
            tail.next = cur;
        }

        return dummyHead.next;
    }


//    public static void main(String[] args) {
//
//        // [1,2,3,4]
//        ListNode L = new ListNode(1);
//        L.next = new ListNode(2);
//        L.next.next = new ListNode(3);
//        L.next.next.next = new ListNode(4);
//
//        new Code2SwapPairs().swapPairs(L);
//
//    }
}
