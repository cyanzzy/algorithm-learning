package com.cyan.course_advance.hash;

import java.util.HashMap;

/**
 * RandomPool
 *
 * @author Cyan Chau
 * @create 2023-08-15
 */
public class Code1RandomPool {

    public static class Pool<T> {
        // 实现T-->index的映射
        private HashMap<T, Integer> keyIndexMap;
        // 实现index-->T的映射
        private HashMap<Integer, T> indexKeyMap;
        // Pool的大小
        private int size;

        public Pool() {
            this.indexKeyMap = new HashMap<>();
            this.keyIndexMap = new HashMap<>();
            this.size = 0;
        }

        // 新增记录
        public void insert(T key) {
            if (!this.keyIndexMap.containsKey(key)) {
                this.keyIndexMap.put(key, this.size);
                this.indexKeyMap.put(this.size++, key);
            }
        }

        /**
         * 删除记录
         * keyIndexMap (lastKey,lastIndex)-->(lastKey,deleteIndex)
         * indexKeyMap (deleteIndex,deleteKey)-->(deleteIndex,lastKey)
         * <p>
         * delete keyIndexMap (deleteKey,deleteIndex)
         * delete indexKeyMap (lastKey,lastIndex)
         *
         * @param key
         */
        public void delete(T key) {
            if (this.keyIndexMap.containsKey(key)) {
                // 指向要删除元素的index
                int deleteIndex = this.keyIndexMap.get(key);
                // 指向最后一个元素的index
                int lastIndex = --this.size;
                // 指向最后一个元素的key
                T lastKey = this.indexKeyMap.get(lastIndex);

                this.keyIndexMap.put(lastKey, deleteIndex);
                this.indexKeyMap.put(deleteIndex, lastKey);

                this.keyIndexMap.remove(key);
                this.indexKeyMap.remove(lastIndex);

            }

        }

        // 随机获取记录
        public T getRandom() {
            if (this.size == 0) {
                return null;
            }
            int randomIndex = (int) (Math.random() * this.size); // 0 ~ size -1
            return this.indexKeyMap.get(randomIndex);
        }
    }
}
