package com.cyan.algorithm_pass.pass14.level2;

/**
 * 堆排序
 *
 * @author Cyan Chau
 * @create 2024-03-25
 */
public class HeapSort {


    // 堆排序
    public void heapSort(int[] arr) {
        // 非法情况
        if (arr == null || arr.length < 2) {
            return;
        }
        int heapSize = arr.length;
        // 从最后一个非叶子节点开始调整堆，编号从 0 开始
        // heapSize / 2 是最后一个非叶子节点
        for (int i = heapSize / 2; i >= 0; --i) {
            heapify(arr, i, heapSize);
        }

        int size = arr.length;

        // 将大顶堆的堆顶元素和末尾元素交换位置
        swap(arr, 0, --size);
        while (size > 0) {
            heapify(arr, 0, size);
            swap(arr, 0, --size);
        }
    }

    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    // 堆调整
    public void heapify(int[] arr, int index, int size) {
        // left 左孩子
        int left = index * 2 + 1;
        while (left < size) {
            // largest 表示index节点两个孩子较大的节点
            int largest = left + 1 < size && arr[left + 1] > arr[left] ? left + 1 : left;
            // 当前节点和largest比较
            largest = arr[largest] > arr[index] ? largest : index;
            // 如果当前节点大于较大孩子节点，则不进行操作
            if (largest == index) {
                break;
            }
            // 如果当前节点小于较大孩子节点，则与孩子节点交换
            int temp = arr[index];
            arr[index] = arr[largest];
            arr[largest] = temp;

            // 向下继续调整
            index = largest;
            left = index * 2 + 1;
        }
    }
}
