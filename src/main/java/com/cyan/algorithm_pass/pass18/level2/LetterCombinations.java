package com.cyan.algorithm_pass.pass18.level2;

import java.util.ArrayList;
import java.util.List;

/**
 *  电话号码的字母组合
 *
 * @author Cyan Chau
 * @create 2024-04-30
 */
public class LetterCombinations {

    // 存储最终答案
    List<String> ans;
    // 存储单个路径的答案
    StringBuilder path;

    public List<String> letterCombinations(String digits) {
        ans = new ArrayList<>();
        path = new StringBuilder();

        if (digits == null || digits.length() == 0) {
            return ans;
        }
        // 构建数字到字符的映射
        String[] letterMap = {"", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"};
        dfs(digits, letterMap, 0);
        return ans;
    }

    public void dfs(String digits, String[] letterMap, int index) {
        // 递归终止条件
        if (index == digits.length()) {
            ans.add(path.toString());
            return;
        }
        // digits字符串中的单个数字对应的字符串
        String str = letterMap[digits.charAt(index) - '0'];
        // 单层遍历逻辑
        for (int i = 0; i < str.length(); ++i) { //控制横向遍历
            // 递归是控制单个分支
            // 当前答案加入path
            path.append(str.charAt(i));
            // 遍历递归树的下一层
            dfs(digits, letterMap, index + 1);
            // 递归结束后，path答案取出，恢复到刚进入dfs的状态
            path.deleteCharAt(path.length() - 1);
        }
    }
}
