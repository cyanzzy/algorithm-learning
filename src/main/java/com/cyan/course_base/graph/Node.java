package com.cyan.course_base.graph;

import java.util.ArrayList;

/**
 * @author Cyan Chau
 * @create 2023-03-27
 */
public class Node {

    // 图节点的值
    public int value;

    // 图节点的出度
    public int out;

    // 图节点的入度
    public int in;

    // 图的邻接点
    public ArrayList<Node> nexts;

    // 图的边
    public ArrayList<Edge> edges;

    public Node(int value) {
        this.value = value;
        in = 0;
        out = 0;
        nexts = new ArrayList<>();
        edges = new ArrayList<>();
    }
}
