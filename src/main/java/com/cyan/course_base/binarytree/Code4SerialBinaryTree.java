package com.cyan.course_base.binarytree;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 序列化问二叉树
 *
 * @author Cyan Chau
 * @create 2023-03-26
 */
public class Code4SerialBinaryTree {

    // 先序遍历序列化
    public String serialByPre(Node head) {
        if (head == null) {
            return "#!";
        }
        String res = head.val + "!";
        res += serialByPre(head.left);
        res += serialByPre(head.right);
        return res;
    }

    // 先序遍历反序列化
    public Node reconPreString(String preStr) {
        String[] values = preStr.split("!");

        Queue<String> queue = new LinkedList<>();

        // 将字符串切割后装入队列
        for (String val : queue) {
            queue.offer(val);
        }

        return reconPreOrder(queue);
    }

    public Node reconPreOrder(Queue<String> queue) {
        String value = queue.poll();
        if (value.equals("#")) {
            return null;
        }

        Node head = new Node(Integer.valueOf(value));
        head.left = reconPreOrder(queue);
        head.right = reconPreOrder(queue);
        return head;
    }


    // 层次遍历序列化
    public String serialByLevel(Node head) {


        if (head == null) {
            return "#!";
        }

        String res = head.val + "!";
        Queue<Node> queue = new LinkedList<>();
        queue.offer(head);

        while (!queue.isEmpty()) {
            head = queue.poll();

            if (head.left != null) {
                res += head.left.val + "!:";
                queue.offer(head.left);
            } else {
                res += "#!";
            }
            if (head.right != null) {
                res += head.right.val + "!";
                queue.offer(head.right);
            } else {
                res += "#!";
            }
        }

        return res;
    }

    // 层次遍历反序列化
    public Node reconByLevelString(String levelStr) {

        String[] values = levelStr.split("!");
        int index = 0;
        Node head = generateNodeByString(values[index++]);
        Queue<Node> queue = new LinkedList<>();

        if (head != null) {
            queue.offer(head);
        }
        Node node = null;
        while (!queue.isEmpty()) {

            node = queue.poll();
            // 设置左孩子
            node.left = generateNodeByString(values[index++]);
            // 设置右孩子
            node.right = generateNodeByString(values[index++]);
            if (node.left != null) {
                queue.offer(node.left);
            }
            if (node.right != null) {
                queue.offer(node.right);
            }
        }
        return head;

    }

    public Node generateNodeByString(String str) {
        if (str.equals("#")) {
            return null;
        }
        return new Node(Integer.valueOf(str));
    }
}
