package com.cyan.algorithm_pass.pass3.level1;

/**
 * 搜索插入位置
 *
 * @author Cyan Chau
 * @create 2024-01-18
 */
public class Code2SearchInsert {

    public int searchInsert(int[] nums, int target) {

        int left = 0, right = nums.length - 1, ans = nums.length;

        while (left <= right) {

            int mid = left + ((right - left) >> 1);
            if (target <= nums[mid]) {
                ans = mid;
                right = mid - 1;
            } else {
                left = mid + 1;
            }
        }
        return ans;
    }
}
