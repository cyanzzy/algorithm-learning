package com.cyan.algorithm_pass.pass2.level2;

import com.cyan.algorithm_pass.ListNode;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.List;

/**
 * 链表相加
 *
 * @author Cyan Chau
 * @create 2024-01-17
 */
public class Code4AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        Deque<Integer> stack1 = new ArrayDeque<>();
        Deque<Integer> stack2 = new ArrayDeque<>();

        while (l1 != null) {
            stack1.push(l1.val);
            l1 = l1.next;
        }
        while (l2 != null) {
            stack2.push(l2.val);
            l2 = l2.next;
        }
        int carry = 0;
        ListNode dummyNode = new ListNode(0);

        while (!stack1.isEmpty()  || !stack2.isEmpty() || carry > 0) {

            int a = stack1.isEmpty() ? 0 : stack1.pop();
            int b = stack2.isEmpty() ? 0 : stack2.poll();
            int sum = a + b + carry;
            // 记录进位
            carry = sum / 10;
            sum %= 10;
            ListNode currNode = new ListNode(sum);
            currNode.next = dummyNode.next;
            dummyNode.next = currNode;
        }

        return dummyNode.next;
    }

}
