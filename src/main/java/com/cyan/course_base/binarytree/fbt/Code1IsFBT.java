package com.cyan.course_base.binarytree.fbt;

import com.cyan.course_base.binarytree.Node;

/**
 * FBT: 满二叉树
 *
 * @author Cyan Chau
 * @create 2023-03-21
 */
public class Code1IsFBT {

    // 整合信息
    public class ReturnData {
        // 树的高度
        public int height;
        // 节点数
        public int nodes;

        public ReturnData(int height, int nodes) {
            this.height = height;
            this.nodes = nodes;
        }
    }

    // 递归函数
    public ReturnData process(Node x) {
        // 空树返回信息
        if (x == null) {
            return new ReturnData(0, 0);
        }

        // 返回左子树的信息
        ReturnData leftData = process(x.left);

        // 返回右子树的信息
        ReturnData rightData = process(x.right);

        // 求以x为根结点的树的高度
        int height = Math.max(leftData.height, rightData.height) + 1;

        // 求以x为根结点的树的数量
        int nodes = leftData.nodes + rightData.nodes + 1;

        // 返回信息结构
        return new ReturnData(height, nodes);
    }

    public boolean isFBT(Node head) {
        // 空树情况
        if (head == null) {
            return true;
        }
        // 返回信息
        ReturnData data = process(head);
        return data.nodes == (1 << data.height - 1);
    }

}
