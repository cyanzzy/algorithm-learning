package com.cyan.course_base.sort;

import java.util.Arrays;

/**
 * HeapSort
 *  复盘：
 *  目标构造大根堆，然后每次把堆顶元素换至末尾，
 *  然后重新调整成大根堆，最终序列有序
 *
 * @author Cyan Chau
 * @create 2023-02-13
 */
public class Code5HeapSort {
    /**
     * 算法思想：
     * 将无序序列调整成大根堆，将堆顶元素与序列最后一个元素交换位置，
     * 此时有序序列增加一个，无序序列减少一个，对新的无序序列进行重复操作，即可实现排序
     */

//    // 堆的插入
//    public void heapInsert(int[] arr, int index) {
//        // 若孩子节点大于父亲节点，则破坏大根堆性质，需要向上调整
//        while (arr[index] > arr[(index - 1) / 2] ) {
//            int temp = arr[index];
//            arr[index] = arr[(index - 1) / 2];
//            arr[(index - 1) / 2] = temp;
//
//            index = (index - 1) / 2;
//        }
//    }

    // 堆调整
    public void heapify(int[] arr, int index, int size) {
        // left 左孩子
        int left = index * 2 + 1;
        while (left < size) {
            // largest 表示index节点两个孩子较大的节点
            int largest = left + 1 < size && arr[left + 1] > arr[left] ? left + 1 : left;
            // 当前节点和largest比较
            largest = arr[largest] > arr[index] ? largest : index;
            // 如果当前节点大于较大孩子节点，则不进行操作
            if (largest == index) {
                break;
            }
            // 如果当前节点小于较大孩子节点，则与孩子节点交换
            int temp = arr[index];
            arr[index] = arr[largest];
            arr[largest] = temp;

            // 向下继续调整
            index = largest;
            left = index * 2 + 1;
        }
    }

    // 堆排序
    public void heapSort(int[] arr) {
        // 非法情况
        if (arr == null || arr.length < 2) {
            return;
        }
        int heapSize = arr.length;
        // 从最后一个非叶子节点开始调整堆，编号从 0 开始
        // heapSize / 2 是最后一个非叶子节点
        for (int i = heapSize / 2; i >= 0; --i) {
            heapify(arr, i, heapSize);
        }

        int size = arr.length;

        // 将大顶堆的堆顶元素和末尾元素交换位置
        swap(arr, 0, --size);
        while (size > 0) {
            heapify(arr, 0, size);
            swap(arr, 0, --size);
        }
    }

    public void swap(int[] arr, int i, int j) {
        int tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    // for test
    public static void comparator(int[] arr) {
        Arrays.sort(arr);
    }

    // for test
    public static int[] generateRandomArray(int maxSize, int maxValue) {
        int[] arr = new int[(int) ((maxSize + 1) * Math.random())];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = (int) ((maxValue + 1) * Math.random()) - (int) (maxValue * Math.random());
        }
        return arr;
    }

    // for test
    public static int[] copyArray(int[] arr) {
        if (arr == null) {
            return null;
        }
        int[] res = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            res[i] = arr[i];
        }
        return res;
    }

    // for test
    public static boolean isEqual(int[] arr1, int[] arr2) {
        if ((arr1 == null && arr2 != null) || (arr1 != null && arr2 == null)) {
            return false;
        }
        if (arr1 == null && arr2 == null) {
            return true;
        }
        if (arr1.length != arr2.length) {
            return false;
        }
        for (int i = 0; i < arr1.length; i++) {
            if (arr1[i] != arr2[i]) {
                return false;
            }
        }
        return true;
    }

    // for test
    public static void printArray(int[] arr) {
        if (arr == null) {
            return;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        System.out.println();
    }

    // for test
    public static void main(String[] args) {
        int testTime = 500000;
        int maxSize = 100;
        int maxValue = 100;
        boolean succeed = true;
        for (int i = 0; i < testTime; i++) {
            int[] arr1 = generateRandomArray(maxSize, maxValue);
            int[] arr2 = copyArray(arr1);
            new Code5HeapSort().heapSort(arr1);
            comparator(arr2);
            if (!isEqual(arr1, arr2)) {
                succeed = false;
                break;
            }
        }
        System.out.println(succeed ? "Nice!" : "Fucking fucked!");

        int[] arr = generateRandomArray(maxSize, maxValue);
        printArray(arr);
        new Code5HeapSort().heapSort(arr);
        printArray(arr);
    }
}
