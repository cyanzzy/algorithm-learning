package com.cyan.leetcode.tutor.t_2_linkedlist;

/**
 * 移除链表元素
 *
 * @accepted Success
 * @link https://leetcode.cn/problems/remove-linked-list-elements/description/
 * @author Cyan Chau
 * @create 2023-02-23
 * update 2024-04-08
 */
public class Code1RemoveElements {


    public ListNode myRemoveElements(ListNode head, int val) {
        // 边界处理
        if (head == null) {
            return null;
        }
        // 虚拟节点
        ListNode dummyNode = new ListNode(-1);
        dummyNode.next = head;

        // 待删除的节点的前驱节点 pre
        // 待删除的节点 p
        ListNode pre = dummyNode, p = head;

        while (p != null) {
            // 如果满足删除条件，执行删除操作
            if (p.val == val) {
                pre.next = p.next;
            } else { // 如果不满足删除条件，继续迭代
                pre = p;
            }
            p = p.next;
        }
        return dummyNode.next;

    }

    // 迭代删除
    public ListNode removeElements1(ListNode head, int val) {
        // 定义虚节点
        ListNode dummyNode = new ListNode(0);

        dummyNode.next = head;
        ListNode temp = head;

        // 核心逻辑
        while (temp.next != null) {
            if (temp.next.val == val) {
               temp.next = temp.next.next;
            } else {
                temp = temp.next;
            }
        }
        return dummyNode.next;
    }




    // 递归删除
    public ListNode removeElements(ListNode head, int val) {
        if (head == null) { // 递归终止条件
            return head;
        }
        // 递归删除下一个节点
        head.next = removeElements(head.next, val);

        /**
         * 判断head节点值是否等于给定的val。
         * 如果节点值等于val，则head需要被删除，因此删除操作后的头节点为head.next；
         * 如果head的节点值不等于val，则head保留，因此删除操作后的头节点还是head。
         * 上述过程是一个递归的过程。
         */
        return head.val == val ? head.next : head;
    }
}
