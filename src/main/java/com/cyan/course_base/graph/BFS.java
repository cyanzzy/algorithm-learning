package com.cyan.course_base.graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

/**
 * @author Cyan Chau
 * @create 2023-04-02
 */
public class BFS {

    public void bfs(Node node) {
        // 节点不存在，则返回
        if (node == null) {
            return;
        }

        // 定义队列存储节点
        Queue<Node> queue = new LinkedList<>();
        // 定义哈希集合标记是否已访问过
        HashSet<Object> set = new HashSet<>();

        // 当前节点入队
        queue.offer(node);
        // 并且标记为已访问
        set.add(node);

        while (!queue.isEmpty()) {
            // 当前节点出队
            Node cur = queue.poll();
            // 访问当前节点 Visit操作
            System.out.println(cur.value);

            // 将当前节点的所有邻节点访问完
            for (Node next : cur.nexts) {
                // 若当前节点的所有邻接点没有访问过
                if (!set.contains(next)) {
                    // 标记为已访问
                    set.add(next);
                    // 该节点入队
                    queue.offer(next);
                }
            }
        }
    }
}
